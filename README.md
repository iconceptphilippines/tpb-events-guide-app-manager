# Installation #

- Download the all the files including the sql file for the database
- Open database file (sql file) on text editor to edit the url 
```
Ex. http://eventsguide.tpb.gov.ph replace this to http://google.com.ph
```
- Upload the files to the server
- After the files have been uploaded, `edit wp-config.php`
```
define( 'DB_NAME' , 'database_name' );\
define( 'DB_USER', 'database_user' );\
define( 'DB_PASS', 'database_password' );\
// DB_HOST if not localhost, you can set other sqlhost\
define( 'DB_HOST', 'localhost' );\
```
- After editing and saving the config, visit the website url with /wp-admin to login, then go to Settings > Permalinks then just save the permalink
- Visit the homepage and sub-pages to verify if all pages are working and not returning 404 page