# IC SMS WordPress Plugin

## How to use
1. Add your API keys on SMS > Settings > SMS Gateways
2. Then choose what SMS API to use.
3. To add contacts just click SMS > Add New Contact.
4. To send message click SMS > Send Message then type the group or contact name where you want to send the message then enter the message.


## Logs
- You can view logs of whats going on after you send messages. 


## Releases

Release 1.9.2

- Bug fix: Fix possible concatenation of numbers when sending in single request

Release 1.9.1

- New: SMS Preview to allow users to preview the message before sending
- Bug Fix: Converted some UTF8 characters to Ascii to prevent other characters in converting to question marks like quotes.

Release 1.8.0

- New: Automated birthday greeting
- Bug Fix: Double send when notification is in draft and published

Release 1.3.1

- Improvement: Applied busy bee 100 numbers in one send capability to reduce api calls and not choke Busy Bee api.

Release 1.3.0

- New: Added counter of how many parts a message is.
- New: BusyBee SMS Gateway implemented.
- Removed: Shortcodes feature.

Release 1.2.0

- Fixed: Shortcodes issue

Release 1.0.9

- Improvement: Added download of csv template in import page.

Release 1.0.8

- Bug fixes for js
- Search Contact list by first, last, middle names

Release 1.0.5

- Fixed: Line breaks doesn't reflect in messages.

Release 1.0.4

- Fixed: full name problem cause it's missing some letters like l, m, and f

Release 1.0.3 

- Fixed: query posts_per_page in getting contacts by group to -1 to show all

Release 1.0.2 

- Improv: Added number column in the subscribers list

Release 1.0.1

- Fixed: shortcode {date} from server to local time

Release 1.0.0

- Scheduling of message
- Multiple numbers per contact
- Removed logs
- Added message history
- Message shortcodes
- Per message status
- Changed gateway implementation 

Release 0.1.0

- Initial release