(function($) {

	// Date picker
	$('.js-datepicker').datepicker({
		dateFormat: "yy-mm-dd"
	});

	// Check if select input is there
	if ( $('#ic-sms-send-message-to').length ) {

		// Select send to
		$('#ic-sms-send-message-to').chosen(); 

		// Per message
		var one_message_length 		= 160,
			multiple_message_length = 154;

        $count         = $('#ic-sms-message-character-count');
        $message_count = $('#ic-sms-messages-count');
        var smsPreview = document.getElementById('ic-sms-preview-sms');

		// Alerted
		var alerted = false;
        var request = false;

		$(document).on( 'keyup', '#ic-sms-send-message-body', function() {

			if ($(this).attr('readonly')) {
				return;
			}

            if (request !== false) {
                request.abort();
            }

            var message = this.value;

            request = ic_sms_to_ascii(this.value, function(res) {
                request = false;
                
                // Prevent multiple request when typing
                if (res.success != true) {
                    smsPreview.innerHTML = '';
                    return;
                }

                // Escape quotes
                var s = res.data.asciiString;
                s = s.replace(/\\"/g,'"');
                s = s.replace(/\\'/g,"'");
                smsPreview.innerHTML = s;
                
                var unsupportedIndices           = [];
                var originalQuestionMarksIndices = [];
                var asciiString                  = res.data.asciiString;

                for (var i=0; i<message.length;i++) {
                    if (message[i] == '?') {
                        originalQuestionMarksIndices.push(i);
                    }
                }

                for (var i=0; i<asciiString.length;i++) {
                    // Check if it is a question mark and check if the question mark
                    // is a converted question mark due to unsupported ascii character
                    if (asciiString[i] == '?' && originalQuestionMarksIndices.indexOf(i) == -1) {
                        unsupportedIndices.push(i);
                    }
                }

                var unsupportedCharacters = [];
                for (var i=0; i<unsupportedIndices.length;i++) {
                    if (!message[unsupportedIndices[i]]) {
                        continue;
                    }
                    unsupportedCharacters.push(message[unsupportedIndices[i]]);
                }

                if (unsupportedCharacters.length != 0) {
                    if (unsupportedCharacters.length > 1) {
                        alert('"'+unsupportedCharacters.join(' ') + '" characters are not supported in a SMS message and it could not be sent properly.');
                    } else {
                        alert('"'+unsupportedCharacters.join(' ') + '" character is not supported in a SMS message and it could not be sent properly.');
                    }
                }
                
            });

            var textarea_character_count = this.value.length;
            var limit                    = one_message_length;
            var total_messages           = 1;

			// Check if only one message
			if (textarea_character_count <= limit) {

				total_messages = 1;

			} else {

				total_messages = Math.ceil(textarea_character_count/multiple_message_length);
			}

			$count.html(textarea_character_count);
			$message_count.html(total_messages);

			// Display alert 
			if (textarea_character_count > limit && !alerted) {
				alerted = true;
				alert("Warning: You are sending a multipart message. Please be advise that depending on the telecom carrier or mobile phone model, sending multipart message sometimes results to the message being split to more than one message on the recipient's mobile phone.");
			}
		});
	}


	// Logger
	$logger_select = $('#ic-sms-logger-select');

	$logger_select.change(function(e) {
		
		console.log( ic_sms_add_query_arg( 'date', this.value ) );
	});

})(jQuery);

function ic_sms_to_ascii(string, callback) {
    return jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        dataType: 'JSON',
        data: {
            action: 'ic_sms/convert_to_ascii',
            nonce: IC_SMS.nonce,
            string: string
        }
    })
    .done(function(data) {
        callback(data);
    })
    .fail(function(data) {
        callback(data);
    });    
}

function ic_sms_add_query_arg(key, value)
{
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    var i=kvp.length; var x; while(i--) 
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&'); 
}