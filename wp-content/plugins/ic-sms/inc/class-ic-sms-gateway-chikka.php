<?php

if( !defined( 'WPINC' ) ) {

	die();
}

if( !class_exists( 'IC_SMS_Gateway_Chikka' ) ) :

class IC_SMS_Gateway_Chikka extends IC_SMS_Gateway
{

	/**
	 * @overide from parent
	 * @var array
	 */
	public $title 	= 'Chikka';


	/**
	 * @overide from parent
	 * @var array
	 */
	public $id 		= 'chikka';


	/**
	 * @overide from parent
	 * @var array
	 */
	public $form_fields = array(
		'client_id' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Client ID',
			'description' 	=> 'You can find this your chikka account.'
		),
		'secret_key' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Secret key',
			'description' 	=> 'You can find this your chikka account.'
		),
		'shortcode'	 => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Shortcode or Number',
			'description' 	=> 'You can find this your chikka account.'
		)
	);


	/**
	 * Holds the chikka api url
	 *
	 * @access private
	 * @var null
	 */
	private $_endpoint = null;


	/**
	 * connect()
	 *
	 * This is called before we can call send() method
	 * IF NOT USE JUST OVERIDE IT AND PUT NOTHING
	 *
	 * This will be called so if your Gateway has a Library 
	 * the connection will not be on loop when sending messages
	 * to multiple contacts
	 *
	 * Just throw an exception if there is an error in your gateway
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @return boolean | WP_Error   True if successfully sent and must return WP_Error if there are errors
	 */
	public function connect() 
	{
		// NO NEED FOR THIS CHIKKA DOESNT HAVE A LIBRARY
	}


	/**
	 * send()
	 *
	 * Sends a message
	 *
	 * Just throw an exception if there is an error in your message
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @param  string $to      		The phone number string
	 * @param  string $message 		The message
	 */
	public function send( $to, $message )
	{
		$from = $this->get_option( 'shortcode' );


		// Defined request params
		$request_params = array(
	        "message_type" 	=> "SEND",
	        "mobile_number" => str_replace('+', '', $to), //remove plus sign chikka dont want it
	        "shortcode" 	=> $from,
	        "message_id" 	=> md5(date('c') . $to), // unique message id using the time and the $v or to number
	        "message" 		=> $message,
	        "client_id" 	=> $this->get_option( 'client_id' ),
	        "secret_key" 	=> $this->get_option( 'secret_key' )
	    );

	    $request_string = '';

	    // Append params into a querystring
	    foreach($request_params as $k => $v) {

	        $request_string .= '&'.$k.'='.$v;
	    }


	    // Initialize a curl request
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $this->_endpoint);
	    curl_setopt($ch, CURLOPT_POST, count($request_params));
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $cr = curl_exec($ch);
	    curl_close($ch);

	    $response = json_decode( $cr, true );

	    // Check if response failed
	    if ( !empty( $response['status'] ) && '200' != $response['status']  ) {
	    	
	    	$response = array_merge( array(
	    		'description' => '',
	    		'message' => ''
	    	), $response );

	    	throw new Exception( "{$response['message']}: {$response['description']}" );
	    }
	}

	public function disconnect() {}
}

return new IC_SMS_Gateway_Chikka();

endif; // End class exists check