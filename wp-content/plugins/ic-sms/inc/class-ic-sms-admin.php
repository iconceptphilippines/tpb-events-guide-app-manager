<?php

// Prevent direct file access
if ( !defined( 'WPINC' ) ) {
	
	die();
}

if ( !class_exists( 'IC_SMS_Admin' ) ):

/**
 * This class customises the WordPress admin
 * of the plugin
 */

class IC_SMS_Admin
{
	/**
	 * assets()
	 *
	 * This registers and enqueues all the styles and scripts
	 * in the admin area
	 *
	 * @static
	 * @access public
	 * 
	 * @see IC_SMS _load_hooks function - add_action( 'admin_enqueue_scripts', $function );
	 *
	 * @return void
	 */
	public static function assets()
	{
		wp_enqueue_style( 'thickbox' );
		wp_enqueue_script( 'thickbox' );
		
		wp_register_style( 'chosen-jquery', IC_SMS_URI . '/vendor/chosen/chosen.min.css' );
		wp_register_script( 'chosen-jquery', IC_SMS_URI . '/vendor/chosen/chosen.jquery.min.js', array( 'jquery' ) );

		wp_register_style( 'ic-sms-admin', IC_SMS_URI . '/assets/css/ic-sms-admin.css', array( 'chosen-jquery' ) );
		wp_enqueue_style( 'ic-sms-admin' );

		wp_register_script( 'ic-sms-admin', IC_SMS_URI . '/assets/js/ic-sms-admin.js', array( 'jquery-ui-datepicker','chosen-jquery' ), '', true );
		wp_enqueue_script( 'ic-sms-admin' );

		wp_localize_script( 'ic-sms-admin', 'IC_SMS', array(
			'nonce' => wp_create_nonce( 'ic_sms_nonce' )
		) );
	}

	/**
	 * menus()
	 *
	 * This adds all the admin menu of the plugin
	 * 
	 * @static
	 * @access public
	 *
	 * @see IC_SMS _load_hooks function - add_action( 'admin_menu', $function );
	 * 
	 * @return void
	 */
	public static function menus()
	{
		// 3rd party customizations
		$capability = apply_filters( 'ic_sms/capability', 'manage_options' );


		// Add submenu page with the Contact post type main menu
		// add_submenu_page( 'edit.php?post_type=' . IC_SMS_Notification::POST_TYPE, 'Send Message', 'Send Message', $capability, 'ic-sms-send-message', array( 'IC_SMS_Admin_Send_Message', 'output' ) );
		add_submenu_page( 'edit.php?post_type=' . IC_SMS_Notification::POST_TYPE, 'Settings', 'Settings', $capability, 'ic-sms-settings', array( 'IC_SMS_Admin_Settings', 'output' ) );
		add_submenu_page( 'edit.php?post_type=' . IC_SMS_Notification::POST_TYPE, 'Import', 'Import', $capability, 'ic-sms-import', array( 'IC_SMS_Admin_Import', 'output' ) );
	}	


	/**
	 * disable_autosave()
	 *
	 * This removes the autosave javascript of WordPress
	 * 
	 * @static
	 * @access public
	 *
	 * @see IC_SMS _load_hooks function - add_action( 'admin_enqueue_scripts', $function );
	 * 
	 * @return void
	 */
	public static function disable_autosave()
	{
		// This are the post types that will be disabled with autosave
		$disabled_post_type_autosave = array( IC_SMS_Notification::POST_TYPE, IC_SMS_Subscriber::POST_TYPE );

		// dont remove the autosave if its not in the disabled post type autosave
		if ( in_array( get_post_type(), $disabled_post_type_autosave ) ) {
			
			return;
		}

		// Removes the script from the admin markup
		wp_dequeue_script( 'autosave' );
	}


	/**
	 * subscriber_title_to_full_name()
	 *
	 * This will replace the title
	 * of the subscriber into the subscriber full name data
	 * base on it's first name, last name, and middle name on the metadata
	 *
	 * @static
	 * @access public
	 *
	 * @see ic_sms_get_subscriber_full_name() - inc/functions-ic-sms-subscriber.php
	 * 
	 * @param  string $title The title of the subscriber
	 * @return string        The title of the subscriber
	 */
	public static function subscriber_title_to_full_name( $title )
	{
		global $post_type;

		if ( $post_type == IC_SMS_Subscriber::POST_TYPE ) {
			
			return ic_sms_get_subscriber_full_name( get_the_ID() );
		}

		return $title;
	}


	/**
	 * remove_groups_metabox_in_notification()
	 *
	 * This will remove the Groups metabox in the notification post type
	 *
	 * @static
	 * @access public
	 *
	 * @see IC_SMS _load_hooks - add_action( 'admin_menu', $function )
	 * 
	 * @return void
	 */
	public static function remove_groups_metabox_in_notification()
	{
		remove_meta_box( 'tagsdiv-' . IC_SMS_Group::TAXONOMY, IC_SMS_Notification::POST_TYPE, 'side' );
	}
}

endif; // End class exists check