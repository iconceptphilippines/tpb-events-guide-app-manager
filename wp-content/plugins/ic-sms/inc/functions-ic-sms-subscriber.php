<?php

/**
 * ic_sms_get_subscriber_first_name()
 * 
 * This gets the first name of the subscriber from the metadata
 * 
 * @param  int 		$subscriber_id
 * @return string
 */
function ic_sms_get_subscriber_first_name( $subscriber_id )
{
	return get_post_meta( $subscriber_id, 'ic_sms_subscriber_first_name', true );
}


/**
 * ic_sms_get_subscriber_last_name()
 * 
 * This gets the last name of the subscriber from the metadata
 * 
 * @param  int 		$subscriber_id
 * @return string
 */
function ic_sms_get_subscriber_last_name( $subscriber_id )
{
	return get_post_meta( $subscriber_id, 'ic_sms_subscriber_last_name', true );
}


/**
 * ic_sms_get_subscriber_middle_name()
 * 
 * This gets the middle name of the subscriber from the metadata
 * 
 * @param  int 		$subscriber_id
 * @return string
 */
function ic_sms_get_subscriber_middle_name( $subscriber_id )
{
	return get_post_meta( $subscriber_id, 'ic_sms_subscriber_middle_name', true );
}


/**
 * ic_sms_get_subscriber_email()
 * 
 * This gets the email of the subscriber from the metadata
 * 
 * @param  int 		$subscriber_id
 * @return string
 */
function ic_sms_get_subscriber_email( $subscriber_id )
{
	return get_post_meta( $subscriber_id, 'ic_sms_subscriber_email', true );
}


/**
 * ic_sms_get_subscriber_numbers()
 * 
 * This gets all the numbers of the subscriber from the metadata
 * 
 * @param  int 		$subscriber_id
 * @return array
 */
function ic_sms_get_subscriber_numbers( $subscriber_id ) 
{
	// Get all the numbers this should be in array
	return get_post_meta( $subscriber_id, 'ic_sms_subscriber_number', false );
}

/**
 * ic_sms_get_subscriber_numbers_string()
 * 
 * This will get all the numbers of the subscriber
 * each in a new line
 * 
 * @param  int 		$post_id 
 * @return string          
 */
function ic_sms_get_subscriber_numbers_string( $subscriber_id )
{
	$numbers = ic_sms_get_subscriber_numbers( $subscriber_id );

	return implode( PHP_EOL , $numbers);
}


/**
 * ic_sms_get_subscriber_full_name()
 * 
 * This gets the full subscriber name of the contact
 * 
 * @param  int    $subscriber_id The id of the contact
 * @param  string $format     The format of the name
 * @return string             The formatted full name of the contact
 */
function ic_sms_get_subscriber_full_name( $subscriber_id, $format = '%l, %f %m' )
{
	$full_name = '';

	$first_name 	= ic_sms_get_subscriber_first_name( $subscriber_id );
	$last_name 		= ic_sms_get_subscriber_last_name( $subscriber_id );
	$middle_name 	= ic_sms_get_subscriber_middle_name( $subscriber_id );

	$full_name = str_replace(array('%l','%f','%m'), array( $last_name, $first_name, $middle_name ), $format);

	return apply_filters( 'ic_sms/contact_get_full_name', $full_name, $subscriber_id, $format );
}


/**
 * ic_sms_update_subscriber_first_name()
 * 
 * This updates the first name of the subscriber
 * 
 * @param  int 		$subscriber_id
 * @param  string 	$first_name
 * @return string
 */
function ic_sms_update_subscriber_first_name( $subscriber_id, $first_name )
{
	return update_post_meta( $subscriber_id, 'ic_sms_subscriber_first_name', $first_name );
}


/**
 * ic_sms_update_subscriber_last_name()
 * 
 * This updates the last name of the subscriber
 * 
 * @param  int 		$subscriber_id
 * @param  string 	$last_name
 * @return string
 */
function ic_sms_update_subscriber_last_name( $subscriber_id, $last_name )
{
	return update_post_meta( $subscriber_id, 'ic_sms_subscriber_last_name', $last_name );
}


/**
 * ic_sms_update_subscriber_middle_name()
 * 
 * This updates the middle name of the subscriber
 * 
 * @param  int 		$subscriber_id
 * @param  string 	$middle_name
 * @return string
 */
function ic_sms_update_subscriber_middle_name( $subscriber_id, $middle_name )
{
	return update_post_meta( $subscriber_id, 'ic_sms_subscriber_middle_name', $middle_name );
}


/**
 * ic_sms_update_subscriber_email()
 * 
 * This updates the email of the subscriber
 * 
 * @param  int 		$subscriber_id
 * @param  string 	$email
 * @return string
 */
function ic_sms_update_subscriber_email( $subscriber_id, $email )
{
	return update_post_meta( $subscriber_id, 'ic_sms_subscriber_email', $email );
}


/**
 * ic_sms_update_subscriber_numbers()
 * 
 * This updates all the numbers of the subscriber
 *
 * Multiple number should be separater by line break
 * 
 * @param  int 		$subscriber_id
 * @param  array 	$numbers_string
 * @return void
 */
function ic_sms_update_subscriber_numbers( $subscriber_id, $numbers )
{
	// Clear the saved numbers first
	delete_post_meta( $subscriber_id, 'ic_sms_subscriber_number' );

	foreach( $numbers as $number ) {

		// Remove whitespaces
		$_n = trim( $number );

		// Just save when there is a number and its not empty
		if( !empty( $_n ) ) {

			add_post_meta( $subscriber_id, 'ic_sms_subscriber_number', $_n );
		}
	}
}


/**
 * ic_sms_update_subscriber_group()
 *
 * Updates the group of the subscriber
 * 
 * @param  int    $subscriber_id 
 * @param  mixed  $groups        
 * @return void
 */
function ic_sms_update_subscriber_group( $subscriber_id, $groups )
{
	wp_set_post_terms( $subscriber_id, $groups, IC_SMS_Group::TAXONOMY );
}

/**
 * ic_sms_get_csv_data_rows()
 *
 * This gets all the rows and columns of a CSV file
 * 
 * @param  string $csv_path The path of the CSV file in the server
 * @return array  			Array containing the data of rows and columns of each row
 */
function ic_sms_get_csv_data_rows( $csv_path )
{
	// Check if the file extension is csv
	if ( pathinfo( $csv_path, PATHINFO_EXTENSION ) !== 'csv' ) {
		
		throw new Exception("File should be a .csv file");
	}

	// Check if the path of the csv exists in the server
	if ( !file_exists( $csv_path ) ) {
		
		throw new Exception("CSV file not do not exist.");
	}

	$headers = array();
	$rows = array();
	$counter = 0;
	$_file = fopen( $csv_path, 'r' );

	while (($emapData = fgetcsv($_file, 10000, ",")) !== FALSE) {

		// Header
		if ( $counter == 0 ) {

			$headers = array_flip( $emapData );
		} else {

			$data = array();

			foreach( $headers as $k => $v ) {

				$data[ $k ] = isset( $emapData[ $v ] ) ? $emapData[ $v ] : '';
			}

			$rows[] = $data;
		}

		$counter++;
	}

	return $rows;
}

/**
 * ic_sms_import_csv_subscribers()
 *
 * This imports all subscriber in a CSV
 * the CSV file should have a FIRST, LAST, MIDDLE, NUMBER, EMAIL, and GROUPS column
 * at the first row of the file
 * to import multiple number just add a comma
 * 
 * @param  string $path The path of the CSV file in the server
 * @return int          The count of the successfully imported subscriber
 */
function ic_sms_import_csv_subscribers( $path ) 
{
	$items 	= ic_sms_get_csv_data_rows( $path );
	$ctr 	= 0;
	
	foreach( $items as $item ) {

		$default = array(
			'first' => '',
			'last' => '',
			'middle' => '',
			'email' => '',
			'number' => '',
			'groups' => ''
		);

		$item = array_merge( $default, array_change_key_case( $item, CASE_LOWER ) );

		$numbers = explode( ',', $item['number'] );

		$new = new IC_SMS_Subscriber();
		$new->set_first_name( $item[ 'first' ] );
		$new->set_last_name( $item[ 'last' ] );
		$new->set_middle_name( $item[ 'middle' ] );
		$new->set_email( $item[ 'email' ] );
		$new->set_numbers( $numbers );
		$id = $new->save();

		ic_sms_update_subscriber_group( $id, $item['groups'] );

		$ctr++;
	}

	return $ctr;
}


function ic_sms_get_shortcodes_description()
{
	$descriptions = array(
		'date' 			=> 'The datetime the message was sent',
		'full_name' 	=> 'Full name of the contact',
		'first_name' 	=> 'First name of the contact',
		'last_name' 	=> 'Last name of the contact',
		'middle_name' 	=> 'Middle name of the contact',
		'email' 		=> 'Email of the contact',
	);

	return apply_filters( 'ic_sms/get_shortcodes_description', $descriptions );
}


function ic_sms_get_subscriber_message( $subscriber_id, $message )
{
	$raw = $message;

	$find = array(
		'date' 			=> date(apply_filters( 'ic_sms/message_date_format', 'F d, Y h:i:s a', $subscriber_id ), strtotime(current_time('mysql'))),
		'full_name' 	=> ic_sms_get_subscriber_full_name( $subscriber_id, '%f %m %l' ),
		'first_name' 	=> ic_sms_get_subscriber_first_name( $subscriber_id ),
		'last_name' 	=> ic_sms_get_subscriber_last_name( $subscriber_id ),
		'middle_name' 	=> ic_sms_get_subscriber_middle_name( $subscriber_id ),
		'email' 		=> ic_sms_get_subscriber_email( $subscriber_id ),
	);

	$keys = array_map( function( $v ) {
		return '{'. $v .'}';
	}, array_keys( $find ) );

	$message = str_replace( $keys, $find, $message);

	return apply_filters( 'ic_sms/get_subscriber_message', $message, $subscriber_id, $raw );
}