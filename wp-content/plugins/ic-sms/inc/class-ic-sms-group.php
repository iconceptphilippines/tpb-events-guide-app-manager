<?php

if( !defined( 'WPINC' ) ) {

	die();
}

if( !class_exists( 'IC_SMS_Group' ) ) :

class IC_SMS_Group
{
	const TAXONOMY = 'ic-sms-group';
}

endif; // End class exists check