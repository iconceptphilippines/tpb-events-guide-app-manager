<?php

if( !defined( 'WPINC' ) ) {

	die();
}


if( !class_exists( 'IC_SMS_Admin_Subscribers' ) ) :

class IC_SMS_Admin_Subscribers
{
	/**
	 * actions()
	 * 
	 * This removes the quick edit option
	 *
	 * @static
	 * @access public
	 *
	 * @see __construct()
	 *
	 * @param  array $actions 
	 * @return void
	 */
	public static function actions( $actions )
	{
		global $post_type;

		if ( $post_type !== IC_SMS_Subscriber::POST_TYPE ) {
			
			return $actions;
		}

		unset( $actions['inline hide-if-no-js'] );

		return $actions;
	}


	/**
	 * columns()
	 *
	 * Customise the columns of the list
	 *
	 * @static
	 * @access public 
	 *
	 * @see __construct()
	 * 
	 * @param  array $columns  The collection of columns
	 * @return array           The customise collection of columns
	 */
	public static function columns( $columns )
	{
		unset( $columns['date'] );
		$columns['title'] = 'Name';
		$columns['numbers'] = 'Numbers';
		$columns['date'] = 'Date Added';

		return $columns;
	}


	/**
	 * custom_column()
	 *
	 * Adds the content of the columns
	 *
	 * @static
	 * @access public 
	 *
	 * @param  string $column  The name of the column
	 * @return void
	 */
	public static function custom_column( $column, $post_id )
	{
		if ( $column == 'numbers' ) {
			
			echo str_replace( PHP_EOL , '<br/>', ic_sms_get_subscriber_numbers_string( $post_id ));
		}
	}


	/**
	 * query()
	 *
	 * This allows the user to search through the contacts
	 * by number, last, first, middle name
	 *
	 * @static
	 * @access public
	 * 
	 * @param  WP_Query &$query 
	 * @return void
	 */
	public static function query( &$query )
	{
		// This query is for admin only
		if ( !is_admin() || !$query->is_main_query() || $query->get( 'post_type' ) !== IC_SMS_Subscriber::POST_TYPE ) {
			
			return;
		}

		// Get search param
		$s = $query->get( 's' );

		if ( !empty( $s ) ) {
			
			$query->set( 's', '' );

			$meta_query = $query->get( 'meta_query' );

			if ( empty( $meta_query ) ) {
				
				$meta_query = array();
			}

			$meta_query['relation'] = 'OR';
			
			$meta_query[] = array(
				'key' => 'ic_sms_subscriber_last_name',
				'value' => $s,
				'compare' => 'LIKE'
			);

			$meta_query[] = array(
				'key' => 'ic_sms_subscriber_first_name',
				'value' => $s,
				'compare' => 'LIKE'
			);

			$meta_query[] = array(
				'key' => 'ic_sms_subscriber_middle_name',
				'value' => $s,
				'compare' => 'LIKE'
			);

			$meta_query[] = array(
				'key' => 'ic_sms_subscriber_number',
				'value' => $s,
				'compare' => 'LIKE'
			);

			$query->set( 'meta_query', $meta_query );
		}
	}
}

endif; // End class exists check