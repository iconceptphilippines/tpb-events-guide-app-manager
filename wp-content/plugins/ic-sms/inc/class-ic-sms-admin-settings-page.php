<?php

abstract class IC_SMS_Admin_Settings_Page
{
	public $id, $label;

	public function __construct()
	{
		add_filter( 'ic_sms_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'ic_sms_sections_' . $this->id, array( $this, 'output_sections' ) );
		add_action( 'ic_sms_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'ic_sms_settings_save_' . $this->id, array( $this, 'save' ) );
	}

	public function get_settings() 
	{
		return array();
	}

	public function get_sections() 
	{
		return array();
	}

	public function output_sections() 
	{
		global $current_section;

		$sections = $this->get_sections();

		if ( empty( $sections ) ) {
			return;
		}

		// Select first section if empty
		if ( empty( $current_section ) ) {
			
			$current_section = $this->get_first_section_key();
		}

		echo '<ul class="subsubsub">';

		$array_keys = array_keys( $sections );

		foreach ( $sections as $id => $label ) {
			echo '<li><a href="' . admin_url( 'edit.php?post_type=' . IC_SMS_Notification::POST_TYPE . '&page=ic-sms-settings&tab=' . $this->id . '&section=' . sanitize_title( $id ) ) . '" class="' . ( $current_section == $id ? 'current' : '' ) . '">' . $label . '</a> ' . ( end( $array_keys ) == $id ? '' : '|' ) . ' </li>';
		}

		echo '</ul><br class="clear" />';
	}

	public function get_first_section_key()
	{
		$sections = $this->get_sections();

		if ( !empty( $sections ) ) {
			// Select first section if empty
				
			$_sections = $sections;
			reset( $_sections );
			return key( $_sections );
		}

		return '';
	}

	public function output() 
	{
		
	}

	public function save() 
	{
	}

	public function add_settings_page( $pages ) 
	{
		$pages[ $this->id ] = $this->label;
		return $pages;
	}
}