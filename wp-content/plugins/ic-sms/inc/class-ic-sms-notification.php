<?php

if ( !defined( 'WPINC' ) ) {
	
	die();
}

if ( !class_exists( 'IC_SMS_Notification' ) ) :

class IC_SMS_Notification extends IC_SMS_Post_Type
{
	const POST_TYPE = 'ic-sms-notification';

	/**
	 * The message of the notification
	 *
	 * @access protected
	 * @var string
	 */
	protected $message;

	/**
	 * The collection of the ids of the subscribers
	 *
	 * @access protected
	 * 
	 * @var array
	 */
	protected $_recipients_id  = array();

	/**
	 * __construct()
	 *
	 * The Constructor
	 *
	 * @access public
	 * 
	 * @param integer $id  The ID of the notification
	 * @return IC_SMS_Notification
	 */
	public function __construct( $id = 0 )
	{
		parent::__construct( $id );

		if ( $this->is_new() === false ) {
			
			$this->set_message( 	ic_sms_get_notification_message( $this->get_id() 		) );
			$this->_recipients_id = ic_sms_get_notification_recipients_id( $this->get_id() );
		}
	}

	/**
	 * add_recipient()
	 *
	 * This adds a recipient to the message
	 *
	 * @access public
	 * 
	 * @param int    $recipient_id  The id of the recipient for adding group "group_ID" for subscriber "subscriber_ID"
	 * @return void
	 */
	public function add_recipient_id( $recipient_id  )
	{
		$this->_recipients_id[] = $recipient_id;
	}


	/**
	 * send()
	 *
	 * This will send the notification via SMS
	 *
	 * @access public
	 * 
	 * @return void
	 */
	public function send()
	{
		ic_sms_broadcast_notification( $this->get_id() );
	}


	// OVERIDE FROM PARENT CLASS
	public function save()
	{
		$notification_id = parent::save();

		ic_sms_update_notification_message( $notification_id, $this->get_message() );
		ic_sms_update_notification_recipients_id( $notification_id, $this->_recipients_id );

		return $notification_id;
	}

	// OVERIDE from the parent class
	protected static function get_post_type()
	{
		return self::POST_TYPE;
	}
}

endif; // End class exists check