<?php

/**
 * ic_sms_get_notification_message()
 * 
 * This gets the message of the notification
 * 
 * @param  int 		$notification_id
 * @return string
 */
function ic_sms_get_notification_message( $notification_id )
{
	return get_post_meta( $notification_id, 'ic_sms_notification_message', true );
}



/**
 * ic_sms_get_notification_subscribers()
 * 
 * This gets all subscribers of the notification
 * 
 * @param  int 		$notification_id
 * @return string
 */
function ic_sms_get_notification_subscribers_id( $notification_id ) 
{
	$recipients_id 	= ic_sms_get_notification_recipients_id( $notification_id );

	$subscribers_id = array();

	foreach( $recipients_id as $recipient_id ) {

		// Group
		if ( strpos( $recipient_id, 'group_' ) !== false ) {

			$_group_id = str_replace( 'group_' , '', $recipient_id);
			$group_id = trim( $_group_id );
			$group_id = (int) $group_id;


			$query = IC_SMS_Subscriber::query( array(
				'posts_per_page' => -1,
				'fields'    => 'ids',
				'tax_query' => array(
					array(
						'taxonomy' => IC_SMS_Group::TAXONOMY,
						'terms' => $group_id
					)
				)
			) );

			foreach( $query->posts as $id ) {

				$subscribers_id[$id] = $id;
			}
		} 
		// Subscriber
		else if( strpos( $recipient_id, 'subscriber_' ) !== false ) {

			$_sid = str_replace( 'subscriber_' , '', $recipient_id);
			$sid = trim( $_sid );
			$sid = (int) $sid;

			$subscribers_id[$sid] = $sid;
		}
	}

	return $subscribers_id;
}

/**
 * ic_sms_get_notification_recipients_id()
 * 
 * This gets the recipients of the notification
 * 
 * @param  int 		$notification_id
 * @return string
 */
function ic_sms_get_notification_recipients_id( $notification_id )
{
	$value = get_post_meta( $notification_id, 'ic_sms_notification_recipients_id', true );

	if ( !is_array( $value ) ) {
		
		$value = array( $value );
	}

	return $value;
}

/**
 * ic_sms_update_notification_recipients_id()
 * 
 * This updates the message of the notification
 * 
 * @param  int 		$notification_id
 * @param  array 	$recipients_id
 * @return string
 */
function ic_sms_update_notification_recipients_id( $notification_id, $recipients_id )
{
	if ( !is_array( $recipients_id ) ) {
		
		return;
	}

	return update_post_meta( $notification_id, 'ic_sms_notification_recipients_id', $recipients_id );
}


/**
 * ic_sms_update_notification_message()
 * 
 * This updates the message of the notification
 * 
 * @param  int 		$notification_id
 * @param  string 	$message
 * @return string
 */
function ic_sms_update_notification_message( $notification_id, $message )
{
	$sanitize_message = $message;

	return update_post_meta( $notification_id, 'ic_sms_notification_message', $sanitize_message );
}


/**
 * ic_sms_get_notification_ajax_view_subscribers_link()
 * 
 * This gets the table of subscribers in the notification
 * 
 * @param  int 		$notification_id
 * @return string
 */
function ic_sms_get_notification_ajax_view_subscribers_link( $notification_id ) 
{
	$params = array(
		'action' => 'ic_sms/view_notification_subscribers',
		'nonce' => wp_create_nonce( 'ic_sms_nonce' ),
		'notification_id' => $notification_id
	);

	return esc_url(add_query_arg( $params, admin_url( 'admin-ajax.php' ) ));
}


/**
 * ic_sms_broadcast_notification()
 * 
 * This function will send the message of the notification to
 * its recipients and add logs 
 * 
 * @param  int 		$notification_id
 * @return string
 */
function ic_sms_broadcast_notification( $notification_id )
{
	// Make sure the notification status is publish before sending
	if ( get_post_status( $notification_id ) !== 'publish' ) {
		
		return;
	}

	$gateway = ic_sms()->gateway_manager()->current_gateway();

	$connection_success = false;
	$error_message 		= '';
	$error_code         = '';
	$logs 				= array();
	$message_logs 		= array();
	$all_sent 			= false;
	$total_sent_messages = 0;
	$number_sent 	= 0;

	// Check if there is a gateway
	if ( is_wp_error( $gateway ) ) {

		$error_code 	= $gateway->get_error_code();
		$error_message 	= $gateway->get_error_message();
	} else {

		// Connect to the SMS Gateway
		try {
			
			$gateway->connect();
			$connection_success = true;

		} catch (Exception $e) {
			
			$connection_success = false;
			$error_message = $e->getMessage();
			$error_code = 'no_gateway';
		}

		// Check if connection is success
		if ( $connection_success ) {

			// Get the subscribers of the notification
			$subscribers_id 	= ic_sms_get_notification_subscribers_id( $notification_id );

			// Message of the notification converted to Ascii
			$message  			= Patchwork\Utf8::toAscii(ic_sms_get_notification_message( $notification_id ));

			foreach( $subscribers_id as $subscriber_id ) {

				// Numbers of the subscriber
				$numbers 			= ic_sms_get_subscriber_numbers( $subscriber_id );
				// Replace the notification message if there are shortcodes in the message
				$process_message 	= ic_sms_get_subscriber_message( $subscriber_id, $message );

				// Loop subscriber numbers
				foreach( $numbers as $number ) {

					$total_sent_messages++;

					$sent 		= false;
					$message_error 	= '';

					try {
						
						$sent = true;
						$gateway->send( $number, $process_message );

					} catch (Exception $e) {
						
						$sent 		= false;
						$message_error = $e->getMessage();
					}

					// Increment the number of sent messages
					if ( $sent ) {
						
						$number_sent++;
					}

					// For reporting add each number of the subscriber
					$message_logs[] = array(
						'subscriber_id' => $subscriber_id,
						'sent' 			=> $sent,
						'error' 		=> $message_error,
						'number'		=> $number
					);
				}
			}

			$all_sent = $total_sent_messages == $number_sent;

			if ( !$all_sent ) {
				
				$error_code 	= 'not_all_sent';
				$error_message 	= 'Messages not all sent';
			}

		}		
	}
		
	$logs['total_sent'] 	= $number_sent;
	$logs['all_sent'] 		= $all_sent;
	$logs['total_messages'] = $total_sent_messages;
	$logs['success'] 		= intval($connection_success);
	$logs['error'] 			= $error_message;
	$logs['error_code'] 	= $error_code;
	$logs['gateway'] 		= ic_sms()->gateway_manager()->get_current_gateway_name();
	$logs['message_logs'] 	= $message_logs;

	$gateway->disconnect();

	update_post_meta( $notification_id, 'ic_sms_notification_logs', $logs );
}



function ic_sms_get_notification_logs( $notification_id )
{
	$default = array(
		'success' => false,
		'gateway' => '',
		'message_logs' => array(),
		'error_code' => '',
		'error' => '',
		'total_sent' => 0,
		'total_messages' => 0,
		'all_sent' => 0
	);

	$r = get_post_meta( $notification_id, 'ic_sms_notification_logs', true );

	if ( !is_array( $r ) ) {
		
		$r = array();
	}

	return array_merge($default, $r);
}



function ic_sms_get_notification_subscriber_number_log( $notification_id, $subscriber_id, $number )
{
	static $logs = null;

	if ( $logs == null ) {
		
		$logs = ic_sms_get_notification_logs( $notification_id );
	}

	foreach( $logs['message_logs'] as $message_log ) {

		if ( $subscriber_id == $message_log['subscriber_id'] && $number == $message_log['number'] ) {
			
			return $message_log;
		}
	}

	return array( 'sent' => 0, 'error' => '', 'number' => '', 'subscriber_id' => '' );
}