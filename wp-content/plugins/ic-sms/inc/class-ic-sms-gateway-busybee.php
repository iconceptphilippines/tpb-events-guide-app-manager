<?php

if( !defined( 'WPINC' ) ) {

	die();
}

if( !class_exists( 'IC_SMS_Gateway_BusyBee' ) ) :

class IC_SMS_Gateway_BusyBee extends IC_SMS_Gateway
{
	/**
	 * @overide from parent
	 * @var array
	 */
	public $title 	= 'Busy Bee';


	/**
	 * Api endpoint
	 * @var string
	 */
	private $endpoint = 'http://203.124.96.62/app/smsapi/index.php';


	/**
	 * @overide from parent
	 * @var array
	 */
	public $id 		= 'busybee';

	/**
	 * Holds all the numbers where the message will be sent to
	 * @var array
	 */
	private $send_to_numbers = array();

	/**
	 * Holds the message
	 * @var string
	 */
	private $message_to_send = '';

	/**
	 * @overide from parent
	 * @var array
	 */
	public $form_fields = array(
		'sender_id' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Sender ID',
			'description' 	=> 'You can find this on your Busy Bee account'
		),
		'api_key' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'API Key',
			'description' 	=> 'You can find this on your Busy Bee account'
		)
	);

	/**
	 * connect()
	 *
	 * This is called before we can call send() method
	 * IF NOT USE JUST OVERIDE IT AND PUT NOTHING
	 *
	 * This will be called so if your Gateway has a Library 
	 * the connection will not be on loop when sending messages
	 * to multiple contacts
	 *
	 * Just throw an exception if there is an error in your gateway
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @return boolean | WP_Error   True if successfully sent and must return WP_Error if there are errors
	 */
	public function connect() 
	{
		// NO NEED FOR THIS CHIKKA DOESNT HAVE A LIBRARY
		// Reset send to number
		$this->send_to_numbers = array();
	}

	/**
	 * send()
	 *
	 * Sends a message
	 *
	 * Just throw an exception if there is an error in your message
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @param  string $to      		The phone number string
	 * @param  string $message 		The message
	 */
	public function send( $to, $message )
	{
		$this->send_to_numbers[] = $to;
		$this->message_to_send = $message;
	}

	private function busyBeeSend($to, $message)
	{
		// Credentials
		$sender_id = $this->get_option( 'sender_id' );
		$api_key   = $this->get_option( 'api_key' );

		// Message details
		$send_to_number      = str_replace('+', '', $to);
		$url_encoded_message = urlencode($message);

		// Start 
		$params = "?key={$api_key}&senderid={$sender_id}&contacts={$send_to_number}&msg={$url_encoded_message}";

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $this->endpoint . $params);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		curl_exec($curl);
	}

	public function disconnect() 
	{
		if (!empty($this->send_to_numbers)) {
			$numbers = array_filter($this->send_to_numbers);
			$groupByHundred = array_chunk($numbers, 100);

			foreach($groupByHundred as $group) {
				$numbers_string = implode(',', $group);
				$this->busyBeeSend($numbers_string, $this->message_to_send);
			}
		}
	}

}

return new IC_SMS_Gateway_BusyBee();

endif;// end class exists check
