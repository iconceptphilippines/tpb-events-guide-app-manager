<?php

if( !defined( 'WPINC' ) ) {

	die();
}


if( !class_exists( 'IC_SMS_Gateway_Manager' ) ) :

class IC_SMS_Gateway_Manager
{
	/**
	 * This is the collection of gateways
	 *
	 * @access protected
	 * 
	 * @var array
	 */
	protected $gateways = array();


	/**
	 * Holds the shared instance of this class
	 *
	 * @static
	 * @access private
	 * 
	 * @var null
	 */
	private static $_instance 	= null;


	/**
	 * __construct()
	 *
	 * The constructor
	 *
	 * Make this private to prevent creation of multiple instance
	 * 
	 * @access private
	 * 
	 * @return void
	 */
	private function __construct()
	{
		// Abstract file
		include_once IC_SMS_INC . '/class-ic-sms-gateway.php';

		$this->gateways['IC_SMS_Gateway_Twilio'] = include( IC_SMS_INC . '/class-ic-sms-gateway-twilio.php' );
		$this->gateways['IC_SMS_Gateway_Chikka'] = include( IC_SMS_INC . '/class-ic-sms-gateway-chikka.php' );
		$this->gateways['IC_SMS_Gateway_BusyBee'] = include( IC_SMS_INC . '/class-ic-sms-gateway-busybee.php' );

		$this->gateways = apply_filters( 'ic_sms/gateways', $this->gateways );
	}


	/**
	 * current_gateway()
	 *
	 * Gets the current activated gateway
	 *
	 * @access public
	 * 
	 * @return mixed      IC_SMS_Gateway for success | WP_Error for error
	 */	
	public function current_gateway()
	{
		$current_gateway = get_option( 'ic_sms_gateway', '' );

		foreach( $this->gateways as $k => $v ) {

			if ( strtolower( $k ) == $current_gateway ) {
				
				return $v;
			}
		}

		return new WP_Error( 'no_gateway', 'No active gateway found. Please select your gateway <a href="'. admin_url( 'edit.php?post_type='. IC_SMS_Notification::POST_TYPE . '&page=ic-sms-settings' ) .'">here</a>.' );
	}


	/**
	 * gateways()
	 *
	 * Gets all the gateways		
	 *
	 * @access public
	 *  
	 * @return array      The collection of gateways
	 */
	public function gateways()
	{
		return $this->gateways;
	}


	/**
	 * has_gateways()
	 *
	 * Checks if there are gateways available
	 *
	 * @access public
	 * 
	 * @return boolean
	 */
	public function has_gateways()
	{
		return COUNT( $this->gateways ) > 0;
	}


	/**
	 * get_gateway_selection()
	 *
	 * Gets the all the gateways with their class names as key an title as value
	 *
	 * @access public
	 * 
	 * @return array
	 */
	public function get_gateway_selection()
	{
		$items = array();

		foreach( $this->gateways() as $gateway_class_name => $gateway ) {

			$items[ strtolower($gateway_class_name) ] = $gateway->title;
		}

		return $items;
	}


	/**
	 * get_current_gateway_name()
	 *
	 * Gets the name current gateway activated
	 *
	 * @access public
	 * 
	 * @return string
	 */
	public function get_current_gateway_name()
	{
		return get_option( 'ic_sms_gateway', '' );
	}


	/**
	 * instance()
	 *
	 * Gets the shared instance of this class
	 *
	 * @static
	 * @access public
	 * 
	 * @return IC_SMS_Gateway_Manager
	 */
	public static function instance()
	{
		if ( is_null( self::$_instance ) ) {
			
			self::$_instance = new self();
		}

		return self::$_instance;
	}
}

endif;