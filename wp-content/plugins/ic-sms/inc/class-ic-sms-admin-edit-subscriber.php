<?php

if ( !defined( 'WPINC' ) ) {

	die();
}

if( !class_exists( 'IC_SMS_Admin_Edit_Subscriber' ) ) :

class IC_SMS_Admin_Edit_Subscriber 
{
	/**
	 * updated_message()
	 *
	 * Customise the subscriber update message
	 * instead of 'Post updated' make it 'Subscriber updated.';		
	 *
	 * @static
	 * @access public
	 * 
	 * @param  array $messages
	 * @return array
	 */
	public static function updated_message( $messages )
	{
		global $post_type;

		// For contact post type only
		if ( $post_type == IC_SMS_Subscriber::POST_TYPE ) {
			
			// Custom message on number 8
			$messages[ $post_type ][1] = 'Subscriber updated.';
			$messages[ $post_type ][6] = 'Subscriber added.';
		}

		return $messages;
	}

	/**
	 * save()
	 *
	 * Save the numbers & subscriber details
	 *
	 * @static
	 * @access public
	 * 
	 * @param  int $subscriber_id 
	 * @return void
	 */
	public static function save( $subscriber_id )
	{
		global $post_type;

		// Get the numbers from the textarea from the $_POST global variable
		$numbers_string = filter_input( INPUT_POST, 'ic_sms_subscriber_numbers' );

		// Check the input if its truly a string
		// don't want arrays to this
		if ( is_string( $numbers_string ) ) {

			$numbers = explode( PHP_EOL, $numbers_string );
			ic_sms_update_subscriber_numbers( $subscriber_id, $numbers );
		}

		// Check if the first name is set
		if ( $first_name = filter_input( INPUT_POST, 'ic_sms_subscriber_first_name' ) ) {
			
			ic_sms_update_subscriber_first_name( $subscriber_id, $first_name );
		}

		// Check if the last name is set
		if ( $last_name = filter_input( INPUT_POST, 'ic_sms_subscriber_last_name' ) ) {
			
			ic_sms_update_subscriber_last_name( $subscriber_id, $last_name );
		}

		// Check if the middle name is set
		if ( $middle_name = filter_input( INPUT_POST, 'ic_sms_subscriber_middle_name' ) ) {
			
			ic_sms_update_subscriber_middle_name( $subscriber_id, $middle_name );
		}

		// Check if the email is set
		if ( $email = filter_input( INPUT_POST, 'ic_sms_subscriber_email' ) ) {
			
			ic_sms_update_subscriber_email( $subscriber_id, $email );
		}

		$birthday      = filter_input( INPUT_POST , 'ic_sms_subscriber_birthday');
		$birthday_data = explode('-', $birthday);
		$day           = '';
		$year          = '';
		$month         = '';

		if (count($birthday_data) == 3) {
			$year  = $birthday_data[0];
			$month = $birthday_data[1];
			$day   = $birthday_data[2];
		}

		update_post_meta( $subscriber_id, 'ic_sms_subscriber_birthday_year', $year );
		update_post_meta( $subscriber_id, 'ic_sms_subscriber_birthday_day', $day );
		update_post_meta( $subscriber_id, 'ic_sms_subscriber_birthday_month', $month );
	}

	/**
	 * change_textbox_placeholder()
	 *
	 * This will change the default placeholder
	 * of the text title input from 'Enter title here'
	 * to 'Contact name here'
	 *
	 * @static
	 * @access public
	 * 
	 * @param  string $title  - The placeholder of the text input
	 * @return string
	 */
	public static function change_textbox_placeholder( $title )
	{
		global $post_type;

		// Just change it when screen is in Contact custom post type
		if ( $post_type == IC_SMS_Subscriber::POST_TYPE ) {
			
			return 'Subscriber name here';
		}

		return $title;
	}

	/**
	 * metaboxes()
	 * 
	 * Adds a metabox in the contacts edit
	 *
	 * @static
	 * @access public
	 * 
	 * @param  int|object $post
	 * @return void
	 */
	public static function metaboxes()
	{
		add_meta_box( 'ic-sms-subscriber-details-metabox', 'Subscriber Details', array( __CLASS__, 'details_meta_box_content' ), IC_SMS_Subscriber::POST_TYPE, 'normal' );
		add_meta_box( 'ic-sms-subscriber-numbers-metabox', 'Numbers', array( __CLASS__, 'numbers_meta_box_content' ), IC_SMS_Subscriber::POST_TYPE, 'normal' );
	}


	/**
	 * details_meta_box_content()
	 *
	 * This is the metabox content of the Subscriber Details metabox
	 *
	 * @static
	 * @access public
	 * 
	 * @param  object $post The current post object being edited
	 * @return void
	 */
	public static function details_meta_box_content( $post )
	{
		$post_id = isset( $post->ID ) ? $post->ID : 0;

		// Get data
		$first_name     = ic_sms_get_subscriber_first_name( $post_id );
		$last_name      = ic_sms_get_subscriber_last_name( $post_id );
		$middle_name    = ic_sms_get_subscriber_middle_name( $post_id );
		$email          = ic_sms_get_subscriber_email( $post_id );
		$birthday_year  = get_post_meta( $post_id, 'ic_sms_subscriber_birthday_year', true );
		$birthday_month = get_post_meta( $post_id, 'ic_sms_subscriber_birthday_month', true );
		$birthday_day   = get_post_meta( $post_id, 'ic_sms_subscriber_birthday_day', true );
		$birthday 		= '';

		if ( !empty( $birthday_year ) 
			&& !empty( $birthday_month )
			&& !empty( $birthday_day )) {
			$birthday = $birthday_year . '-' . $birthday_month . '-' . $birthday_day;
		}

		?>
		<table class="form-table">
			<tbody>
				<tr>
					<th><label>First Name</label><span class="ic-sms-required">*</span></th>
					<td>
						<input name="ic_sms_subscriber_first_name" value="<?php echo esc_attr( $first_name ); ?>" required class="widefat" type="text" placeholder="Enter first name here..."/>
					</td>
				</tr>
				<tr>
					<th><label>Last Name</label><span class="ic-sms-required">*</span></th>
					<td>
						<input name="ic_sms_subscriber_last_name" value="<?php echo esc_attr( $last_name ); ?>" required class="widefat" type="text" placeholder="Enter last name here..."/>
					</td>
				</tr>
				<tr>
					<th><label>Middle Name/ Middle Initial</label></th>
					<td>
						<input name="ic_sms_subscriber_middle_name" value="<?php echo esc_attr( $middle_name ); ?>" class="widefat" type="text" placeholder="Enter middle name here..."/>
					</td>
				</tr>
				<tr>
					<th><label>Email</label></th>
					<td>
						<input name="ic_sms_subscriber_email" value="<?php echo esc_attr( $email ); ?>" class="widefat" type="email" placeholder="Enter email here..."/>
					</td>
				</tr>
				<tr>
					<th><label>Birthday</label></th>
					<td>
						<input type="text" class="js-datepicker" name="ic_sms_subscriber_birthday" value="<?php echo esc_attr( $birthday ) ?>">
					</td>
				</tr>
			</tbody>
		</table>
		<?php
	}

	/**
	 * numbers_meta_box_content()
	 *
	 * this will be the content of the numbers metabox
	 *
	 * @static
	 * @access public
	 *         
	 * @param  int|object $post
	 * @return void
	 */
	public static function numbers_meta_box_content( $post )
	{
		$post_id = isset( $post->ID ) ? $post->ID : 0;

		?>
		<p class="description">
			Enter the numbers of this subscriber<br/>
			One line per number<br/>
			For number format make it country code first with the '+' plus sign.
		</p>
		<textarea required placeholder="+63912345678" name="ic_sms_subscriber_numbers" class="widefat ic-sms-numbers-textbox" rows="10"><?php echo ic_sms_get_subscriber_numbers_string( $post_id ) ?></textarea>
		<?php
	}
}

endif; // End class exists check