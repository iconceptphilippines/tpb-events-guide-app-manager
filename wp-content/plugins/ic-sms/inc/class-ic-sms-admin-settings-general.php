<?php

class IC_SMS_Admin_Settings_General extends IC_SMS_Admin_Settings_Page
{
	public function __construct()
	{
		$this->id 		= 'general';
		$this->label 	= 'General';

		parent::__construct();
	}

	public function output()
	{
		$gateway_manager     = ic_sms()->gateway_manager();
		$current_gateway     = $gateway_manager->get_current_gateway_name();
		$selection_gateways  = $gateway_manager->get_gateway_selection();
		$has_gateways        = $gateway_manager->has_gateways();
		$birthday_greet_time = get_option('ic_sms_birthday_greet_time', '08:00');
		$birthday_message    = get_option('ic_sms_birthday_message', 'Happy Birthday!');
		$enabled_greeting 	 = get_option('ic_sms_enable_automated_greeting');

		?>
		<table class="form-table">
			<tbody>
			
				<tr>
					<th>
						<label>Gateway</label>
					</th>
					<td>
						
						<?php if( $has_gateways ): ?>

						<fieldset>
							<legend class="screen-reader-text"><span>Gateway</span></legend>
							<select required name="ic_sms_gateway">
								
								<option value="">- Select a gateway -</option>

								<?php foreach( $selection_gateways as $k => $v ): ?>
								
									<option <?php selected( $k, $current_gateway ) ?> value="<?php echo esc_attr( $k ) ?>"><?php echo $v; ?></option>
								
								<?php endforeach; ?>

							</select>
							<p class="description">Select the SMS Gateway you want to use by default.</p>
						</fieldset>

						<?php else: ?>
						
						<p class="description">No registered gateways found.</p>

						<?php endif; ?>
					</td>
				</tr>
				
				<tr>
					<th>
						<label>Automated Birthday Greeting</label>
					</th>
					<td>
						<fieldset>
							<!-- <input type="hidden" name="ic_sms_enable_automated_birthday_greeting" value="0"> -->
							<input <?php checked( true, $enabled_greeting ); ?> id="birthday-checkbox" type="checkbox" name="ic_sms_enable_automated_birthday_greeting" value="1"> <label for="birthday-checkbox">Check to enable</label>
						</fieldset>
					</td>
				</tr>

				<tr>
					<th>
						<label>Birthday Message</label>
					</th>
					<td>
						<fieldset>
							<textarea required="required" name="ic_sms_birthday_message" cols="40" rows="10"><?php echo esc_html($birthday_message) ?></textarea>
						</fieldset>
					</td>
				</tr>
	
				<tr>
					<th>
						<label>Birthday Time Greet</label>
					</th>
					<td>
						<fieldset>
							<select name="ic_sms_birthday_greet_time" required="required">
								<?php foreach( array(
									'01:00',
									'02:00',
									'03:00',
									'04:00',
									'05:00',
									'06:00',
									'07:00',
									'08:00',
									'09:00',
									'10:00',
									'11:00',
									'12:00',
									'13:00',
									'14:00',
									'15:00',
									'16:00',
									'17:00',
									'18:00',
									'19:00',
									'20:00',
									'21:00',
									'22:00',
									'23:00',
									'24:00',
								) as $v): ?>
								<option value="<?php echo esc_attr( $v ) ?>" <?php selected( $v, $birthday_greet_time ); ?>><?php echo $v; ?></option>
								<?php endforeach; ?>
							</select>
						</fieldset>
					</td>
				</tr>
			</tbody>
		</table>
		<?php
	}

	public function save()
	{
		$gateway             = filter_input( INPUT_POST, 'ic_sms_gateway' );
		$birthday_message    = filter_input( INPUT_POST , 'ic_sms_birthday_message' );
		$birthday_greet_time = filter_input( INPUT_POST , 'ic_sms_birthday_greet_time' );
		$enabled_greeting    = (bool)filter_input( INPUT_POST , 'ic_sms_enable_automated_birthday_greeting' );

		if ( !is_null( $gateway ) ) {
			
			update_option( 'ic_sms_gateway', $gateway );
		}

		if ( !is_null( $birthday_message ) ) {
			
			update_option( 'ic_sms_birthday_message', $birthday_message );
		}

		if ( !is_null( $birthday_greet_time ) ) {
			
			update_option( 'ic_sms_birthday_greet_time', $birthday_greet_time );

			// Change scheduled hook
			ic_sms_cron_set_greeting_time( get_option( 'ic_sms_birthday_greet_time' ) );
		}

		// Clear schedule if not enabled
		if (!$enabled_greeting) {
			
			ic_sms_cron_remove_scheduled_greeting();
		}

		update_option( 'ic_sms_enable_automated_greeting', $enabled_greeting );
	}
}

return new IC_SMS_Admin_Settings_General();