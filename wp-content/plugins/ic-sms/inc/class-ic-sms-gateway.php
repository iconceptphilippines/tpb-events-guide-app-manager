<?php

if ( !defined( 'WPINC' ) ) {

	die();
}


if ( !class_exists( 'IC_SMS_Gateway' ) ) :

abstract class IC_SMS_Gateway extends IC_SMS_Settings_API
{
	/**
	 * Max Characters of the message
	 *
	 * Leave it -1 to so it will not have a limit
	 *
	 * This depends on the Gateway API
	 * 
	 * @access public
	 * 
	 * @var integer
	 */
	public $maximum_message_characters = -1;


	/**
	 * connect()
	 *
	 * This is called before we can call send() method
	 * IF NOT USE JUST OVERIDE IT AND PUT NOTHING
	 *
	 * This will be called so if your Gateway has a Library 
	 * the connection will not be on loop when sending messages
	 * to multiple contacts
	 *
	 * Just throw an exception if there is an error in your gateway
	 *
	 * @abstract 
	 * @access public
	 * 
	 * @return void
	 */
	abstract public function connect();

	/**
	 * send()
	 *
	 * Sends a message
	 *
	 * Just throw an exception if there is an error in your message
	 *
	 * @abstract 
	 * @access public
	 * 
	 * @param  string $to      		The phone number string
	 * @param  string $message 		The message
	 */
	abstract public function send( $to, $message );

	abstract public function disconnect();
}

endif;