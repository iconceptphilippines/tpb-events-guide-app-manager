<?php

if( !defined( 'WPINC' ) ) {

	die();
}

if( !class_exists( 'IC_SMS_Gateway_Twilio' ) ) :

class IC_SMS_Gateway_Twilio extends IC_SMS_Gateway
{

	/**
	 * @overide from parent
	 * @var array
	 */
	public $title 	= 'Twilio';


	/**
	 * @overide from parent
	 * @var array
	 */
	public $id 		= 'twilio';


	/**
	 * @overide from parent
	 * @var array
	 */
	public $form_fields = array(
		'number' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Twilio Number',
			'description' 	=> 'You can find this in your Twilio account. This will be the "FROM" number in the message.'
		),
		'sid' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Twilio Account SID',
			'description' 	=> 'You can find this in your Twilio account.'
		),
		'token' => array(
			'required' 		=> 1,
			'type' 			=> 'text',
			'title' 		=> 'Twilio Auth Token',
			'description' 	=> 'You can find this in your Twilio account.'
		)
	);


	/**
	 * Holds the instnce of the Twilio Client
	 *
	 * @access private
	 * @var null
	 */
	private $_service = null;


	/**
	 * connect()
	 *
	 * This is called before we can call send() method
	 * IF NOT USE JUST OVERIDE IT AND PUT NOTHING
	 *
	 * This will be called so if your Gateway has a Library 
	 * the connection will not be on loop when sending messages
	 * to multiple contacts
	 *
	 * Just throw an exception if there is an error in your gateway
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @return boolean | WP_Error   True if successfully sent and must return WP_Error if there are errors
	 */
	public function connect() 
	{
		require_once IC_SMS_DIR . '/vendor/twilio/Twilio.php';

		// Assign the service to reuse it to other function
		// This will throw an exception if there is an error
		$this->_service = new Services_Twilio($this->get_option( 'sid' ), $this->get_option( 'token' ));
	}


	/**
	 * send()
	 *
	 * Sends a message
	 *
	 * Just throw an exception if there is an error in your message
	 *
	 * @abstract parent
	 * @access public
	 * 
	 * @param  string $to      		The phone number string
	 * @param  string $message 		The message
	 */
	public function send( $to, $message )
	{
		// This will throw an exception if there is an error
		$this->_service->account->messages->sendMessage( $this->get_option( 'number' ), $to, $message );
	}

	public function disconnect() {}
}

return new IC_SMS_Gateway_Twilio();

endif; // End class exists check