<?php


class IC_SMS_Admin_Logs
{
	public static function output()
	{
		$files 	= glob( IC_SMS_DIR . '/logs/*.txt' );
		$logs 	= array();
		$log_file_name 	= '';
		$current_date = null;

		foreach( $files as $file ) {

			$file_name = basename($file);
			$date = substr( $file_name, (strpos( $file_name, '-' ) + 1) );
			$date = substr( $date, 0, strpos( $date, '.' ) );
			$logs[$file_name] = $date;

			if ( filter_input( INPUT_GET, 'date' ) == $file_name ) {
				
				$current_date = $date;
				$current_date_file = $file_name;
			}
		}

		if ( is_null( $current_date ) ) {
			
			$_logs = $logs;
			current( $_logs );
			$current_date_file = key( $_logs );
		}

		$log = IC_SMS_Logger::get_log( $current_date_file );

		?>
		<div class="wrap">
			<h2>Logs</h2>
			
			<p>
				<select class="widefat" id="ic-sms-logger-select">

					<?php foreach( $logs as $k => $v ): ?>

						<option value="<?php echo esc_attr( $k ) ?>" <?php selected( $current_date, $v ); ?>><?php echo $v; ?></option>

					<?php endforeach; ?>

				</select>
			</p>

			<div class="ic-sms-log-wrapper">
				
				<?php echo nl2br( $log ); ?>

			</div>

		</div>
		<?php
	}
}
