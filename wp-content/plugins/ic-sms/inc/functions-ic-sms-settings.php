<?php


function ic_sms_settings_get( $setting, $default = '' )
{
	return get_option( 'ic_sms_' . $setting, $default );
}


function ic_sms_settings_get_birthday_message($default = '')
{
	return ic_sms_settings_get( 'birthday_message', $default );
}


function ic_sms_settings_get_birthday_greet_time( $default = '' )
{
	return ic_sms_settings_get( 'birthday_greet_time', $default );
}