<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'IC_SMS_Admin_Notifications' ) ) :

class IC_SMS_Admin_Notifications
{
	/**
	 * row_actions()
	 *
	 * Removes the 'Quick Edit' action 
	 *
	 * @static
	 * @access public
	 * 
	 * @param  array $actions 
	 * @return array          
	 */
	public static function row_actions( $actions )
	{
		global $post;

		if ( get_post_type() == IC_SMS_Notification::POST_TYPE ) {
			
			// Removes the quick edit action
			unset( $actions['inline hide-if-no-js'] );

			// Changes the Edit to View if the notification is already publish
			if ( !empty( $post->post_status ) && $post->post_status == 'publish' ) {
								
				$actions['edit'] = sprintf( '<a title="Published Notifications can only be viewed but not edited." href="%s">%s</a>', get_edit_post_link( $post->ID ), 'View' );
			}
		}

		return $actions;
	}


	/**
	 * columns()
	 *
	 * This customizes the columns of the notification
	 *
	 * @static
	 * @access public
	 * 
	 * @param  array $columns   The columns of the notification
	 * @return array          
	 */
	public static function columns( $columns ) 
	{			
		// unset( $columns['date'] ); Remove date column
		unset( $columns['taxonomy-ic-sms-group'] ); // Remove taxonomy group column
		$columns['message'] = 'Message';
		$columns['sent'] = 'Sent Messages';
		$columns['no_recipients'] = 'No. of Recipients';
		$columns['status'] = 'Status';

		return $columns;
	}

	public static function bulk_actions( $actions ) 
	{
		unset( $actions['edit'] );

		return $actions;
	}

	/**
	 * custom_column()
	 *
	 * Customizes the column values of the notification
	 *
	 * @static
	 * @access public
	 * 
	 * @param  string $column  
	 * @param  int    $post_id 
	 * @return void
	 */
	public static function custom_column( $column, $post_id )
	{
		global $post;

		$notification_id = $post->ID;

		$logs = ic_sms_get_notification_logs( $notification_id );

		if ( $column == 'status' ) {

			$post_date = date('F d, Y h:i:s a', strtotime( $post->post_date ));
			
			switch ( $post->post_status ) {

				case 'future':
					
					echo sprintf( '<span class="ic-sms-notification-status ic-sms-notification-status-scheduled" title="%s"><i class="dashicons dashicons-clock" ></i> %s</span>', 'Scheduled at ' . $post_date,'Scheduled' );

					break;

				case 'publish':

					$status = 'Sent';
					$sent = $logs['success'];

					if ( $sent ) {
						
						$message = 'Sent';
						$error = '';

						if ( $logs['all_sent'] == false ) {
							
							$message = 'Not all sent';
							$error = 'Click no. of recipients for more info';
						}

						echo '<span class="ic-sms-notification-status ic-sms-notification-status-sent"><i class="dashicons dashicons-yes" ></i> '. $message .'</span>';
						echo '<br/>' . $error;
					} else {
						$error = $logs['error'];
						echo '<span class="ic-sms-notification-status"><i style="color: red;" class="dashicons dashicons-no"></i>Not sent</span>';
						echo "<br/>";
						echo $error;
					}

					break;
				
				default:
					echo 'send';
					break;
			}
		} else if( $column == 'message' ) {

			$s = '';

			$s .= '<span class="ic-sms-notification-message">';
				$s .= ic_sms_get_notification_message( $notification_id );
			$s .= '</span>';

			echo $s;

		} else if( $column == 'no_recipients' ) {

			$subscribers = ic_sms_get_notification_subscribers_id( $notification_id );

			$s = '';

			$s .= '<a href="#" onclick="tb_show(\''. get_the_title( $notification_id ) .' Notification Subscribers\',\''. ic_sms_get_notification_ajax_view_subscribers_link( $notification_id ) .'\')" title="Click to view recipients"><span class="ic-sms-notification-recipients-count">';
				$s .= COUNT($subscribers);
			$s .= '</span></a>';

			echo $s; 
		} else if( $column == 'sent' ) {

			echo $logs['total_sent'] . '/' . $logs['total_messages'];
		}
	}
}

endif; // end class exists check

