<?php

if ( !defined( 'WPINC' ) ) {
	
	die();
}


if ( !class_exists( 'IC_SMS_Admin_Settings' ) ) :

class IC_SMS_Admin_Settings
{
	/**
	 * Holds all the collection of settings page
	 *
	 * @static
	 * @access private
	 * 
	 * @var array
	 */
	private static $settings = array();

	/**
	 * Holds all the collection of errors
	 *
	 * @static
	 * @access private
	 * 
	 * @var array
	 */
	private static $errors   = array();


	/**
	 * Holds all the collection of messages page
	 *
	 * @static
	 * @access private
	 * 
	 * @var array
	 */
	private static $messages = array();


	/**
	 * get_settings_pages()
	 *
	 * Gets all the settings pages
	 *
	 * @static
	 * @access public
	 * 
	 * @return array
	 */
	public static function get_settings_pages()
	{
		if ( empty( self::$settings ) ) {
			
			$settings = array();

			include IC_SMS_INC . '/class-ic-sms-admin-settings-page.php';

			$settings[] = include IC_SMS_INC . '/class-ic-sms-admin-settings-general.php';
			$settings[] = include IC_SMS_INC . '/class-ic-sms-admin-settings-gateways.php';

			self::$settings = apply_filters( 'ic_sms_get_settings_pages', $settings );
		}

		return self::$settings;
	}

	public static function save()
	{
		global $current_tab;

		if ( empty( $_REQUEST['_wpnonce'] ) || !wp_verify_nonce( $_REQUEST['_wpnonce'], 'ic-sms-settings' ) ) {
			die( __( 'Action failed. Please refresh the page and retry.', 'woocommerce' ) );
		}

		// Trigger actions
		do_action( 'ic_sms_settings_save_' . $current_tab );
		do_action( 'ic_sms_update_options_' . $current_tab );
		do_action( 'ic_sms_update_options' );

		self::add_message( __( 'Your settings have been saved.', 'woocommerce' ) );

		do_action( 'woocommerce_settings_saved' );
	}

	public static function output()
	{
		global $current_tab, $current_section;

		$current_tab     = filter_input( INPUT_GET, 'tab' ) == null ? 'general' : sanitize_title( filter_input( INPUT_GET, 'tab' ) );
		$current_section = filter_input( INPUT_GET, 'section' ) == null ? '' : sanitize_title( filter_input( INPUT_GET, 'section' ) );

		self::get_settings_pages();

		// Save settings if data has been posted
		if ( ! empty( $_POST ) ) {
			
			self::save();
		}

		self::show_messages();

		// Get tabs for the settings page
		$tabs = apply_filters( 'ic_sms_settings_tabs_array', array() );

		?>
		
		<div class="wrap ic-sms">
			<form method="<?php echo esc_attr( apply_filters( 'ic-sms_settings_form_method_tab_' . $current_tab, 'post' ) ); ?>" id="mainform" action="" enctype="multipart/form-data">
				<div class="icon32 icon32-ic-sms-settings" id="icon-ic-sms"><br /></div><h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
					<?php
						foreach ( $tabs as $name => $label ) {
							echo '<a href="' . admin_url( 'edit.php?post_type='. IC_SMS_Notification::POST_TYPE .'&page=ic-sms-settings&tab=' . $name ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';
						}
					?>
				</h2>

				<?php
					do_action( 'ic_sms_sections_' . $current_tab );
					do_action( 'ic_sms_settings_' . $current_tab );
				?>

				<p class="submit">
					<input name="save" class="button-primary" type="submit" value="<?php _e( 'Save changes', 'ic-sms' ); ?>" />
					<input type="hidden" name="subtab" id="last_tab" />
					<?php wp_nonce_field( 'ic-sms-settings' ); ?>
				</p>
			</form>
		</div>

		<?php
	}

	/**
	 * Output messages + errors
	 * @return string
	 */
	public static function show_messages() 
	{
		if ( sizeof( self::$errors ) > 0 ) {
			foreach ( self::$errors as $error ) {
				echo '<div id="message" class="error fade"><p><strong>' . esc_html( $error ) . '</strong></p></div>';
			}
		} elseif ( sizeof( self::$messages ) > 0 ) {
			foreach ( self::$messages as $message ) {
				echo '<div id="message" class="updated fade"><p><strong>' . esc_html( $message ) . '</strong></p></div>';
			}
		}
	}

	public static function add_message( $text ) 
	{
		self::$messages[] = $text;
	}

	public static function add_error( $text ) 
	{
		self::$errors[] = $text;
	}
}

endif;