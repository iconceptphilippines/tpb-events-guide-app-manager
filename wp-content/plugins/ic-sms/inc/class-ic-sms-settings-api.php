<?php

if( !defined( 'WPINC' ) ) {

	die();
}


if( !class_exists( 'IC_SMS_Settings_API' ) ) :

class IC_SMS_Settings_API
{
	public $plugin_id 			= 'ic_sms_';
	public $id 					= '';
	public $title 				= '';
	public $errors 				= array();
	public $form_fields 		= array();
	public $settings 			= array();
	public $sanitized_fields 	= array();
	public $enabled 			= true;

	public function get_field_boolean( $key, $field )
	{
		$field['choices'] = array(
			'1' => 'Yes',
			'0' => 'No'
		);

		return $this->get_field_radio( $key, $field );
	}

	public function get_field_radio( $key, $field )
	{
		$choices = isset( $field['choices'] ) ? $field['choices'] : array();
		$choices = !is_array( $choices ) ? array() : $choices;
		$inline = isset($field['inline']) ? true : false;

		$field_key 	= $this->get_key( $key );
		$data 		= $this->field_defaults( $field );

		$input_attr = array(
			'class' 		=> 'input-text regular-input ' . $data['class'],
			'type'			=> 'radio',
			'name'			=> $field_key,
			'placeholder' 	=> $data['placeholder'],
		);

		if ( $data['required'] ) {
			
			$input_attr['required'] = 'required';
		}

		$input_attr = wp_parse_args( $input_attr, $data['attributes'] );
		$value 		= $this->get_option( $key );

		ob_start();

		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $data['id'] ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					
					<?php if( !empty($input_attr['required']) ): ?>

						<input type="hidden" name="<?php echo esc_attr( $input_attr['name'] ); ?>" value="">

					<?php endif; ?>
					
					<ul class="ic-sms-list <?php echo ($inline) ? 'ic-sms-list-inline' : ''; ?>">
					<?php foreach( $choices as $k => $v ): ?>

						<?php  
						$input_attr['value'] = $k;
						$input_attr['id'] = $input_attr['name'] . '-' . $k;

						?>
						
						<li>
							<input <?php checked( $value, $k ); ?> <?php $this->attributed_string( $input_attr ); ?> />
							<label for="<?php echo esc_attr($input_attr['id']); ?>"><?php echo $v; ?></label>
						</li>

					<?php endforeach; ?>
					</ul>

					<?php echo $this->get_description_html( $data ); ?>

				</fieldset>
			</td>
		</tr>
		<?php

		return ob_get_clean();
	}

	public function get_field_email( $key, $field )
	{
		$field['type'] = 'email';
		return $this->get_field_text( $key, $field );
	}

	public function get_field_textarea( $key, $field )
	{
		$field_key 	= $this->get_key( $key );
		$data 		= $this->field_defaults( $field );

		$input_attr = array(
			'class' 		=> 'input-text regular-input ' . $data['class'],
			'name'			=> $field_key,
			'id'			=> $data['id'],
			'placeholder' 	=> $data['placeholder']
		);

		if ( $data['required'] ) {
			
			$input_attr['required'] = 'required';
		}

		if ( isset( $input_attr['type'] ) ) {
			
			unset( $input_attr['type'] );
		}

		$input_attr = wp_parse_args( $input_attr, $data['attributes'] );

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $data['id'] ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					
					<textarea <?php $this->attributed_string( $input_attr ) ?> <?php echo disabled( $data['disabled'], true ) ?>><?php echo esc_textarea( $this->get_option( $key ) ); ?></textarea>
					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php

		return ob_get_clean();
	}

	public function get_field_number( $key, $field )
	{
		$field['type'] = 'number';

		if ( isset( $field['min'] ) ) {
			
			$field['attributes']['min'] = $field['min'];
		}

		if ( isset( $field['max'] ) ) {
			
			$field['attributes']['max'] = $field['max'];
		}

		if ( isset( $field['step'] ) ) {
			
			$field['attributes']['step'] = $field['step'];
		}

		return $this->get_field_text( $key, $field );
	}

	public function get_field_password( $key, $field )
	{
		$field['type'] = 'password';

		return $this->get_field_text( $key, $field );
	}

	public function get_field_text( $key, $field )
	{
		$field_key 	= $this->get_key( $key );
		$data 		= $this->field_defaults( $field );

		$input_attr = array(
			'class' 		=> 'input-text regular-input ' . $data['class'],
			'type'			=> $data['type'],
			'name'			=> $field_key,
			'id'			=> $data['id'],
			'placeholder' 	=> $data['placeholder'],
			'value'			=> $this->get_option( $key )
		);

		if ( $data['required'] ) {
			
			$input_attr['required'] = 'required';
		}

		$input_attr = wp_parse_args( $input_attr, $data['attributes'] );

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $data['id'] ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					
					<input <?php $this->attributed_string( $input_attr ) ?> <?php echo disabled( $data['disabled'], true ) ?> />
					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php

		return ob_get_clean();
	}

	public function html( $form_fields = array() ) 
	{
		if ( ! $form_fields ) {
			$form_fields = $this->get_form_fields();
		}

		$html = '<table class="form-table"><tbody>';

		foreach ( $form_fields as $k => $v ) {

			if ( ! isset( $v['type'] ) || ( $v['type'] == '' ) ) {
				$v['type'] = 'text'; // Default to "text" field type.
			}

			if ( method_exists( $this, 'get_field_' . $v['type'] ) ) {
				$html .= $this->{'get_field_' . $v['type']}( $k, $v );
			} else {
				$html .= $this->get_field_text( $k, $v );
			}
		}

		$html .= '</tbody></table>';

		echo $html;
	}

	public function get_key( $key )
	{
		return $this->plugin_id . $this->id . '_' . $key;
	}

	public function get_description_html( $data ) 
	{
		if ( $data['desc_tip'] === true ) {
			$description = '';
		} elseif ( ! empty( $data['desc_tip'] ) ) {
			$description = $data['description'];
		} elseif ( ! empty( $data['description'] ) ) {
			$description = $data['description'];
		} else {
			$description = '';
		}

		return $description ? '<p class="description">' . wp_kses_post( $description ) . '</p>' . "\n" : '';
	}

	public function attributed_string( $attributes, $echo = true )
	{
		if ( !is_array( $attributes ) ) {
		
			return;
		}

		$s = '';

		foreach ($attributes as $k => $v) {
			
			$s .= ' ' . esc_attr( $k ) . '="' . esc_attr( $v ) . '"';
		}

		if ( $echo ) {
			
			echo $s;
		}

		return $s;
	}

	public function field_defaults( $field )
	{
		$defaults = array(
			'required'			=> false,
			'title'             => '',
			'id'             	=> '',
			'disabled'          => false,
			'class'             => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'attributes' 		=> array()
		);

		return wp_parse_args( $field, $defaults );
	}

	public function init_settings()
	{
		$this->settings = get_option( $this->plugin_id . $this->id . '_settings', null );

		if ( ! $this->settings || ! is_array( $this->settings ) ) {

			$this->settings = array();

			// If there are no settings defined, load defaults
			if ( $form_fields = $this->get_form_fields() ) {

				foreach ( $form_fields as $k => $v ) {
					$this->settings[ $k ] = isset( $v['default'] ) ? $v['default'] : '';
				}
			}
		}

		if ( $this->settings && is_array( $this->settings ) ) {

			$this->settings = array_map( array( __CLASS__, 'format_settings' ), $this->settings );
			$this->enabled  = isset( $this->settings['enabled'] ) && $this->settings['enabled'] == 'yes' ? 'yes' : 'no';
		}
	}

	public function validate_text_field( $key ) 
	{
		$text = $this->get_option( $key );

		if ( isset( $_POST[ $this->get_key( $key ) ] ) ) {
			$text = wp_kses_post( trim( stripslashes( $_POST[ $this->get_key( $key ) ] ) ) );
		}

		return $text;
	}

	public function process_admin_options()
	{
		$this->validate_settings_fields();

		if ( count( $this->errors ) == 0 ) {

			update_option( $this->plugin_id . $this->id . '_settings', $this->sanitized_fields );
			$this->init_settings();
		}
	}

	public function validate_settings_fields( $form_fields = array() ) {

		if ( ! $form_fields ) {
			$form_fields = $this->get_form_fields();
		}

		$this->sanitized_fields = array();

		foreach ( $form_fields as $k => $v ) {

			if ( empty( $v['type'] ) ) {
				$v['type'] = 'text'; // Default to "text" field type.
			}

			// Look for a validate_FIELDID_field method for special handling
			if ( method_exists( $this, 'validate_' . $k . '_field' ) ) {
				$field = $this->{'validate_' . $k . '_field'}( $k );
				$this->sanitized_fields[ $k ] = $field;

			// Look for a validate_FIELDTYPE_field method
			} elseif ( method_exists( $this, 'validate_' . $v['type'] . '_field' ) ) {
				$field = $this->{'validate_' . $v['type'] . '_field'}( $k );
				$this->sanitized_fields[ $k ] = $field;

			// Default to text
			} else {
				$field = $this->{'validate_text_field'}( $k );
				$this->sanitized_fields[ $k ] = $field;
			}
		}
	}

	public static function format_settings( $value ) 
	{
		return is_array( $value ) ? $value : $value;
	}

	public function get_form_fields()
	{
		return $this->form_fields;
	}

	public function get_option( $key, $empty_value = null ) {

		if ( empty( $this->settings ) ) {
			$this->init_settings();
		}

		// Get option default if unset
		if ( ! isset( $this->settings[ $key ] ) ) {
			$form_fields            = $this->get_form_fields();
			$this->settings[ $key ] = isset( $form_fields[ $key ]['default'] ) ? $form_fields[ $key ]['default'] : '';
		}

		if ( ! is_null( $empty_value ) && empty( $this->settings[ $key ] ) ) {
			$this->settings[ $key ] = $empty_value;
		}

		return $this->settings[ $key ];
	}
}

endif; // end class exists check