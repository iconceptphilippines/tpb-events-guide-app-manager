<?php

// Prevent direct file access 
if ( !defined( 'WPINC' ) ) {
	
	die();
}

// Check if IC_SMS_Data class already exists
if( !class_exists( 'IC_SMS_Data' ) ):

/**
 * This class just registers all the post types and taxonomies needed
 */
class IC_SMS_Data
{
	/**
	 * register()
	 *
	 * Registers all the post types and taxonomies
	 *
	 * @static
	 * @access public
	 *
	 * @see IC_SMS _load_hooks - add_action( 'init', $function )
	 * 
	 * @return void
	 */
	public static function register()
	{
		// Notification Custom Post Type
		$labels = array(
			'name'                => __( 'Notifications', 'ic-sms' ),
			'singular_name'       => __( 'Notification', 'ic-sms' ),
			'add_new'             => _x( 'Create Notification', 'ic-sms', 'ic-sms' ),
			'add_new_item'        => __( 'Create Notification', 'ic-sms' ),
			'edit_item'           => __( 'View Notification', 'ic-sms' ),
			'new_item'            => __( 'New Notification', 'ic-sms' ),
			'view_item'           => __( 'View Notification', 'ic-sms' ),
			'search_items'        => __( 'Search Notifications', 'ic-sms' ),
			'not_found'           => __( 'No Notifications found', 'ic-sms' ),
			'not_found_in_trash'  => __( 'No Notifications found in Trash', 'ic-sms' ),
			'parent_item_colon'   => __( 'Parent Notification:', 'ic-sms' ),
			'menu_name'           => __( 'SMS', 'ic-sms' ),
			'all_items'		 	  => __( 'Notifications', 'ic-sms' )
		);
		
		$args = array(
			'labels'              => $labels,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => false,
			'menu_icon'			  => 'dashicons-admin-comments',
			'menu_position'       => 99,
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'supports'         	  => array('title'),
		);
		
		register_post_type( IC_SMS_Notification::POST_TYPE, $args );

		// Contact Custom Post Type
		$labels = array(
			'name'                => __( 'Contacts', 'ic-sms' ),
			'singular_name'       => __( 'Contact', 'ic-sms' ),
			'add_new'             => _x( 'Add New Contact', 'ic-sms', 'ic-sms' ),
			'add_new_item'        => __( 'Add New Contact', 'ic-sms' ),
			'edit_item'           => __( 'Edit Contact', 'ic-sms' ),
			'new_item'            => __( 'New Contact', 'ic-sms' ),
			'view_item'           => __( 'View Contact', 'ic-sms' ),
			'search_items'        => __( 'Search Contacts', 'ic-sms' ),
			'not_found'           => __( 'No Contacts found', 'ic-sms' ),
			'not_found_in_trash'  => __( 'No Contacts found in Trash', 'ic-sms' ),
			'parent_item_colon'   => __( 'Parent Contact:', 'ic-sms' ),
			'menu_name'           => __( 'SMS', 'ic-sms' ),
			'all_items'		 	  => __( 'Contacts', 'ic-sms' )
		);
		
		$args = array(
			'labels'              => $labels,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=' . IC_SMS_Notification::POST_TYPE,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'supports'         	  => array(''),
			'taxonomies'		  => array( IC_SMS_Group::TAXONOMY )
		);
		
		register_post_type( IC_SMS_Subscriber::POST_TYPE, $args );

		// Groups Custom Taxonomy
		$labels = array(
			'name'					=> _x( 'Groups', 'Taxonomy Groups', 'ic-sms' ),
			'singular_name'			=> _x( 'Group', 'Taxonomy Group', 'ic-sms' ),
			'search_items'			=> __( 'Search Groups', 'ic-sms' ),
			'popular_items'			=> __( 'Popular Groups', 'ic-sms' ),
			'all_items'				=> __( 'All Groups', 'ic-sms' ),
			'parent_item'			=> __( 'Parent Group', 'ic-sms' ),
			'parent_item_colon'		=> __( 'Parent Group', 'ic-sms' ),
			'edit_item'				=> __( 'Edit Group', 'ic-sms' ),
			'update_item'			=> __( 'Update Group', 'ic-sms' ),
			'add_new_item'			=> __( 'Add New Group', 'ic-sms' ),
			'new_item_name'			=> __( 'New Group Name', 'ic-sms' ),
			'add_or_remove_items'	=> __( 'Add or remove Groups', 'ic-sms' ),
			'choose_from_most_used'	=> __( 'Choose from most used ic-sms', 'ic-sms' ),
			'menu_name'				=> __( 'Groups', 'ic-sms' ),
		);
		
		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => true,
			'hierarchical'      => false,
			'show_tagcloud'     => true,
			'show_ui'           => true
		);
		
		register_taxonomy( IC_SMS_Group::TAXONOMY, array( IC_SMS_Subscriber::POST_TYPE, IC_SMS_Notification::POST_TYPE ), $args );
	}
}

endif;