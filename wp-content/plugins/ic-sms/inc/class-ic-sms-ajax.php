<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'IC_SMS_Ajax' ) ) :

class IC_SMS_Ajax
{
	/**
	 * @var array
	 */
	private $actions = array();


	/**
	 * @static
	 */
	private static $_instance;


	/**
	 * __construct()
	 *
	 * The constructor
	 *
	 * Make this private to prevent creation of multiple instance
	 * 
	 * @access private
	 * 
	 * @return void
	 */
	private function __construct()
	{
		$this->add_action( 'view_notification_subscribers', 'view_notification_subscribers' );
		$this->add_action( 'convert_to_ascii', 'convert_to_ascii' );
	}

	public function convert_to_ascii()
	{
		check_ajax_referer( 'ic_sms_nonce', 'nonce');

		$string = isset($_REQUEST['string']) ? $_REQUEST['string'] : '';

		if (empty($string)) {
			wp_send_json_error();
		}

		return wp_send_json_success( array(
			'asciiString' => Patchwork\Utf8::toAscii($string)
		) );
	}


	/**
	 * view_notification_subscribers()
	 *
	 * Views the list of the notification subscribers
	 * 
	 * @return void
	 */
	public function view_notification_subscribers()
	{
		// Use $_REQUEST to allow  post and get request
		$nonce = isset( $_REQUEST['nonce'] ) ? $_REQUEST['nonce'] : null;

		// Check and verify if the request is valid
		if ( !wp_verify_nonce( $nonce, 'ic_sms_nonce' ) ) {
			
			echo 'Invalid request.';
			die();
		}

		$params = array(
			'notification_id' => FILTER_VALIDATE_INT
		);

		$data = filter_var_array( $_REQUEST, $params );
		extract( $data );

		$logs 			= ic_sms_get_notification_logs( $notification_id );
		$subscribers_id = ic_sms_get_notification_subscribers_id( $notification_id );
		$has_subscribers = COUNT( $subscribers_id );
		$count = 1;
		?>
		
		<?php if( $has_subscribers ): ?>

			<table class="widefat" style="margin-top: 10px;border: 0;box-shadow: none;">
				<thead>
					<tr>
						<th style="width: 15px;"><strong>#</strong></th>
						<th><strong>Name</strong></th>
						<th><strong>Numbers</strong> (<i style="color: green;" class="dashicons dashicons-yes"></i> Sent | <i style="color: #f30;" class="dashicons dashicons-no"></i> Not Sent)</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach( $subscribers_id as $subscriber_id ): ?>
						
						<?php  
						$numbers = ic_sms_get_subscriber_numbers( $subscriber_id );
						?>

						<tr>	
							<td><?php echo $count; ?></td>
							<td><a href="<?php echo esc_url( get_edit_post_link( $subscriber_id ) ) ?>"><?php echo ic_sms_get_subscriber_full_name( $subscriber_id ) ?></a></td>
							<td>
								<?php foreach( $numbers as $number ): ?>
									
									<?php $log = ic_sms_get_notification_subscriber_number_log( $notification_id, $subscriber_id, $number ); ?>
									<?php $message = ''; ?>

									<?php if( $log['sent'] ): ?>
										<?php $message = 'Sent'; ?>
										<i title="Sent" style="color: green;" class="dashicons dashicons-yes"></i>
									<?php else: ?>
										<?php $message = $log['error']; ?>
										<i style="color: #f30;" title="<?php echo esc_attr( $log['error'] ) ?>" class="dashicons dashicons-no"></i>
									<?php endif; ?>
	
									<u title="<?php echo esc_attr( $message ) ?>"><?php echo $number; ?></u>

								<?php endforeach; ?>
							</td>
						</tr>

					<?php $count++;endforeach; ?>

				</tbody>
			</table>

		<?php else: ?>
			
			<p>No subscribers found.</p>

		<?php endif; ?>

		<?php
		die();
	}


	/**
	 * initialize()
	 *
	 * This will register all the hooks and setup
	 * needed to run the manager
	 *
	 * @access public
	 * 
	 * @return void
	 */
	public function initialize()
	{
		foreach( $this->actions as $v ) {

			$action = 'ic_sms/' . $v[0];
			$method = array( $this, $v[1] );

			add_action( 'wp_ajax_' . $action, $method );

			if ( $v[2] ) {
				
				add_action( 'wp_ajax_nopriv_' . $action, $method );
			}
		}
	}

	/**
	 * add_action()
	 *
	 * This registers an ajax action in WordPress
	 *
	 * @access private
	 * 
	 * @param string  $action 
	 * @param string  $method 
	 * @param boolean $public 
	 */
	private function add_action( $action, $method, $public = false )
	{
		$this->actions[$action] = array( $action, $method, $public );
	}


	/**
	 * instance()
	 *
	 * @static
	 * @access public
	 * 
	 * @return IC_SMS_Ajax
	 */
	public static function instance()
	{
		if ( self::$_instance == null ) {
			
			self::$_instance = new self();
		}

		return self::$_instance;
	}

}

endif; // end class exists check