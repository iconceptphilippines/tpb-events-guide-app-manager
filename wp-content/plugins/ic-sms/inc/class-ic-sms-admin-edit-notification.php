<?php

class IC_SMS_Admin_Edit_Notification
{
	/**
	 * new_title()
	 *
	 * This change the title input text box default value to the current date
	 *
	 * @static 
	 * @access public
	 *	        	
	 * @param  string $title The default title of the input box
	 * @return string        
	 */
	public static function new_title( $title )
	{
		// Bail early if this is not a notification post type
		if( !self::is_right_screen() ) {

			return $title;
		}

		return date('F d, Y');
	}


	/**
	 * remove_submitbox_on_publish()
	 *
	 * This just removes the submit metabox when a notification is already published
	 *
	 * @static 
	 * @access public
	 * 
	 * @return void
	 */
	public static function remove_submitbox_on_publish()
	{
		global $post;

		if ( (!empty( $post->post_type ) && !empty( $post->post_status )) && ($post->post_type == IC_SMS_Notification::POST_TYPE && $post->post_status == 'publish') ) {
			
			// Remove the publish metabox if the notification is already publish	
			remove_meta_box( 'submitdiv', IC_SMS_Notification::POST_TYPE, 'side' );
		}
	}


	/**
	 * remove_visibility_option()
	 *
	 * Removes the visibility dropdown in the submit metabox
	 *
	 * @static
	 * @access public
	 * 
	 * @return void
	 */
	public static function remove_visibility_option()
	{
		if ( !self::is_right_screen() ) {
			
			return;
		}

		?>
		<style>
		.submitbox #visibility { display: none; }
		.ic-sms-notes li {list-style: circle;}
		</style>
		<?php
	}


	/**
	 * is_right_screen()
	 *
	 * Determines whether we are in the edit post screen in the admin
	 *
	 * @static
	 * @access public
	 * 
	 * @return boolean 
	 */
	public static function is_right_screen()
	{
		return (filter_input( INPUT_GET, 'post_type' ) == IC_SMS_Notification::POST_TYPE) || get_post_type() == IC_SMS_Notification::POST_TYPE;
	}


	/**
	 * change_submitbox_texts()
	 *
	 * Changes some text in the submit metabox for much easy to understand texts
	 *
	 * @static
	 * @access public
	 * 
	 * @param  string $translated 
	 * @param  string $text       
	 * @param  string $domain     
	 * @return string
	 */
	public static function change_submitbox_texts( $translated, $text, $domain )
	{
		static $count = 0;

		if ( !self::is_right_screen() ) {
			
			return $translated;
		}

		if ( 'publish' !== strtolower($translated) ) {
			
			return $translated;
		}

		$count++;

		switch ( $count ) {
			case 4:
				
				$translated = 'Send';
				break;

			case 2:
				
				$translated = 'Actions';
				break;

			case 4:
				
				$translated = 'asd';
				break;
			
			default:
				# code...
				break;
		}

		return $translated;
	}


	/**
	 * metaboxes()
	 *
	 * Adds all the metaboxes needed in the edit notification
	 *
	 * @static 
	 * @access public
	 * 
	 * @return void
	 */
	public static function metaboxes()
	{
		add_meta_box( 'ic-sms-notification-send-message-metabox', 'Send Message', array( __CLASS__, 'message_metabox_content' ), IC_SMS_Notification::POST_TYPE, 'normal' );
	}	


	/**
	 * message_metabox_content()
	 *
	 * The content of the send message metabox
	 *
	 * @static
	 * @access public
	 * 
	 * @param  WP_Post $post 
	 * @return void
	 */
	public static function message_metabox_content( $post )
	{
		global $pagenow;

		$is_edit 				= "post.php" === $pagenow;
		$notification_id 		= $post->ID;
		$notification_manager 	= ic_sms()->notification_manager();
		$recipients 			= $notification_manager->get_recipients();
		$selected 				= ic_sms_get_notification_recipients_id( $notification_id );
		$message 				= ic_sms_get_notification_message( $notification_id );
		$shortcodes 			= ic_sms_get_shortcodes_description();
		?>
		<table class="form-table">
			
			<tbody>
				
				<tr>
					<th><label>Recipients<span class="ic-sms-required">*</span></label></th>
					<td>
						<fieldset>
							<select name="ic_sms_notification_recipients[]" required multiple class="widefat" id="ic-sms-send-message-to" data-placeholder="Type a group or contact name here...">
								
								<?php foreach( $recipients as $k => $v ): ?>

								<option <?php echo in_array( $k, $selected ) ? 'selected="selected"' : ''; ?> value="<?php echo esc_attr( $k ) ?>"><?php echo $v; ?></option>

								<?php endforeach; ?>

							</select>
						</fieldset>
					</td>
				</tr>

				<tr>	
					<th><label>Message<span class="ic-sms-required">*</span></label></th>
					<td>
						<fieldset>
							<table class="ic-sms-table ic-sms-2-col">
								<tbody>
									<tr>
										<td>
											<textarea name="ic_sms_notification_message" id="ic-sms-send-message-body" rows="8" required class="widefat" <?php echo $is_edit ? "readonly" : ""; ?>><?php echo esc_textarea( $message ); ?></textarea>
											<p>
												<em>Total Characters: </em><strong id="ic-sms-message-character-count"><?php echo strlen($message); ?></strong> |
												<em>Total Parts: </em><strong id="ic-sms-messages-count">1</strong>
											</p>
										</td>
										<td>
											<div class="ic-sms-preview-sms" id="ic-sms-preview-sms"><?php echo esc_textarea( Patchwork\Utf8::toAscii($message) ); ?></div>
											<p>
												<em>SMS Preview</em>
											</p>
										</td>
									</tr>
								</tbody>
							</table>
							<br/>
							<strong>Please read before sending:</strong>
							<ul class="ic-sms-notes">
								<li>Sending message with more than 160 characters sometimes leads to message sent in multiple parts depending on the mobile phone handset or telecom.</li>
								<li>The character limit is 160 for the first part of a message and 154 for the remaining parts.</li>
								<li>It's recommended to keep your messages as short as possible.</li>
							</ul>
						</fieldset>
					</td>
				</tr>
			</tbody>

		</table>
		<?php
	}


	/**
	 * save()
	 *
	 * Customise the saving of the notitication
	 *
	 * @static
	 * @access public
	 * 
	 * @param  int     $notification_id The id of the notification
	 * @param  WP_Post $post_object     The WordPress object of the notification
	 * @param  boolean $update          Update or new
	 * @return void
	 */
	public static function save( $notification_id, $post_object, $update )
	{
		// If no form is submitted don't do anything
		if ( empty( $_POST ) ) {
			
			return;
		}

		// Get recipients
		$recipients = filter_input( INPUT_POST, 'ic_sms_notification_recipients', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );

		// Get message
		$message = filter_input( INPUT_POST, 'ic_sms_notification_message' );


		if ( $message && $recipients ) {
			
			// Update message of the notification
			ic_sms_update_notification_message( $notification_id, $message );
			ic_sms_update_notification_recipients_id( $notification_id, $recipients );
		}
	}
}


