<?php


function ic_sms_hook_birthday_greet()
{
	// Month and day
	$month = current_time('m');
	$day   = current_time('d');

	// Make sure we only execute this once per day regardless of the cron interval
	$key         = 'ic_sms_birthday_greeting_' . current_time('Ymd');
	$has_greeted = get_transient( $key );

	if ($has_greeted) {

		return;
	}

	// Set cache that we already greeted
	set_transient( $key, 1, DAY_IN_SECONDS );

	// Month today
	$query = IC_SMS_Subscriber::query(array(
		'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key'      => 'ic_sms_subscriber_birthday_month',
				'value'    => $month,
				'compare' => '=='
			),
			array(
				'key'      => 'ic_sms_subscriber_birthday_day',
				'value'    => $day,
				'compare' => '=='
			)
		),
		'posts_per_page' => -1,
		'fields' => 'ids'
	));

	// dont doe anything if there are no celebrants
	if (!$query->have_posts()) {

		return;
	}

	$ids = $query->posts;
	
	$notification = new IC_SMS_Notification();
	$notification->set_message( get_option( 'ic_sms_birthday_message' ) );
	$notification->set_title( current_time('F d') . ' Birthday Greetings' );

	foreach( $ids as $id ) {

		$notification->add_recipient_id( 'subscriber_' . $id );
	}

	$notification->save();
	$notification->send();
}

add_action( 'ic_sms_cron_birthday_greeting', 'ic_sms_hook_birthday_greet' );


function ic_sms_cron_register_events()
{
	// Set default greeting to 8 in the morning
	ic_sms_cron_set_greeting_time( get_option( 'ic_sms_birthday_greet_time', '08:00' ) );
}

function ic_sms_cron_set_greeting_time( $time )
{
	$schedule       = 'ic_sms_cron_birthday_greeting';

	// Clear schedule first
	ic_sms_cron_remove_scheduled_greeting();

	// Always execute greetings everyday
	$recurrance = 'daily';

	// Convert local time to GMT time cause cron works on GMT and not local time
	wp_schedule_event( strtotime(get_gmt_from_date( date('Y-m-d ') . $time .':00', 'Y-m-d h:i:s' )), $recurrance, $schedule );
}

// add_filter( 'cron_schedules', function($schedules) {
// 	$schedules['minute'] = array(
// 		'interval' => 60
// 	);

// 	return $schedules;
// } );

function ic_sms_cron_remove_scheduled_greeting()
{
	wp_clear_scheduled_hook( 'ic_sms_cron_birthday_greeting' );
}

function ic_sms_cron_unregister_events()
{
	// Dont greet when plugin is deactivated
	ic_sms_cron_remove_scheduled_greeting();
}
