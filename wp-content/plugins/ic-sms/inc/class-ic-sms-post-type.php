<?php

class IC_SMS_Post_Type
{
	/**
	 * Id of the object
	 *
	 * @access private
	 * 
	 * @var integer
	 */
	private $_id = 0;

	/**
	 * Default properties
	 *
	 * @access private
	 * 
	 * @var mixed
	 */
	private $title, $_created_at, $_updated_at;

	/**
	 * Holds the WP_Post
	 * 
	 * @access private
	 * 
	 * @var WP_Post
	 */
	private $_post;

	/**
	 * __construct()
	 *
	 * This instantiate the object
	 *
	 * @access public
	 * 
	 * @param integer $id  - The id of the post
	 * @return void
	 */
	public function __construct( $id = 0 )
	{
		$this->_id = $id;

		if ( $this->is_new() === false ) {

			$this->_post 		= get_post( $this->get_id() );
			$this->_created_at 	= $this->get_post()->post_date;
			$this->_updated_at 	= $this->get_post()->post_modified;

			$this->set_title( get_the_title( $this->get_id() ) );
		}
	}


	/**
	 * __call()
	 *
	 * This creates automatic setters and getters
	 *
	 * @access public
	 * 
	 * @param  string $method  The method called
	 * @param  array  $args    The parameters
	 * @return mixed
	 */
	public function __call( $method, $args )
	{
		// Check if the method called is a getter
		if ( ( $pos = strpos( $method, 'get_' ) ) != -1 ) {
			
			// Get the property
			$property = str_replace( 'get_', '', $method );

			// Check if propery exists
			if ( property_exists( $this, $property ) ) {
				
				// then return the value of that propery
				return $this->$property;
			}
		}

		// Check if the method called is a setter
		if ( ( $pos = strpos( $method, 'set_' ) ) != -1 ) {
			
			// Get the property
			$property 	= str_replace( 'set_', '', $method );
			$value 		= isset( $args[0] ) ? $args[0] : null;

			// Check if propery exists
			if ( property_exists( $this, $property ) && !is_null( $value ) ) {
				
				// set the value of that propery
				$this->$property = $value;
			}
		}
	}


	/**
	 * get_id()
	 *
	 * This gets the id of the object
	 *
	 * @access public
	 * 
	 * @return int
	 */
	public function get_id()
	{
		return $this->_id;	
	}


	/**
	 * get_post()
	 *
	 * Just gets the post WP_Post object
	 *
	 * @access public
	 * 
	 * @return WP_Post
	 */
	public function get_post()
	{
		return $this->_post;
	}


	/**
	 * get_created_at()
	 *
	 * Just gets the post date 
	 *
	 * @access public
	 * 
	 * @return date
	 */
	public function get_created_at()
	{
		return $this->_created_at;
	}


	/**
	 * get_updated_at()
	 *
	 * Just gets the post modified date 
	 *
	 * @access public
	 * 
	 * @return date
	 */
	public function get_updated_at()
	{
		return $this->_updated_at;
	}


	/**
	 * is_new()
	 *
	 * Checks whethere the object is new or not
	 *
	 * @access protected
	 * 
	 * @return boolean 
	 */
	protected function is_new()
	{
		return !($this->_id > 0);
	}

	/**
	 * get_post_type()
	 *
	 * This just returns the slug or name of the post type
	 * MUST BE OVERIDEN WHEN SUBCLASS
	 *
	 * @static 
	 * @access protected
	 * 
	 * @return string   The slug or name of the post type
	 */
	protected static function get_post_type()
	{
		return 'post';
	}


	/**
	 * save()
	 *
	 * This saves or updates a post
	 *
	 * @access public
	 * 
	 * @return int The id of the object
	 */
	public function save()
	{		
		// If the post is new inset new
		// post
		if ( $this->is_new() ) {
			
			$this->_id = wp_insert_post( array(
				'post_status' => 'publish',
				'post_type'	  => static::get_post_type(),
				'post_title'  => $this->get_title()
			) );
		}

		// Update the title of the post
		wp_update_post( array(
			'ID' => $this->get_id(),
			'post_title' => $this->get_title(),
			'post_type' => static::get_post_type()
		));

		// Update the _post property
		$this->_post = get_post( $this->get_id() );

		return $this->get_id();
	}

	/**
	 * query()
	 *
	 * This creates a WP_Query object with the post_type automatically
	 *
	 * @static
	 * @access public
	 * 
	 * @param  array  $custom_query_args For customising the query results
	 * @return WP_Query
	 */
	public static function query( $custom_query_args = array() )
	{
		$default_query_args = array(
			'post_type' => static::get_post_type()
		);

		return new WP_Query( array_merge( $default_query_args, $custom_query_args ) );
	}
}