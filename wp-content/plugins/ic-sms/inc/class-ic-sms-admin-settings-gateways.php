<?php

class IC_SMS_Admin_Settings_Gateways extends IC_SMS_Admin_Settings_Page
{
	public function __construct()
	{
		$this->id = 'gateways';
		$this->label = 'SMS Gateways';

		parent::__construct();
	}

	public function get_sections() {

		$sections = array();

		$gateway_manager	= ic_sms()->gateway_manager();
		$gateways 			= $gateway_manager->get_gateway_selection();

		return $gateways;
	}

	public function save()
	{
		global $current_section;

		if ( empty( $current_section ) ) {
			
			$current_section = $this->get_first_section_key();
		}

		$gateways  = ic_sms()->gateway_manager()->gateways();

		foreach ( $gateways as $gateway_class_name => $gateway ) {

			if ( strtolower( $gateway_class_name ) == $current_section ) {
				
				$gateway->process_admin_options();
			}
		}
	}

	public function output()
	{
		global $current_section;

		$gateways  = ic_sms()->gateway_manager()->gateways();

		foreach ( $gateways as $gateway_class_name => $gateway ) {

			if ( strtolower( $gateway_class_name ) == $current_section ) {
				
				$gateway->html();
			}
		}
	}
}

return new IC_SMS_Admin_Settings_Gateways();