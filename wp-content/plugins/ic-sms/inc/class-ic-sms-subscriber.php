<?php

if ( !defined( 'WPINC' ) ) {
	
	die();
}

if ( !class_exists( 'IC_SMS_Subscriber' ) ) :

class IC_SMS_Subscriber extends IC_SMS_Post_Type
{
	const POST_TYPE = 'ic-sms-subscriber';

	/**
	 * String properties
	 *
	 * @access protected
	 */
	protected $first_name, $last_name, $middle_name, $email;


	/**
	 * Array properties
	 * @access protected
	 */
	protected $numbers = array();


	/**
	 * __construct()
	 *
	 * The constructor
	 * 
	 * @static
	 * @access public
	 *
	 * @return IC_SMS_Subscriber
	 */
	public function __construct( $id = 0 )
	{
		parent::__construct( $id );
		
		// Load detail if there is an id pass
		if ( $this->is_new() === false ) {
			
			// Prepopulate properties the WordPress metadata
			$this->set_first_name( ic_sms_get_subscriber_first_name( $this->get_id() ) );
			$this->set_last_name( ic_sms_get_subscriber_last_name( $this->get_id() ) );
			$this->set_middle_name( ic_sms_get_subscriber_middle_name( $this->get_id() ) );
			$this->set_email( ic_sms_get_subscriber_email( $this->get_id() ) );
			$this->set_numbers( ic_sms_get_subscriber_numbers( $this->get_id() ) );
		}
	}


	/**
	 * save()
	 *
	 * OVERIDE PARENT
	 * 
	 * @return int   The id of the subscriber
	 */
	public function save()
	{
		$id = parent::save();

		ic_sms_update_subscriber_first_name( 	$this->get_id(), $this->get_first_name() 	);
		ic_sms_update_subscriber_last_name( 	$this->get_id(), $this->get_last_name() 		);
		ic_sms_update_subscriber_middle_name( 	$this->get_id(), $this->get_middle_name() 	);
		ic_sms_update_subscriber_email( 		$this->get_id(), $this->get_email() 			);
		ic_sms_update_subscriber_numbers( 		$this->get_id(), $this->get_numbers() 		);

		return $id;
	}

	// OVERIDE
	protected static function get_post_type()
	{
		return self::POST_TYPE;
	}
}

endif;