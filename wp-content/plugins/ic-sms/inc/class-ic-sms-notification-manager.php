<?php

// Check if the class doesn't exists
if( !class_exists( 'IC_SMS_Notification_Manager' ) ):

/**
 * This class will manage all the 
 * notifications for sending, scheduling notifications
 */
class IC_SMS_Notification_Manager 
{
	/**
	 * Handles the single instance of this class
	 * 
	 * @static
	 * @access private
	 * 
	 * @var IC_SMS_Notification_Manager
	 */
	private static $_instance = null;

	/**
	 * __construct()
	 *
	 * Constructor
	 * just to prevent having a multiple instance of the object
	 * make the constructor private
	 *
	 * @access private
	 * 
	 * @return void
	 */
	private function __construct() {}

	/**
	 * initialize()
	 *
	 * This will register all the hooks and setup
	 * needed to run the manager
	 *
	 * @access public
	 * 
	 * @return void
	 */
	public function initialize()
	{
		// Hook whe 
		add_action( 'save_post_' . IC_SMS_Notification::POST_TYPE, array( $this, 'send_notification' ), 99, 2 );
	}


	/**
	 * send_notification()
	 * 
	 * Sends a notification
	 *
	 * This does the cool thing sending the message via SMS
	 * 
	 * @param  int $notification_id 
	 * @return void
	 */
	public function send_notification( $notification_id, $post )
	{
		// Only hook this action once
		if (did_action( 'save_post_' . IC_SMS_Notification::POST_TYPE ) > 2) {
			return;
		}

		$notification = new IC_SMS_Notification( $notification_id );
		$notification->send();
	}

	/**
	 * instance()
	 *
	 * @static
	 * @access public
	 * 
	 * @return IC_SMS_Notification_Manager
	 */
	public static function instance()
	{
		if ( is_null( self::$_instance ) ) {
			
			self::$_instance = new self();
		}

		return self::$_instance;
	}


	/**
	 * get_recipients()
	 *
	 * Gets all the recipients
	 *
	 * @access public
	 * 
	 * @param  string $s The search string
	 * @return array
	 */
	public function get_recipients( $s = '' )
	{
		static $cache = array();

		// Cache results
		if ( isset( $cache[ $s ] ) && !empty( $s ) ) {
			
			return $cache[ $s ];
		}

		$choices = array();

		$terms = get_terms( array( IC_SMS_Group::TAXONOMY ), array(
			'search' => $s
		));

		foreach( $terms as $term ) {

			$choices[ 'group_' . $term->term_id ] = $term->name . ' ('. $term->count .' Contacts)';
		}

		$subscribers = IC_SMS_Subscriber::query( array(
			'fields' 			=> 'ids',
			's' 				=> $s,
			'posts_per_page' 	=> -1,
			'post_status' 		=> 'publish'
		) );

		foreach( $subscribers->posts as $id ) {

			$choices[ 'subscriber_' . $id ] = ic_sms_get_subscriber_full_name( $id, '%f %m %l' ) . ' (Contact)';
		}

		$cache[ $s ] = $choices;

		return $choices;
	}
}

endif;// end class exists check
