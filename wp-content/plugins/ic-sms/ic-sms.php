<?php
/**
 * Plugin Name: IC SMS
 * Plugin URI: mailto:allanchristiancarlos@gmail.com
 * Author: Allan Christian Carlos
 * Author URI: mailto:allanchristiancarlos@gmail.com
 * Description: IC SMS sends bulk sms using the SMS Gateways.
 * Version: 1.9.2
 */

if( !defined( 'ABSPATH' ) )
{
	die();
}

if ( !defined( 'IC_SMS_DIR' ) ) 
{
	define( 'IC_SMS_DIR', dirname( __FILE__ ) );
}

if ( !defined( 'IC_SMS_INC' ) ) 
{
	define( 'IC_SMS_INC', IC_SMS_DIR . '/inc' );
}

if ( !defined( 'IC_SMS_URI' ) ) 
{
	define( 'IC_SMS_URI', str_replace( WP_CONTENT_DIR, WP_CONTENT_URL, IC_SMS_DIR ) );
}

class IC_SMS
{
	/**
	 * Version of the plugin
	 * 
	 * @var string
	 */
	public $version = '1.9.2';

	/**
	 * Holds the single instance of this class
	 * 
	 * @static
	 * @access private
	 * 
	 * @var IC_SMS
	 */
	private static $_instance = null;


	/**
	 * __construct()
	 *
	 * Constructor
	 * just to prevent having a multiple instance of the object
	 * make the constructor private
	 *
	 * @access private
	 * 
	 * @return void
	 */
	private function __construct() {}


	/**
	 * initialize()
	 *
	 * This will register all the hooks and setup and classes
	 * needed to run the manager
	 *
	 * @access public
	 * 
	 * @return void
	 */
	public function initialize()
	{
		$this->_load_includes();
		$this->_load_modules();
		$this->_load_hooks();
	}


	/**
	 * _load_includes()
	 *
	 * This method will only include all the necesarry files
	 * to run the plugin
	 * 
	 * @access private
	 * 
	 * @return void
	 */
	private function _load_includes()
	{
		include_once IC_SMS_DIR . '/vendor/autoload.php';
		include_once IC_SMS_INC . '/functions-ic-sms-subscriber.php';
		include_once IC_SMS_INC . '/functions-ic-sms-notification.php';
		include_once IC_SMS_INC . '/cron-ic-sms.php';
		include_once IC_SMS_INC . '/class-ic-sms-post-type.php';
		include_once IC_SMS_INC . '/class-ic-sms-settings-api.php';
		include_once IC_SMS_INC . '/class-ic-sms-gateway-manager.php';
		include_once IC_SMS_INC . '/class-ic-sms-notification-manager.php';
		include_once IC_SMS_INC . '/class-ic-sms-data.php';
		include_once IC_SMS_INC . '/class-ic-sms-subscriber.php';
		include_once IC_SMS_INC . '/class-ic-sms-notification.php';
		include_once IC_SMS_INC . '/class-ic-sms-group.php';
		include_once IC_SMS_INC . '/class-ic-sms-ajax.php';
		
		if( is_admin() ) {

			include_once IC_SMS_INC . '/class-ic-sms-admin.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-notifications.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-edit-notification.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-edit-subscriber.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-subscribers.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-settings.php';
			include_once IC_SMS_INC . '/class-ic-sms-admin-import.php';
		}
	}

	/**
	 * _load_modules()
	 *
	 * This will initialize all the classes that needs 
	 * to be initialized
	 * 
	 * @access private
	 * 
	 * @return void
	 */
	private function _load_modules()
	{
		$this->notification_manager()->initialize();
		$this->ajax()                ->initialize();
	}


	/**
	 * _load_hooks()
	 *
	 * This will initialize all the hooks to customise WordPress 
	 * 
	 * @access private
	 * 
	 * @return void
	 */
	private function _load_hooks()
	{
		// Register hooks
		register_activation_hook( __FILE__, 'ic_sms_cron_register_events' );
		register_deactivation_hook( __FILE__, 'ic_sms_cron_unregister_events' );

		// Global
		add_action( 'init', array( 'IC_SMS_Data', 'register' ) );

		if( is_admin() ) {

			// Admin
			add_action( 'admin_enqueue_scripts', 		array( 'IC_SMS_Admin', 'assets' 								) );
			add_action( 'admin_menu',					array( 'IC_SMS_Admin', 'menus' 									) );
			add_action( 'admin_enqueue_scripts',		array( 'IC_SMS_Admin', 'disable_autosave' 						) );
			add_action( 'admin_menu', 					array( 'IC_SMS_Admin', 'remove_groups_metabox_in_notification' 	) );
			add_filter( 'the_title', 					array( 'IC_SMS_Admin', 'subscriber_title_to_full_name' 			) );


			// Notifications
			add_filter( 'manage_edit-' . IC_SMS_Notification::POST_TYPE . '_columns', 			array( 'IC_SMS_Admin_Notifications', 'columns' ) );
			add_filter( 'post_row_actions',			 											array( 'IC_SMS_Admin_Notifications', 'row_actions' ) );
			add_action( 'manage_' . IC_SMS_Notification::POST_TYPE . '_posts_custom_column', 	array( 'IC_SMS_Admin_Notifications', 'custom_column' ), 10, 2 );
			add_filter( 'bulk_actions-edit-' . IC_SMS_Notification::POST_TYPE, 						array( 'IC_SMS_Admin_Notifications', 'bulk_actions' ) );

			// Edit Notification
			add_action( 'add_meta_boxes', 								array( 'IC_SMS_Admin_Edit_Notification', 'metaboxes' ) );
			add_action( 'save_post_' . IC_SMS_Notification::POST_TYPE,	array( 'IC_SMS_Admin_Edit_Notification', 'save' ), 10, 3 );
			add_filter( 'default_title', 								array( 'IC_SMS_Admin_Edit_Notification', 'new_title' ) );
			add_filter( 'gettext', 										array( 'IC_SMS_Admin_Edit_Notification', 'change_submitbox_texts' ), 20, 3 );
			add_filter( 'admin_head', 									array( 'IC_SMS_Admin_Edit_Notification', 'remove_visibility_option' ) );
			add_action( 'admin_notices', 								array( 'IC_SMS_Admin_Edit_Notification', 'remove_submitbox_on_publish' ) );

			// Subscribers
			add_filter( 'manage_edit-' . IC_SMS_Subscriber::POST_TYPE . '_columns', 		array( 'IC_SMS_Admin_Subscribers', 'columns' ) );
			add_filter( 'post_row_actions', 												array( 'IC_SMS_Admin_Subscribers', 'actions' ) );
			add_action( 'manage_' . IC_SMS_Subscriber::POST_TYPE . '_posts_custom_column', 	array( 'IC_SMS_Admin_Subscribers', 'custom_column' ), 10, 2 );
			add_action( 'pre_get_posts', 													array( 'IC_SMS_Admin_Subscribers', 'query' ) );

			// Edit Subscriber
			add_action( 'add_meta_boxes', 								array( 'IC_SMS_Admin_Edit_Subscriber', 'metaboxes' 						) );
			add_action( 'save_post_' . IC_SMS_Subscriber::POST_TYPE,	array( 'IC_SMS_Admin_Edit_Subscriber', 'save' 							) );
			add_filter( 'post_updated_messages', 						array( 'IC_SMS_Admin_Edit_Subscriber', 'updated_message' 				) );
			add_filter( 'enter_title_here', 							array( 'IC_SMS_Admin_Edit_Subscriber', 'change_textbox_placeholder' 	) );
		}
	}

	/**
	 * instance()
	 *
	 * @static
	 * @access public
	 * 
	 * @return IC_SMS
	 */
	public static function instance()
	{
		if ( is_null( self::$_instance ) ) {
			
			self::$_instance = new self();
		}

		return self::$_instance;
	}


	/**
	 * gateway_manager()
	 *
	 * This gets gateway manager shared instance
	 *
	 * @access public
	 * 
	 * @return IC_SMS_Gateway_Manager object
	 */
	public function gateway_manager()
	{
		return IC_SMS_Gateway_Manager::instance();
	}


	/**
	 * ajax()
	 *
	 *
	 * @access public
	 * 
	 * @return IC_SMS_Ajax object
	 */
	public function ajax()
	{
		return IC_SMS_Ajax::instance();
	}


	/**
	 * notification_manager()
	 *
	 * This gets notification manager shared instance
	 *
	 * @access public
	 * 
	 * @return IC_SMS_Notification_Manager object
	 */
	public function notification_manager()
	{
		return IC_SMS_Notification_Manager::instance();
	}
}


/**
 * ic_sms()
 *
 * The shared instance of the class
 *
 * @return IC_SMS
 */
function ic_sms()
{
	$ic_sms = IC_SMS::instance();

	$ic_sms->initialize();

	return $ic_sms;
}

$GLOBALS['ic_sms'] = ic_sms();