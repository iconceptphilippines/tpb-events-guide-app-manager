<?php
/**
 * Plugin Name: TPB App Manager
 * Description: Manages the content of the TPB Events Guide App
 * Author: iConcept Philippines
 * Author URI: http://iconcept.com.ph
 * Version: 0.0.1
 */


if ( !defined( 'WPINC' ) ) {

	die();
}

if ( !defined( 'TAM_VERSION' ) ) {

	define( 'TAM_VERSION', '0.0.1' );
}

if ( !defined( 'TAM_DIR' ) ) {

	define( 'TAM_DIR', dirname( __FILE__ ) );
}

if ( !defined( 'TAM_URL' ) ) {

	$_url = plugin_dir_url( __FILE__ );

	define( 'TAM_URL', substr($_url, 0, strlen( $_url ) - 1) );
}

if ( !defined( 'TAM_PARSE_APP_ID' ) ) {

	define( 'TAM_PARSE_APP_ID', 'GHfyuasdkdmaHJAasdlasdkasdasdAasd' ); // Production
	// define( 'TAM_PARSE_APP_ID', 'ObjMW6OwKX3VJ6B6cBRSv4ii7RXVGTcxGvpOywoY' ); // Dev
}

if ( !defined( 'TAM_PARSE_REST_KEY' ) ) {

	define( 'TAM_PARSE_REST_KEY', '' ); // Production
	// define( 'TAM_PARSE_REST_KEY', 'i2Z7ZtQ97Pb02WFp6TR00bSgYMqnwtePkSkxW9nn' ); // Dev
}

if ( !defined( 'TAM_PARSE_MASTER_KEY' ) ) {

	define( 'TAM_PARSE_MASTER_KEY', 'sRrdigZMvXg211MVuLQO76Es5H6PJ32a' ); // Production
	// define( 'TAM_PARSE_MASTER_KEY', 'u71mhf0voDcp8iRTSM1CKO3j35uUh0SmXB4gIG0X' ); // Dev
}

require_once TAM_DIR . '/vendor/autoload.php';
require_once TAM_DIR . '/inc/classes/parse-list-table.php';
require_once TAM_DIR . '/inc/classes/tam-parse-object.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-general.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-menus.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-gallery.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-questions.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-pages.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-announcements.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-schedules.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-attendees.php';
require_once TAM_DIR . '/inc/classes/tam-event-tab-file.php';
require_once TAM_DIR . '/inc/api.php';
require_once TAM_DIR . '/inc/admin.php';
require_once TAM_DIR . '/inc/ajax.php';
require_once TAM_DIR . '/inc/assets.php';
require_once TAM_DIR . '/inc/actions.php';
require_once TAM_DIR . '/inc/events.php';
require_once TAM_DIR . '/inc/users.php';
require_once TAM_DIR . '/inc/questions.php';

// See inc/functions.php
tam_parse_connect();
