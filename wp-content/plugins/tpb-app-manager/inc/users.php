<?php

/**
 * Registers the events table
 */
function tam_users_register_table()
{
	// attendees table
	tam_register_parse_table( 'users', '_User', array(
		'columns'				=> array(
			'cb' 			=> ' ',
			'firstName'		=> 'Name',
			'username'		=> 'Username',
			'editor'		=> 'Editor',
			'createdAt'		=> 'Date Registered',
		),
		'autofill'				=> array(
			'username'
		),
		'default_column' 		=> 'firstName',
		'sortable_columns' 		=> array(
			'firstName' 		=> array( 'firstName', false ),
			'username' 			=> array( 'username', false ),
		),
		'enable_search'			=> true,
		'searcheable_columns'	=> array( 'keywords', 'firstName' ),
		'per_page'				=> 10
	));
}

add_action( 'init', 'tam_users_register_table' );

/**
 * Fills the contents users table columns
 * @param  string 		$column_name The name of the column
 * @param  ParseObject 	$object      The object column
 * @return void
 */
function tam_users_columns_content( $column_name, $object )
{

	if ( 'editor' == $column_name ) {

		$checked = tam_user_is_editor( $object->getObjectId() );

		echo '<input data-userid="'. $object->getObjectId() .'" data-action="user-as-editor" type="checkbox" value="1" '. checked( $checked, true, false ) .'>';

	} elseif( 'position' == $column_name ) {

		echo $object->get( 'position' );
	}
}

add_action( 'tam_parse_list_table/users/column', 'tam_users_columns_content', 10, 2 );

function users_primary_column_text( $text, $object )
{
	$first_name = $object->get( 'firstName' );
	$middle_name = $object->get( 'middleName' );
	$last_name = $object->get( 'lastName' );

	if( $middle_name != '' ) {

		$name = $first_name.' '.$middle_name.' '.$last_name;
	} else {

		$name = $first_name.' '.$last_name;
	}

	return $name;
}
add_filter( 'tam_parse_list_table/users/primary_column_text', 'users_primary_column_text', 10, 2 );

function tam_users_table_row_actions( $actions, $object, $table )
{
	$actions = array();
	$stripped_link = remove_query_arg( array('s', 'paged') );

	// Edit link
	$actions['edit'] = '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'">View Profile</a>';
	$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete</a>';

	return $actions;
}

add_filter( 'tam_parse_list_table/users/row_actions', 'tam_users_table_row_actions', 10, 3 );

function users_edit_link( $link, $object_id, $class )
{
	if ( $class != '_User' ) {
		return $link;
	}

	$params = array(
		'page'			=> 'tam-users',
		'object_class'	=> $class,
		'object_id' 	=> $object_id,
	);

	return esc_url( add_query_arg( $params, tam_admin_page_url() ) );
}
add_filter( 'tam/get_object_edit_link',	'users_edit_link', 10, 3 );

function users_before_table( $table )
{
	echo '<input type="hidden" value="1" name="force_delete">';
}
add_action( 'tam_parse_list_table/users/before_table', 'users_before_table', 10, 1 );

function users_bulk_actions( $actions, $table )
{
	$actions  = array();

	$actions[ 'delete' ] = 'Delete';

	return $actions;
}
add_filter( 'tam_parse_list_table/users/bulk_actions', 'users_bulk_actions', 10, 2 );

function avatar_uploaded( $file, $data )
{
	$user = Parse\ParseObject::create( '_User', $data['object_id'] );
	$path = $file['file'];
	$file = Parse\ParseFile::createFromFile( $path, basename( $path ), $file['type'] );
	$user->set( 'avatar', $file );

	try {

		$user->save( TAM_PARSE_MASTER_KEY );

		$user->fetch();

		$new_image = $user->get( 'avatar' );
		$image_url = '';

		if ( $new_image ) {

			$image_url = $new_image->getURL();
		}

		wp_send_json_success( array(
			'name'	=> $new_image->getName(),
			'url' 	=> $image_url
		));
	} catch (Exception $e) {

		wp_send_json_error( array(
			'message' => $e->getMessage()
		) );
	}
}
add_action( 'tam/ajax_upload/name=avatar_thumbnail', 'avatar_uploaded', 10, 2 );




function tam_listen_action_new_event( $data )
{
	if( is_null( filter_input( INPUT_POST , 'tam_title') ) ) {
		return;
	}

	try {

		$id = tam_save_object( 'Event', array(
			'title' 	=> filter_input( INPUT_POST, 'tam_title' ),
			'content'	=> filter_input( INPUT_POST, 'tam_content' ),
			'address'	=> filter_input( INPUT_POST, 'tam_address' ),
			'venueName'	=> filter_input( INPUT_POST, 'tam_venue_name' ),
			'startDate'	=> array(
				'type' => 'date',
				'date'	=> filter_input( INPUT_POST , 'tam_start_date')
			),
			'endDate'	=> array(
				'type' => 'date',
				'date'	=> filter_input( INPUT_POST , 'tam_end_date')
			),
			'coordinates' => array(
				'type' 		=> 'geopoint',
				'latitude' 	=> filter_input( INPUT_POST, 'tam_latitude', FILTER_VALIDATE_FLOAT ),
				'longitude' => filter_input( INPUT_POST, 'tam_longitude', FILTER_VALIDATE_FLOAT ),
			),
			'image'		=> array(
				'type' => 'file',
			),
			'city'		=> filter_input( INPUT_POST, 'tam_city' ),
			'country'	=> filter_input( INPUT_POST, 'tam_country' ),
			'zipCode'	=> filter_input( INPUT_POST, 'tam_zipcode' ),
			'isPrivate' => array(
				'type'	=> 'boolean',
				'value'	=> isset( $_POST['tam_public'] ) ? false : true,
			),
			'status'	=> filter_input( INPUT_POST , 'status'),
		));

		wp_redirect( tam_get_object_edit_link( $id, 'Event' ) );
		exit;

	} catch (Exception $e) {

		tam_view( 'alert', array(
			'type' 	=> 'warning',
			'label' => $e->getMessage()
		));
	}
}

add_action( 'tam/admin_page/action/new_event', 'tam_listen_action_new_event' );