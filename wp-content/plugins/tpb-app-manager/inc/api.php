<?php
use Parse\ParseObject;
use Parse\ParseQuery;


/**
 * Used to display a view
 * 	
 * @param  string $view The name of the view
 * @param  array  $vars The array of variables this will be extract the key as the variable name in the view file
 */
function tam_view( $view, $vars = array() )
{
	$path = TAM_DIR . '/views/' . $view . '.php';

	if ( file_exists( $path ) ) {
		
		extract( $vars );

		include $path;
	}
}


/**
 * Initializes connection to parse.com api
 * 
 * @return void
 */
function tam_parse_connect()
{
	try {
		
		Parse\ParseClient::initialize( TAM_PARSE_APP_ID, TAM_PARSE_REST_KEY, TAM_PARSE_MASTER_KEY );
		Parse\ParseClient::setServerURL('http://samsa.zoom.ph:1338', 'parse');
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}



/**
 * Returns the url of the main admin page
 * 
 * @return string The url of the main admin page
 */
function tam_admin_page_url()
{
	return add_query_arg( array( 'page' => 'tam' ), admin_url( '/admin.php' ) );
}


/**
 * Returns the edit link of the object
 * 
 * @param  string $object_id The id of the object
 * @return string            The edit link of the object
 */
function tam_get_object_edit_link( $object_id, $class )
{
	$link = add_query_arg( array( 
		'object_class' 	=> $class,
		'object_id' 	=> $object_id 
	), tam_admin_page_url() );
	
	return apply_filters( 'tam/get_object_edit_link', $link, $object_id, $class );
}


function tam_get_user_edit_link( $user_id )
{
	return add_query_arg( array( 'object_id' => $user_id, 'object_class' => '_User', 'page' => 'tam-users' ), admin_url('admin.php') );
}

/**
 * Creates a delete link of the object
 * 
 * @param  strign  $object_id    The id of the object
 * @param  string  $class        The class of the object
 * @param  boolean $force_delete If true the object will be permanently deleted
 * @param  string  $redirect_url The url to be redirected to after the delete is executed
 * @return string                The delete url
 */
function tam_get_object_delete_link( $object_id, $class, $force_delete = false, $redirect_url = null )
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$delete_link = add_query_arg( array(
		'action'		=> 'delete',
		'object_id' 	=> $object_id,
		'object_class' 	=> $class,
		'force_delete' 	=> $force_delete,
		'redirect_url'  => urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $delete_link, 'tam_delete_' . $object_id, 'tam_nonce' ));
}




/**
 * Restores the trashed object
 * 
 * @param  strign  $object_id    The id of the object
 * @param  string  $class        The class of the object
 * @param  string  $redirect_url The url to be redirected to after the delete is executed
 * @return string                The restore url
 */
function tam_get_object_restore_link( $object_id, $class, $redirect_url = null )
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$restore_link = add_query_arg( array(
		'action'		=> 'restore',
		'object_id' 	=> $object_id,
		'object_class' 	=> $class,
		'redirect_url'  => urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $restore_link, 'tam_restore_' . $object_id, 'tam_nonce' ));
}




/**
 * Creates a change status link of the object
 * 
 * @param  string  $object_id    The id of the object
 * @param  string  $class        The class of the object
 * @param  string  $status 		 The status to be change with 
 * @param  string  $redirect_url The url to be redirected to after the delete is executed
 * @return string                The delete url
 */
function tam_get_change_status_link( $object_id, $class, $status, $redirect_url = null )
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$link = add_query_arg( array(
		'action'		=> 'change_status',
		'object_id' 	=> $object_id,
		'object_class' 	=> $class,
		'status'		=> $status,
		'redirect_url'  => urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $link, 'tam_status_' . $object_id . $status, 'tam_nonce' ));
}


/**
 * Returns the duplicate link of the object
 * @param  string $object_id    The id of the object
 * @param  string $class        The class name of the object
 * @param  string $redirect_url The url to redirect to when duplication is finished
 * @return string               The duplicate url
 */
function tam_get_duplicate_link( $object_id, $class, $redirect_url = null )
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$link = add_query_arg( array(
		'action'		=> 'duplicate',
		'object_id' 	=> $object_id,
		'object_class' 	=> $class,
		'redirect_url'  => urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $link, 'tam_duplicate_' . $object_id, 'tam_nonce' ));
}


function tam_get_approve_attendee_link( $object_id, $redirect_url = null ) 
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$link = add_query_arg( array(
		'action'		=> 'approve_attendee',
		'object_id' 	=> $object_id,
		'redirect_url'	=> urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $link, 'tam_nonce' , 'tam_nonce' ));
}


function tam_get_unapprove_attendee_link( $object_id, $redirect_url = null ) 
{
	// Default redirect url
	if ( is_null( $redirect_url ) ) {
		
		$redirect_url = esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	}

	$link = add_query_arg( array(
		'action'		=> 'unapprove_attendee',
		'object_id' 	=> $object_id,
		'redirect_url'	=> urlencode($redirect_url)
	), tam_admin_page_url() );

	return esc_url_raw(wp_nonce_url( $link, 'tam_nonce' , 'tam_nonce' ));
}



/**
 * Gets the parse table
 * @param  string $table_id The table id of the parse table
 * @return Parse_List_Table The table object
 */
function tam_get_parse_table( $table_id )
{
	global $tam_parse_tables;

	// Check if parse table is registered
	if ( !isset( $tam_parse_tables[ $table_id ] ) ) {
		return new WP_Error( 'not_exists', 'Parse table don\'t exists.' );
	}

	// Get the table
	$args = $tam_parse_tables[ $table_id ];

	// Create new parse table
	$table = new Parse_List_Table( $args );

	// Prepare table
	$table->prepare_items();

	return $table;
}


function tam_register_parse_table( $table_id, $parse_class, $args = array() )
{
	global $tam_parse_tables;

	if ( ! is_array( $tam_parse_tables ) ) {
		$tam_parse_tables = array();
	}

	$args = wp_parse_args( array(
		'screen'		=> $table_id,
		'parse_class' 	=> $parse_class
	), $args );

	$tam_parse_tables[ $table_id ] = $args;

	return $args;
}


function tam_get_event_tabs()
{
	return apply_filters( 'tam/event_tabs', array() );
}


function tam_get_event_active_tab()
{
	$tab 	= filter_input( INPUT_GET, 'event_tab' );
	$tabs 	= tam_get_event_tabs();

	// Get first tab if tab is not defined
	if ( is_null( $tab ) || !in_array( $tab, array_keys( $tabs ) ) ) {
		
		foreach( $tabs as $k => $v ) 
			return $k;
	}

	return $tab;
}


function tam_get_event_tab_link( $tab, $event_id = null )
{
	if ( is_null( $event_id ) ) {
		
		$event_id = filter_input( INPUT_GET , 'object_id');
	}

	$params = array(
		'object_id' 	=> $event_id,
		'object_class' 	=> 'Event',
		'event_tab'		=> $tab
	);

	return esc_url( add_query_arg( $params, tam_admin_page_url() ) );
}


function tam_get_event_object_link( $class, $id, $link )
{
	$query_arg 	= '';

	if ( 'Gallery' == $class ) {
		
		$query_arg 	= 'gallery_id';
	} else if( 'Page' == $class ) {

		$query_arg = 'page_id';
	} else if( 'Topic' == $class ) {

		$query_arg = 'topic_id';
	}

	return add_query_arg( array( $query_arg => $id ), $link );
}


function tam_get_object_pointer_count( $object, $class, $key )
{
	$query = new Parse\ParseQuery( $class );

	$query->equalTo( $key, $object );

	return $query->count();
}



function tam_get_gallery_thumbnail_urls( $id, $count = 1 )
{
	$query 	= new Parse\ParseQuery( 'Photo' );
	$object = Parse\ParseObject::create( 'Gallery', $id );

	$query->equalTo( 'gallery', $object );
	$query->limit( $count );

	$urls = array();

	try {
		
		$items = $query->find();

		foreach( $items as $item ) {

			if ( $file = $item->get('image') ) {
				
				$urls[] = $file->getURL();
			}
		}

	} catch (Exception $e) {
		
		$urls = array();
	}

	return $urls;
}


function tam_update_parse_object_string_columns( $object_class, $object_id, $data )
{
	$object = Parse\ParseObject::create( $object_class, $object_id );
}

function tam_save_object( $object_class, $data, $object_id = null )
{
	if ( empty( $object_id ) ) {
		
		$object_id = null;
	}

	$unlinks 	= array();
	$relations 	= array();

	// Create new parse object
	// if object_id is null then it will create new object
	// if defined it will just update the object
	$object = Parse\ParseObject::create( $object_class, $object_id );

	foreach( $data as $k => $v ) {

		$final_value 	= null;
		$type 			= '';
		$delete 		= false;

		if ( is_array( $v ) ) {
			
			$type = !empty( $v['type'] ) ? $v['type'] : '';
		}


		// Pointer column
		if ( $type == 'pointer' ) {
			
			if ( empty( $v['object_id'] ) || empty( $v['class'] ) ) {
				
				continue;
			}

			$final_value = Parse\ParseObject::create( $v['class'], $v['object_id'] );
		} 

		// Relation
		else if( $type == 'relation' ) {

			if ( !isset( $v['merge'] ) ) {
				$v['merge'] = true;
			}

			if ( empty( $v['objects'] ) ) {
				
				continue;
			}

			$converted_relation_objects  = array();
			$relation_objects            = (array) $v['objects'];

			foreach( $relation_objects as $order => $relation_object_id ) {
				if ($relation_object_id != "") {
					$relational_object = Parse\ParseObject::create( $v['class'], $relation_object_id );
					$relational_object->set('order', $order);
					$relational_object->save();
					$converted_relation_objects[] = $relational_object;
				}
			}

			$relations[ $k ] = $converted_relation_objects;
		}

		// Date column
		else if( $type == 'date' ) {

			if ( empty( $v['date'] ) ) {
				
				continue;
			}

			$final_value = new DateTime( $v['date'] );
		}

		// File 
		else if( $type == 'file' ) {

			if ( empty( $_FILES[ $k ] ) ) {

				continue;
			}


			// Upload file first
			$file 		= $_FILES[ $k ];
			$overrides 	= array( 
				'test_form' => false 
			);
			$uploaded = wp_handle_upload( $file, $overrides );

			if( !isset($uploaded['error']) ){

				$unlinks[] 		= $uploaded['file'];
				$final_value 	= Parse\ParseFile::createFromFile($uploaded['file'], basename($uploaded['file']) );
			}

		}

		// Geopoint
		else if( $type == 'geopoint' ) {

			if ( empty( $v['latitude'] ) || empty( $v['longitude'] )  ) {
				
				continue;
			}

			$final_value = new Parse\ParseGeoPoint( floatval($v['latitude']), floatval($v['longitude']) );
		}

		else if( $type == 'boolean' ) {

			if ( !isset( $v['value'] ) ) {
				
				continue;
			}

			$value = $v['value'];

			if ( is_string( $value ) ) {
				
				if ( $value == 'true' ) {
					$final_value = true;
				} else {
					$final_value = false;
				}
			} else if( is_int( $value ) ) {
				if ( $value > 0 ) {
					$final_value = true;
				} else {
					$final_value = false;
				}
			} else {
				$final_value = $value;
			}
		}

		else if( $type == 'number' ) {

			if ( !isset( $v['value'] ) ) {
				continue;
			}

			$final_value = (int)$v['value'];
		}

		// Default type
		else {

			if ( empty( $v ) ) {
				$final_value = null;
				$delete = true;
			} else if ( !is_array( $v ) ) {

				$final_value = trim($v);
			} else if ( isset( $v['delete'] ) ) {

				$final_value 	= null;
				$delete 		= true;
			}
		}


		// Set column value
		if ( !is_null( $final_value ) ) {

			$object->set( $k, $final_value );

		// Delete column
		} else if( $delete ) {
			
			$object->delete( $k );
		}

		if ( !empty( $relations ) ) {

			
			// REaltion columns
			foreach( $relations as $relation_column => $relation_objects ) {				

				// Check if we want to merge the objects or not
				if ( isset( $v['merge'] ) && !$v['merge'] ) {					
					// Delete unslected 
					try {
						$object->fetch();
						$object_relation = $object->getRelation( $relation_column );
						$query           = $object_relation->getQuery();
						
						$object_relation->remove( $query->find() );
					} catch (Exception $e) {						
						// ignore excepttion
					}		

				}

				if ( !empty( $relation_objects ) ) {
					// Relation
					$object_relation = $object->getRelation( $relation_column );
					// Add the objects to the relation
					$object_relation->add( $relation_objects );
				}
			}
		}
	}

	// Save object
	$object->save( TAM_PARSE_MASTER_KEY );

	// Delete uploaded files
	foreach( $unlinks as $v ) {

		unlink( $v );
	}

	// Return the object's id
	return $object->getObjectId();
}



function tam_get_users_by_role( $role ) 
{
	static $role_users = array();

	if ( isset( $role_users[ $role ] ) ) {
		return $role_users[ $role ];
	}

	$role_users[ $role ] = array();

	$query_role = new Parse\ParseQuery( '_Role' );
	$query_role->equalTo( 'name', $role );
	$query_role->limit(1);
	$roles = $query_role->find();

	if ( !isset( $roles[0] ) ) {
		
		return array();
	}

	$role_editor 	= $roles[0];
	$relation 		= $role_editor->getRelation( 'users' );
	$query 			= $relation->getQuery();

	$role_users[ $role ] = $query->find();

	return $role_users[ $role ];
}


function tam_get_role( $role )
{
	$query_role = new Parse\ParseQuery( '_Role' );
	$query_role->equalTo( 'name', $role );
	$query_role->limit(1);
	$roles = $query_role->find();

	if ( !isset( $roles[0] ) ) {
		
		throw new Exception("Role doesnt exists.", 1);
	}

	return $roles[0];
}


function tam_user_is_editor( $user_id ) 
{
	return tam_user_has_role( $user_id, 'Editor' );
}


function tam_user_has_role( $user_id, $role ) 
{
	$users = tam_get_users_by_role( $role );

	foreach( (array)$users as $user ) {
		if ( $user_id == $user->getObjectId() ) {
			return true;
		}
	}

	return false;
}


function tam_update_user_role( $user_id, $role )
{
	$role_object = tam_get_role( $role );

	$relation = $role_object->getRelation('users');

	$relation->add(Parse\ParseObject::create('_User', $user_id));

	$role_object->save( TAM_PARSE_MASTER_KEY );
}


function tam_remove_user_role( $user_id, $role )
{
	$role_object = tam_get_role( $role );

	$relation = $role_object->getRelation('users');

	$relation->remove(Parse\ParseObject::create('_User', $user_id));

	$role_object->save( TAM_PARSE_MASTER_KEY );
}



function tam_register_menu_type( $type, $args = array() )
{
	global $tam_menu_types;

	if ( !$tam_menu_types ) {
		
		$tam_menu_types = array();
	}

	$defaults = array(
		'label' 			=> $type,
		'default_icon_url' 	=> 'http://placehold.it/64x64',
		'description'		=> '',
		'fields'			=> array(
			'title' 		=> array(
				'label' 	=> 'Title',
				'required' 	=> 1,
				'name'		=> 'title',
				'description' => 'The label of the menu.',
				'input_attributes' => array(
					'class' => 'tam-form-control'
				)
			),
			'visibility' 	=> array(
				'label' 	=> 'Visibility',
				'required' 	=> 1,
				'name'		=> 'visibility',
				'type'		=> 'select',
				'choices'	=> array(
					'public' 	=> 'Public',
					'login' 	=> 'Logged in Users',
					'attendees' => 'Attendees of event'
				),
				'description' => 'Make the menu visible to specific user status.',
				'input_attributes' => array(
					'class' => 'tam-form-control'
				)
			),
			'status'		=> array(
				'parse' 	=> array(
					'type' => 'boolean'
				),
				'name'		=> 'editor',
				'label'		=> 'Editor Mode',
				'type'		=> 'boolean',
				'description' => 'When editor mode is enabled only users with editor role can view the menu in the app.<br/>
		This will be helpful to preview the content in the app first without displaying it to the public users of the app.'
			),
			'enabled'		=> array(
				'name'			=> 'enabled',
				'label'			=> 'Enabled',
				'type'			=> 'boolean',
				'default'	=> true,
				'description' 	=> 'This will just hide the menu when disabled.',
				'parse' 	=> array(
					'type' => 'boolean'
				)
			)
		)
	);

	if ( isset( $args['fields'] ) ) {
		
		$args['fields'] = wp_parse_args( $args['fields'], $defaults['fields'] );
	}

	$args = wp_parse_args( $args, $defaults );

	$tam_menu_types[ $type ] = $args;

	return $args;
}



function tam_get_menu_type( $type )
{
	global $tam_menu_types;

	if ( isset( $tam_menu_types[$type] ) ) {
		
		return $tam_menu_types[$type];
	}

	return new WP_Error( 'not_found', 'Menu type doesn\'t exists.' );
}


function tam_get_menu_type_label( $type )
{
	$menu_type = tam_get_menu_type( $type );

	if ( !is_wp_error( $menu_type ) ) {
		return $menu_type[ 'label' ];
	}

	return '';
}


function tam_get_menu_types() 
{
	global $tam_menu_types;

	return $tam_menu_types;
}


function tam_get_menu_type_selection()
{
	$types = tam_get_menu_types();

	$return = array();

	foreach( $types as $k => $v ) {
		$return[ $k ] = $v['label'];
	}

	return $return;
}

function tam_esc_field( $field )
{
	return wp_parse_args( $field, array(
		'label' => '',
		'type'	=> 'text',
		'id'	=> 'field-' . sanitize_html_class($field['name']),
		'name'	=> '',
		'description'	=> '',
		'wrapper_attributes' 	=> array(),
		'input_attributes' 		=> array(),
		'required'	=> 0,
		'wrapper'	=> true,
		'default' => ''
	));
}


function tam_render_field( $field )
{
	$field 				= tam_esc_field( $field );
	$type 				= $field['type'];
	$input_attributes 	= array(
		'id' 	=> $field['id'],
		'name' 	=> $field['name'],
	);
	$value = isset($field['value']) ? $field['value'] : $field['default'];
	$choices = isset($field['choices']) ? $field['choices'] : array(); 

	$input_attributes = wp_parse_args( $field['input_attributes'], $input_attributes );

	if ( $field['required'] ) {
		$input_attributes['required'] = 'required';
	}

	if ( $field['wrapper'] )
		do_action( 'tam/render_field/before', $field );

	$s = '';

	/**
	 * Select Field
	 */
	if ( 'select' == $type ) {

		$s = '<select '. tam_array_to_html_attribute_string( $input_attributes ) .'>';

		foreach( (array) $choices as $k => $v ) {
			$selected = false;

			if ( is_array( $value ) && in_array( $k, $value ) ) {
				$selected = true;
			} else {
				$selected = ($value == $k);
			}

			$s .= '<option value="'. $k .'" '. selected( $selected, true, false ) .'>'. $v .'</option>';
		}

		$s .= '</select>';

	} 

	/**
	 * Radio
	 */
	else if( 'radio' == $type ) {

		$s  = '<ul class="tam-field-radio-list">';
		foreach( (array) $choices as $k => $v ) {
			$s .= '<li>';
			$input_attributes['type'] = 'radio';
			$input_attributes['id'] = $input_attributes['id'] . '-' . $k;

			if ( $k == $value )
				$input_attributes['checked'] = 'checked';

			$s .= '<input ' . tam_array_to_html_attribute_string( $input_attributes ) . '>';
			$s .= '<label class="tam-field-radio-label" for="'. $input_attributes['id'] .'">'. $v .'</label>';
			$s .= '</li>';
		}		
		$s .= '</ul>';
	}


	/**
	 * Checklist
	 */
	else if( 'checklist' == $type ) {

		$checklist_args = array(
			'sortable' => 1,
		);

		$field = wp_parse_args( $field, $checklist_args );

		$s  = '<ul class="tam-field-list" ' . ( $field['sortable'] ? 'data-sortable' : '' ) . ' >';
		$id = $input_attributes['id'];
		foreach( (array) $choices as $k => $v ) {
			$s .= '<li>';
			$input_attributes['type'] 	= 'checkbox';
			$item_id 					= $id . '-' . $k;
			$input_attributes['id'] 	= $item_id;
			$input_attributes['value'] 	= $k;
			if ( in_array( $k, (array)$value ) )
				$input_attributes['checked'] = 'checked';

			$s .= '<input ' . tam_array_to_html_attribute_string( $input_attributes ) . '>';
			$s .= '<label class="tam-field-list-label" for="'. $item_id .'">'. $v .'</label>';
			$s .= '</li>';
		}		
		$s .= '</ul>';
	}

	else if( 'drop_selection' == $type ) {

		$selected  = array();
		$selection = array();
		$input_attributes['name'] = $field['name'].'[]';
		$input_attributes['type'] = 'hidden';

		// Get selected items
		foreach( $choices as $k => $v ) {
			if ( !in_array( $k, (array) $value ) ) {
				$selection[ $k ] = $v;
			}
		}

		foreach( (array)$value as $v ) {
			if ( isset( $choices[$v] ) ) {
				$selected[$v] = $choices[$v];
			}
		}

		$input_attributes['value'] = '';
		?>
		<div class="tam-drop-selection">
			<input <?php echo tam_array_to_html_attribute_string( $input_attributes ); ?>>
			<ul class="tam-drop-selection-panel selection" data-drop-selection-panel>

				<?php foreach( $selection as $k => $v ): 

					$input_attributes['value'] = $k;
					?>

					<li class="ui-state-default">
						<?php echo $v; ?>
						<input <?php echo tam_array_to_html_attribute_string( array_merge($input_attributes, array( 'disabled' => 'disabled' )) ); ?>>
					</li>

				<?php endforeach; ?>

			</ul>
			<ul class="tam-drop-selection-panel selected" data-drop-selection-panel>
				
				<?php foreach( $selected as $k => $v ): 

					$input_attributes['value'] = $k;
					?>

					<li class="ui-state-default">
						<?php echo $v; ?>
						<input <?php echo tam_array_to_html_attribute_string( $input_attributes ); ?>>
					</li>

				<?php endforeach; ?>

			</ul>
		</div>
		<?php
	}

	/**
	 * Parse Object Selection
	 */
	else if( 'parse_object' == $type ) {

		$field = wp_parse_args( $field, array(
			'parse_class' 		=> '',
			'parse_label_key' 	=> 'title',
			'parse_field_type'	=> 'select',
		));

		if ( !empty( $field['parse_class'] ) ) {
				
			$query 		= new Parse\ParseQuery( $field['parse_class'] );
			$query 		= apply_filters( 'tam/render_field/parse_object/query', $query, $field );
			$objects 	= $query->find();
			$choices 	= array();

			if ( !empty($field['allow_null']) && $field['allow_null'] ) {
				$choices[''] = '';
			}

			foreach( $objects as $object ) {
				$choices[ $object->getObjectId() ] = $object->get( $field['parse_label_key'] );
			}

			ob_start();

			tam_render_field( wp_parse_args( array(
				'choices' 	=> $choices,
				'type'		=> $field['parse_field_type'],
				'wrapper'	=> false,
			), $field ));

			$s = ob_get_clean();
		}

	}


	/**
	 * Boolean
	 */
	else if( 'boolean' == $type ) {

		$input_attributes['type'] 	= 'checkbox';

		$s  = '<ul class="tam-field-list">';
			$s .= '<li>';

				$input_attributes['type'] = 'checkbox';
				if ( $value == true ) {
					$input_attributes['checked'] = 'checked';
				}
				$s .= '<input ' . tam_array_to_html_attribute_string( $input_attributes ) . '>';

			$s .= '</li>';
		$s .= '</ul>';
	}


	/**
	 * Default text field
	 */
	else {
		$input_attributes['type'] = $type;
		$input_attributes['value'] = $value;

		$s = '<input ' . tam_array_to_html_attribute_string( $input_attributes ) . '/>';
	}

	$s = apply_filters( 'tam/render_field/field', $s, $field );

	echo $s;

	if ( $field['wrapper'] )
		do_action( 'tam/render_field/after', $field );
}


function tam_array_to_html_attribute_string( $array )
{
	$s = '';

	foreach( (array) $array as $k => $v )
		$s .= $k . '="' . esc_attr( $v ) . '" ';

	return $s;
}


function tam_render_field_before( $field ) 
{
	?>
	<div class="tam-field-group tam-field-wrapper-<?php echo $field['type']; ?> tam-field-wrapper-<?php echo $field['id'] ?>" <?php echo tam_array_to_html_attribute_string( $field['wrapper_attributes'] ); ?>>
		<label for="<?php echo $field['id'] ?>">
			<?php echo $field['label'] ?>
			<?php if( $field['required'] ): ?>
				<span class="tam-text-danger">*</span>
			<?php endif; ?>
		</label>
		<p class="description">
			<small><?php echo $field['description']; ?></small>
		</p>
	<?php
}

add_action( 'tam/render_field/before', 'tam_render_field_before', 10, 1 );


function tam_render_field_after( $field ) 
{
	?>
	</div>
	<?php
}
add_action( 'tam/render_field/after', 'tam_render_field_after', 10, 1 );



function tam_menu_type_fields( $menu_id, $type, $event_id ) 
{
	$data = Parse\ParseObject::create( 'Menu', $menu_id );

	if ( !is_null( $menu_id ) ) {
		
		$data->fetch();
		$type = $data->get( 'menuType' );
	}

	$menu = tam_get_menu_type( $type );

	echo '<div class="tam-menu-details">';
		echo '<h3>'. $menu['label'] .'</h3>';
		if ( is_null( $menu_id ) )
		echo '<p>'. $menu['description'] .'</p>';
	echo '</div>';

	foreach( (array) $menu['fields'] as $parse_column => $field ) {

		$field['event_id'] 	= $event_id;

		// Autofill fields
		if ( !is_null( $menu_id ) ) {

			if ( isset( $field['parse'] ) ) {
				
				$parse_type = $field['parse']['type'];

				if ( $parse_type == 'pointer' ) {
					$object 		= $data->get( $parse_column );
					$field['value'] = $object->getObjectId();
				} else if ( $parse_type == 'relation' ) {
					$relation 	= $data->get( $parse_column );
					$query 		= $relation->getQuery();
					$query->ascending('order');

					$choices = array();

					foreach( $query->find() as $object ) {
						$choices[] = $object->getObjectId();
					}

					$field['value'] = $choices;
				} else {
					$field['value'] = $data->get( $parse_column );	
				}
			} else {
				$field['value'] = $data->get( $parse_column );
			}

			$field['value'] = apply_filters( 'tam/menu_type/get_value', $field['value'], $parse_column, $field );
		}

		tam_render_field( $field );
	}
}
add_action( 'tam/menu_type/fields', 'tam_menu_type_fields', 10, 3 );




function tam_get_topic_questions($topic_id)
{
	// Vars
	$object = new ParseObject('Topic', $topic_id);
	$query  = new ParseQuery('Question');

	// Construct query
	$query->equalTo( 'topic', $object );

	$query->includeKey('user');

	// From new to old
	$query->ascending( 'createdAt' );


	// Execute query
	return $query->find();
}




