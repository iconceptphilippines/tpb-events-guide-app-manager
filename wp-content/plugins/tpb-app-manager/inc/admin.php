<?php

function tam_admin_add_menu()
{
	// Main events
	$load_main = add_menu_page( 'TPB App Manager', 'TPB App Manager', 'manage_options', 'tam', 'tam_admin_page_view', null, 81 );

	// Just added this to rename the child menu into "Events" and not "TPB App Manager"
	$load_sub = add_submenu_page( 'tam', 'Events', 'Events', 'manage_options', 'tam', 'tam_admin_page_view' );

	add_action( 'load-' . $load_main, 'tam_admin_loaded_main' );
	add_action( 'load-' . $load_sub, 'tam_admin_loaded_main' );

	// TODO
	$load_users = add_submenu_page( 'tam', 'Users', 'Users', 'manage_options', 'tam-users', 'tam_users_admin_page_view' );
	add_action( 'load-'. $load_users, 'tam_admin_loaded_main' );

	// TODO
	// add_submenu_page( 'tam', 'Settings', 'Settings', 'manage_options', 'tam-settings', '__return_false' );
}

add_action( 'admin_menu', 'tam_admin_add_menu' );


// Controls what view to render
function tam_admin_page_view()
{
	$id 	= filter_input( INPUT_GET , 'object_id' );
	$class 	= filter_input( INPUT_GET , 'object_class' );

	echo '<div class="wrap">';

	// Default event list
	if ( is_null( $class ) ) {

		// Get events table
		$table = tam_get_parse_table( 'events' );

		// Display list events view
		tam_view( 'list-event', array(
			'title' 		=> 'Events',
			'button' 		=> 'Create Event',
		));

		// Display events table
		$table->display();
	}
	// Single
	else {

		// Get the object data
		$object = Parse\ParseObject::create( $class, $id );
		$object->fetch();

		// Display view for detail
		tam_view( 'single-' . strtolower( $class ), array(
			'object_id' 	=> $id,
			'object_class' 	=> $class,
			'object'		=> $object
		));
	}

	echo '</div>';
}



function tam_admin_loaded_main()
{
	if ( isset( $_REQUEST['action'] ) ) {
		$action = (string) $_REQUEST['action'];
		do_action( 'tam/admin_page/action/' . $action, $_REQUEST );
	} else {
		
		do_action( 'tam/admin_page_load' );
	}

	add_filter( 'user_can_richedit', 'tam_wysywyd_remove_visual_tab' );
}


function tam_is_doing_ajax()
{
	if (defined('DOING_AJAX') && DOING_AJAX) {
		add_filter( 'user_can_richedit', 'tam_wysywyd_remove_visual_tab' );
	}
}

add_action('init', 'tam_is_doing_ajax');

function tam_wysywyd_remove_visual_tab( $can ) 
{
    return false;
}


// Create a hook when an event tab is loaded
function tam_admin_event_tab_load()
{
	if ( !empty( $_GET['object_class'] ) && $_GET['object_class'] == 'Event' ) {

		$active_tab = tam_get_event_active_tab();

		do_action( 'tam/event_'. $active_tab .'_tab_load', filter_input( INPUT_GET, 'object_id' ) );
	}
}

add_action( 'tam/admin_page_load', 'tam_admin_event_tab_load' );

function tam_users_admin_page_view()
{
	$id 	= filter_input( INPUT_GET , 'object_id' );
	$class 	= filter_input( INPUT_GET , 'object_class' );

	echo '<div class="wrap">';

	// Default event list
	if ( is_null( $class ) ) {

		// Get events table
		$table = tam_get_parse_table( 'users' );

		// Display list events view
		tam_view( 'list-user', array(
			'title' 		=> 'Users',
			'button' 		=> 'Add User',
		));

		// Display events table
		$table->display();
	}
	// Single
	else {

		// Get the object data
		$object = Parse\ParseObject::create( $class, $id );
		$object->fetch();
		$avatar = $object->get( 'avatar' );
		$avatar_url = '';

		if( $avatar ) {
			$avatar_url = $avatar->getURL();
		}

		// Display view for detail
		tam_view( 'single-user', array(
			'object_id' 	=> $id,
			'object_class' 	=> $class,
			'object'		=> $object,
			'username'		=> $object->get( 'username' ),
			'first_name'	=> $object->get( 'firstName' ),
			'middle_name'	=> $object->get( 'middleName' ),
			'last_name'		=> $object->get( 'lastName' ),
			'email_add'		=> $object->get( 'email' ),
			'mobile_no'		=> $object->get( 'mobileNo' ),
			'company'		=> $object->get( 'company' ),
			'position'		=> $object->get( 'position' ),
			'avatar'		=> $avatar_url
		));
	}

	echo '</div>';
}







