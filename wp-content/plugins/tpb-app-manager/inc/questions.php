<?php
use Parse\ParseObject;

/**
 * Registers the questions table
 */
function tam_questions_register_table()
{
	tam_register_parse_table( 'questions', 'Question', array(
		'columns'	=> array(
			'cb' 		=> ' ',
			'title' 	=> 'Question',
			'createdAt'	=> 'Created',
		),
		'autofill'	=> array(
			'title',
		),
		'default_column' => 'title',
		'sortable_columns' => array(
			'title' 	=> array( 'title', false ),
			'createdAt' 	=> array( 'createdAt', false )
		)
	) );
}

add_action( 'init', 'tam_questions_register_table' );


function tam_questions_export_questions_endpoint()
{
	add_rewrite_tag( '%tam_topic_id%', '([0-9a-zA-Z]+)' );
    add_rewrite_rule( 'export_questions/([0-9a-zA-Z]+)/?', 'index.php?tam_topic_id=$matches[1]', 'top' );
}

add_action( 'init', 'tam_questions_export_questions_endpoint' );


function tam_questions_export_questions_endpoint_content()
{
	global $wp_query;

	$topic_id = $wp_query->get('tam_topic_id');
	$html = '';

	if ( !$topic_id ) {
		return;
	}

	// Allow request only if the nonce is valid and there is a logged in user
	if (!wp_verify_nonce( filter_input(INPUT_GET, 'tam_nonce'), 'tam_nonce' ) || !is_user_logged_in()) {
		echo "Invalid request";
		die();
	}

	try {
		// Get all the questions from the topic
		$questions = tam_get_topic_questions($topic_id);


		if (COUNT($questions) == 0) {
			echo "No questions found to export.";
			die();
		}

		// topic title first
		$topic = new ParseObject('Topic', $topic_id);
		$topic->fetch();

		$html .= '
		<style>
		.table-header{ border-bottom: 1px solid #000; width: 100%; }
		li { padding: 0;margin-bottom: 20px; }
		ol { padding: 18px; }
		</style>
		';

		$html .= '<ol>';

		foreach( $questions as $k => $question ) {
			$user = $question->get('user');

			$html .= '<li>';
				$html .= '<strong>'. $user->get('firstName') . ' ' . $user->get('lastName') .'</strong> <br/> (<em>'. $user->get('username')  . ' - ' . $user->get('email') . '<em>) - <small><em> sent: '. $question->getCreatedAt()->format('F d Y h:i a') .'</em></small><br/><br/>';
				$html .= nl2br($question->get('question'));
			$html .= '</li>';
		}

		$html .= '</ol>';

		$pdf = new mPDF('','', 0, '', 15, 15, 21, 16, 9, 9 );
		$pdf->setHTMLHeader('
		<table class="table-header">
			<tbody>
				<tr>
					<td><h3>'. $topic->get('title') .' Questions</h3></td>
					<td style="text-align: right;"><small>Page {PAGENO}</small></td>
				</tr>
			</tbody>
		</table>
		');
		$pdf->WriteHTML($html);
		$pdf->Output(sanitize_file_name( $topic->get('title') .'-' .$topic->getObjectId() ).'.pdf', 'I');

	} catch (Exception $e) {
		echo $e->getMessage();
	}

	die();

}

add_action( 'template_redirect', 'tam_questions_export_questions_endpoint_content' );



function tam_get_topic_export_link($topic_id)
{
	return wp_nonce_url( site_url('export_questions/'. $topic_id), 'tam_nonce', 'tam_nonce' );
}




