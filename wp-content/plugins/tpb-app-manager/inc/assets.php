<?php


function tam_admin_assets()
{
	wp_register_style( 'jquery-ui-theme', 	'//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css' );

	wp_register_script( 'location-picker', 	TAM_URL . '/vendor/location-picker/locationpicker.jquery.min.js', array( 'google-map-lib' ), null, true );
	wp_register_script( 'google-map-lib', 	'//maps.google.com/maps/api/js?sensor=false&libraries=places', array(  ), null, true );
	wp_register_script( 'event-manager', 	TAM_URL . '/vendor/event-manager/event-manager.js', array( 'jquery' ), null, true );
	wp_register_script( 'dropzone', 	TAM_URL . '/vendor/dropzone/dropzone.js', array(), null, true );
	wp_register_style( 'dropzone', 	TAM_URL . '/vendor/dropzone/dropzone.css' );

	wp_register_style( 'tam-admin', 		TAM_URL . '/assets/css/admin.css', array( 'jquery-ui-theme', 'dropzone' ), null, 'all' );

	wp_register_script( 'tam', 				TAM_URL . '/assets/js/tam.js', array( 'jquery-ui-dialog', 'event-manager', 'dropzone', 'jquery-ui-datepicker', 'location-picker', 'jquery-ui-sortable' ), null, true );
	wp_register_script( 'tam-admin', 		TAM_URL . '/assets/js/admin.js', array( 'tam' ), null, true );

	wp_enqueue_script( 'tam-admin' );
	wp_enqueue_style( 'tam-admin' );

	wp_localize_script( 'tam', '_TAM', array(
		'o' => array(
			'ajaxUrl' 	=> admin_url( 'admin-ajax.php' ),
			'nonce'		=> wp_create_nonce( 'tam_nonce' )
		)
	));
}

add_action( 'admin_enqueue_scripts', 'tam_admin_assets' );

