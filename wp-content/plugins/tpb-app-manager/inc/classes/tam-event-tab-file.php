<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Files' ) ) :

class Tam_Event_Tab_Files extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'file', 'Files' );

		add_action( 'init', array( $this, 'register' ) );
		add_action( 'tam/ajax_upload/name=file', array( $this,'file_upload' ), 10, 2 );
	}

	public function load( $event_id )
	{
		parent::load( $event_id );

		add_filter( 'tam/get_object_edit_link',						array( $this, 'edit_link' ), 10, 3 );
		add_filter( 'tam_parse_list_table/file/query', 			array( $this, 'files_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/file/column', 		array( $this, 'files_columns' ), 10, 2 );
		add_filter( 'tam_parse_list_table/file/row_actions', 	array( $this, 'files_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/file/bulk_actions', 	array( $this, 'files_bulk_actions' ), 10, 2 );
		add_action( 'tam_parse_list_table/file/before_table',	array( $this, 'files_before_table' ), 10, 1 );

	}

	public function render( $event_id )
	{

		// Get file list table
		$table = tam_get_parse_table( 'file' );

		// Check if table has no errors
		if ( !is_wp_error( $table ) ) {

			// Display view for list file
			tam_view( 'list-file', array(
				'table' => $table
			));
		} else {

			// Alert whats the errors message
			tam_view( 'alert', array(
				'label' => $table->get_error_message(),
				'type' 	=> 'warning'
			));
		}
	}

	public function edit_link( $link, $object_id, $class )
	{
		return add_query_arg( array(
			'file_id' => $object_id
		));
	}

	public function register()
	{
		// Register File Table
		tam_register_parse_table( 'file', 'File', array(
			'columns'	=> array(
				'cb' 				=> ' ',
				'fileName' 			=> 'File Name',
				'fileType' 			=> 'Mime Type',
				'createdAt'			=> 'Uploaded',
			),
			'autofill'	=> array(
				'fileName',
				'createdAt',
			),
			'default_column' 	=> 'title',
			'sortable_columns' 	=> array(
				'fileName' 			=> array( 'fileName', false ),
				'fileType' 			=> array( 'fileType', false ),
				'createdAt' 		=> array( 'createdAt', false )
			),
			'searcheable_columns'	=> array(
				'fileName'
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'fileName' ),
			'primary_link'			=> false,
			'per_page'				=> 10
		));

	}

	public function file_upload( $file, $request )
	{
		$event_id = !empty( $request['event_id'] ) ? $request['event_id'] : '';

		if ( empty( $event_id ) ) {
			
			return;
		}

		$path 		= $file['file'];
		$type 		= $file['type'];
		$name 		= basename( $path );
		$event 		= Parse\ParseObject::create( 'Event', $event_id );
		$file 		= Parse\ParseObject::create( 'File' );

		// Set file data
		$file->set( 'fileSource', 'file' );
		$file->set( 'file', Parse\ParseFile::createFromFile( $path, $name, $type ) );
		$file->set( 'fileName', $name );
		$file->set( 'event', $event );

		$file->save();

		// Delete file
		unlink( $path );
	}

	public function files_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function files_columns( $column_name, $object ) 
	{
		if ( $column_name == 'fileType' ) {
			if ( $object->get( 'fileSource' ) == 'url' ) {
				echo $object->get('fileType');
			} else {
				echo $object->get('file')->getMimeType();
			}
		}
	}

	public function files_actions( $actions, $object, $table )
	{
		$actions = array();
		$stripped_link = remove_query_arg( array('s', 'paged') );

		$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete Permanently</a>';

		return $actions;
	}

	public function files_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;
	}

	public function files_before_table( $table )
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

}

new Tam_Event_Tab_Files();

endif;
