<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Schedules' ) ) :

class Tam_Event_Tab_Schedules extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'schedule', 'Schedules' );

		add_action( 'init', array( $this, 'register' ) );
		add_action( 'tam/ajax_upload/name=schedule_thumbnail', array( $this, 'thumbnail_uploaded' ), 10, 2 );
	}

	public function load( $event_id )
	{
		parent::load( $event_id );

		// add_filter( 'tam/get_object_edit_link', 					array( $this, 'schedule_edit_link' ), 10, 3 );
		add_filter( 'tam_parse_list_table/schedule/column', 		array( $this, 'schedule_column' ), 10, 2 );
		add_filter( 'tam_parse_list_table/schedule/query', 			array( $this, 'schedules_query' ), 10, 2 );
		add_filter( 'tam_parse_list_table/schedule/row_actions', 	array( $this, 'schedule_row_actions' ), 10, 3 );
		add_action( 'tam_parse_list_table/schedule/before_table', 	array( $this, 'before_table_force_delete' ), 10, 1 );
		add_filter( 'tam_parse_list_table/schedule/bulk_actions', 	array( $this, 'schedules_bulk_actions' ), 10, 2 );

		add_filter( 'tam/get_object_edit_link', 						array( $this, 'slots_edit_link' ), 10, 3 );
		add_filter( 'tam_parse_list_table/schedule_slot/query',			array( $this, 'slots_query' ), 10, 2 );
		add_filter( 'tam_parse_list_table/schedule_slot/row_actions', 	array( $this, 'slots_actions' ), 10, 3 );
		add_action( 'tam_parse_list_table/schedule_slot/before_table',	array( $this, 'slots_before_table' ), 10, 1 );
		add_filter( 'tam_parse_list_table/schedule_slot/bulk_actions',	array( $this, 'slots_bulk_actions' ), 10, 2 );

		add_filter( 'tam/get_object_edit_link', 						array( $this, 'slot_single_edit_link' ), 10, 3 );

		if ( $id = $this->is_single( 'schedule_slot_id' ) ) {

			$this->save_schedule_slot( $id );

		}
	}

	public function render( $event_id )
	{
		if ( $id = $this->is_single( 'schedule_id' ) ) {

			// Single slot
			if ( $slot_id = $this->is_single( 'schedule_slot_id' ) ) {

				$schedule_slot = Parse\ParseObject::create( 'ScheduleSlot', $slot_id );
				$schedule_slot->fetch();

				$title 			= $schedule_slot->get( 'title' );
				$content 		= $schedule_slot->get( 'content' );
				$venue_name 	= $schedule_slot->get( 'venueName' );
				$start_time 	= $schedule_slot->get( 'startTime' );
				$address		= $schedule_slot->get( 'address' );
				$end_time 		= $schedule_slot->get( 'endTime' );
				$coordinates 	= $schedule_slot->get( 'coordinates' );
				$image 			= $schedule_slot->get( 'image' );
				$image_url 		= '';
				$latitude 		= 0;
				$longitude 		= 0;

				if ( $image ) {
					
					$image_url = $image->getURL();
				}

				if ( $coordinates ) {
					
					$latitude  = $coordinates->getLatitude();
					$longitude = $coordinates->getLongitude();
				}

				tam_view( 'single-schedule-slot', array(
					'title' 			=> $title,
					'content' 			=> $content,
					'venue_name'		=> $venue_name,
					'start_time' 		=> $start_time,
					'end_time' 			=> $end_time,
					'address'			=> $address,
					'schedule_slot_id'	=> $id,
					'thumbnail_url' 	=> $image_url,
					'latitude' 			=> $latitude,
					'longitude' 		=> $longitude,
					'delete_url'		=> tam_get_object_delete_link( $id, 'ScheduleSlot', true, remove_query_arg( array( 'schedule_slot_id' ) ) )
				));

			} else {
				// List of schedule slots
				$table = tam_get_parse_table( 'schedule_slot' );

				if ( !is_wp_error( $table ) ) {

					tam_view( 'list-schedule-slots', array(
						'table' => $table
					));
				} else {

					tam_view( 'alert', array(
						'type' 	=> 'warning',
						'label' => $table->get_error_message()
					));
				}
			}
		} else {

			// List of schedules
			$table = tam_get_parse_table( 'schedule' );

			if ( !is_wp_error( $table ) ) {

				tam_view( 'list-schedules', array(
					'table' => $table
				));
			} else {

				tam_view( 'alert', array(
					'type' 	=> 'warning',
					'label' => $table->get_error_message()
				));
			}
		}
	}


	public function slots_edit_link( $link, $object_id, $class )
	{
		if ($class != 'Schedule') {
			return $link;
		}

		return add_query_arg( array(
			'schedule_id' => $object_id
		));
	}

	public function slot_single_edit_link( $link, $object_id, $class )
	{
		if ($class != 'ScheduleSlot') {
			return $link;
		}

		return add_query_arg( array(
			'schedule_slot_id' => $object_id
		));
	}

	public function schedule_row_actions( $actions, $object, $table )
	{
		$stripped_link 	= remove_query_arg( array('s', 'paged') );
		$delete_url 	= tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link );

		return array(
			'edit'		=> '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'">View Schedule Slots</a>',
			'delete' 	=> '<a data-action="confirm-delete" href="' . $delete_url . '">Delete</a>'
		);

	}

	public function slots_actions( $actions, $object, $table )
	{
		$stripped_link 	= remove_query_arg( array('s', 'paged') );
		$delete_url 	= tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link );

		return array(
			'edit'		=> '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'">Edit</a>',
			'delete' 	=> '<a data-action="confirm-delete" href="' . $delete_url . '">Delete</a>'
		);

	}

	public function schedules_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function slots_query( $query, $table )
	{
		$query->ascending('order');
		return $query;
	}


	public function schedule_column( $column_name, $object )
	{
		if( 'startDate' === $column_name ) {

			$start_date = $object->get( 'startDate' );
			echo ( $start_date != null ? $start_date->format('m/d/Y') : '' );
		}
		elseif( 'endDate' === $column_name ) {

			$end = $object->get( 'endDate' );
			echo ( $end != null ? $end->format('m/d/Y') : '' );
		}

	}

	public function before_table_force_delete()
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function slots_before_table()
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function schedules_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;

	}
	public function slots_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;
	}

	public function save_schedule_slot( $id )
	{
		if ( empty( $_POST ) ) {

			return;
		}

		// Get the submitted data
		$go_to_list = isset( $_POST['save_and_close'] );
		$title 		= $_POST['title'];
		$content 	= filter_input( INPUT_POST, 'content' );
		$venue_name = filter_input( INPUT_POST, 'venue_name' );
		$start_time = filter_input( INPUT_POST, 'start_time' );
		$end_time = filter_input( INPUT_POST, 'end_time' );
		$address 	= filter_input( INPUT_POST, 'address' );
		$latitude 	= filter_input( INPUT_POST, 'latitude', FILTER_VALIDATE_FLOAT );
		$longitude 	= filter_input( INPUT_POST, 'longitude', FILTER_VALIDATE_FLOAT );

		try {

			// Save the schedule details
			tam_save_object( 'ScheduleSlot', array(
				'title' 		=> $title,
				'content' 		=> $content,
				'venueName'		=> $venue_name,
				'startTime' 	=> $start_time,
				'endTime' 		=> $end_time,
				'coordinates' 	=> array(
					'type' 		=> 'geopoint',
					'latitude' 	=> $latitude,
					'longitude' => $longitude
				),
				'address'		=> $address,
			), $id );

			// This redirects to the list of schedules
			if ( $go_to_list ) {

				wp_redirect( remove_query_arg( array( 'schedule_slot_id' ) ) );
				die();
			}

		} catch (Exception $e) {

			echo $e->getMessage();
			die();
		}
	}

	public function register()
	{
		tam_register_parse_table( 'schedule', 'Schedule', array(
			'columns'	=> array(
				'cb' 				=> ' ',
				'title' 			=> 'Title',
				'startDate'			=> 'Start Date',
				'endDate'			=> 'End Date',
			),
			'autofill'	=> array(
				'title'
			),
			'default_column' 		=> 'title',
			'sortable_columns' 		=> array(
				'title' 			=> array( 'title', false ),
				'startDate' 		=> array( 'startDate', false ),
				'endDate' 			=> array( 'endDate', false )
			),
			'searcheable_columns'	=> array(
				'title'
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'title' ),
			'per_page'				=> 10
		) );

		tam_register_parse_table( 'schedule_slot', 'ScheduleSlot', array(
			'columns'	=> array(
				'cb' 				=> ' ',
				'ordering'			=> '',
				'title' 			=> 'Title',
				'startTime'			=> 'Start Time',
				'endTime'			=> 'End Time',
			),
			'autofill'	=> array(
				'title',
				'startTime',
				'endTime',
			),
			'default_column' 		=> 'title',
			'sortable_columns' 		=> array(
				'title' 			=> array( 'title', false )
			),
			'searcheable_columns'	=> array(
				'title'
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'title' ),
			'per_page'				=> 10
		) );

	}


	public function thumbnail_uploaded( $file, $data )
	{
		$schedule = Parse\ParseObject::create( 'ScheduleSlot', $data['schedule_slot_id'] );
		$path = $file['file'];
		$file = Parse\ParseFile::createFromFile( $path, basename( $path ), $file['type'] );
		$schedule->set( 'image', $file );

		try {

			$schedule->save();

			$schedule->fetch();

			$new_image = $schedule->get( 'image' );
			$image_url = '';

			if ( $new_image ) {

				$image_url = $new_image->getURL();
			}

			wp_send_json_success( array(
				'name'	=> $new_image->getName(),
				'url' 	=> $image_url
			));
		} catch (Exception $e) {

			wp_send_json_error( array(
				'message' => $e->getMessage()
			) );
		}
	}
}

new Tam_Event_Tab_Schedules();

endif; // end class exists check