<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Parse_List_Table' ) ) :

if( !class_exists( 'WP_List_Table' ) ) :

require_once ABSPATH . '/wp-admin/includes/class-wp-list-table.php';

endif;

class Parse_List_Table extends WP_List_Table
{
	public function prepare_items()
	{
		// Prepare parse query
		$query = $this->parse_query();
		$items = array();

		// Pagination params
		$per_page 		= $this->_args['per_page'];
		$current_page 	= $this->get_pagenum();
		$offset			= ( $per_page * ($current_page - 1) );


		// Parse query pagination
		$query->limit( $per_page );
		$query->skip( $offset );

		

		// Try to get data to parse.com
		try {
			
			$items = $query->find();
		} catch (Parse\ParseException $e) {
			
			$items = array();
		}

		// prepare columns
		$columns 	= $this->get_columns();
		$hidden 	= $this->get_hidden_columns();
		$sortable 	= $this->get_sortable_columns();

		// Set columns
		$this->_column_headers 	= array($columns, $hidden, $sortable);

		$total_items = 0;

		try {
			$total_items = $query->count();	
		} catch (Exception $e) {
			$total_items = 0;
		}
		
		$total_pages = ceil( $total_items / $per_page );

		// Pagination
		$this->set_pagination_args( array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
		));

		// Set table rows
		$this->items 			= $items;
	}

	public function parse_query()
	{
		$query = new Parse\ParseQuery( $this->_args['parse_class'] );

		// Search by titlte
		if ( isset( $_REQUEST['s'] ) ) {
			
			$keyword = filter_var( $_REQUEST['s'], FILTER_DEFAULT );

			if ( $keyword ) {
				
				$or_queries = array();
				foreach( $this->_args['searcheable_columns'] as $v ) {

					$query = new Parse\ParseQuery( $this->_args['parse_class'] );
					$query->equalTo( $v, $keyword );
					$or_queries[] = $query;
				}

				$query = Parse\ParseQuery::orQueries( $or_queries );
			}
		}

		$query->descending( 'createdAt' );

		// Sortables
		if ( !empty( $_REQUEST['orderby'] ) && !empty( $_REQUEST['order'] ) ) {
			
			$orderby 	= $_REQUEST['orderby'];
			$order 		= $_REQUEST['order'];

			if ( $order == 'asc' ) {
				
				$query->ascending( $orderby );
			} else {

				$query->descending( $orderby );
			}
		}

		return apply_filters( 'tam_parse_list_table/'. $this->_args['screen'] .'/query', $query, $this );
	}

	public function __construct( $args = array() )
	{
		$args = wp_parse_args( $args, array(
			'columns' 				=> array(),
			'ajax'					=> true,
			'default_column' 		=> '',
			'sortable_columns'		=> array(),
			'searcheable_columns' 	=> array(),
			'enable_search'			=> true,
			'primary_link'			=> true,
			'per_page'				=> 20,
			'autofill'				=> array()
		) );

		parent::__construct( $args );
	}

	public function column_default( $object, $column_name ) 
	{
		$primary = $this->get_default_primary_column_name() == $column_name;

		if ( $this->is_column_fillable( $column_name ) ) {
			
			$val = $object->get( $column_name );

			if ( is_string( $val ) && !$primary ) {

				echo $val;
			} else if( $primary ) {

				$this->column_primary( $object );
			}
		} else {
			if ( $primary ) {
				
				$this->column_primary( $object );
			}
		}

		do_action( 'tam_parse_list_table/'.  $this->_args['screen'] .'/column', $column_name, $object );
	}

	public function is_column_fillable( $column_name )
	{
		return in_array( $column_name, $this->_args[ 'autofill' ] );
	}

	public function get_columns()
	{
		return apply_filters( 'tam_parse_list_table/'.  $this->_args['screen'] .'/columns', $this->_args['columns'] );
	}


	public function column_primary( $object )
	{
		$trash = $this->is_status( 'trash' );

		$link = tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() );

		if ( !$trash && !empty( $link ) && $this->_args['primary_link'] ) 
			echo '<a href="'. $link .'">';

				echo '<strong>';
					echo apply_filters( 'tam_parse_list_table/'. $this->_args['screen'] .'/primary_column_text', $object->get( $this->get_default_primary_column_name() ), $object );
				echo '</strong>';

		if( !$trash && !empty( $link ) && $this->_args['primary_link'] )
			echo '</a>';

		$this->handle_row_actions( $object, $this->get_default_primary_column_name(), $this->get_default_primary_column_name() );
	}

	public function column_createdAt( $object )
	{
		echo $object->getCreatedAt()->format( get_option( 'date_format' ) );
	}

	public function column_ordering( $object ) 
	{
		echo '<div class="tam-hamburger" data-id="'. $object->getObjectId() .'" data-parse-class="'. $object->getClassName() .'">';
			echo '<span></span>';
			echo '<span></span>';
			echo '<span></span>';
		echo '</div>';
	}


	public function get_hidden_columns()
	{
		return apply_filters( 'tam_parse_list_table/' . $this->_args['screen'] . '/hidden_columns', array() );
	}

	public function is_status( $status )
	{
		return $this->get_current_status() == $status;
	}


	public function get_statuses()
	{
		return apply_filters( 'tam_parse_list_table/' . $this->_args['screen']. '/views', array(), $this );
	}


	public function get_current_status()
	{
		$current = !empty( $_REQUEST['object_status'] ) ? $_REQUEST['object_status'] : 'publish';

		if ( !in_array( $current, array_keys( $this->get_statuses() ) ) ) {
			
			$current = 'publish';
		}

		return $current;
	}


	public function get_default_primary_column_name()
	{
		return $this->_args['default_column'];
	}


	// Sortable columns
	public function get_sortable_columns()
	{
		$columns = $this->_args['sortable_columns'];

		return apply_filters( 'tam_parse_list_table/' . $this->_args['screen'] . '/sortable_columns' , $columns );
	}



	public function handle_row_actions( $object, $column_name, $primary )
	{
		if ( $column_name != $primary ) {
			
			return;
		}

		$actions = apply_filters( 'tam_parse_list_table/' . $this->_args['screen'] . '/row_actions', array(), $object, $this );

		return $this->row_actions( $actions );
	}


	// Content of 'cb' column
	public function column_cb( $object ) 
	{
		$id = $object->getObjectId();

		?>
			<label class="screen-reader-text" for="cb-select-<?php echo $id; ?>">Select</label>
			<input id="cb-select-<?php echo $id; ?>" type="checkbox" name="object_ids[]" value="<?php echo $id; ?>" />
			<div class="locked-indicator"></div>
		<?php
	}


	public function display()
	{
	    $this->views();

	    if ( $this->_args['enable_search'] ) {

			echo '<form method="GET">';

				$ignored = array( 'paged', 's' );

				foreach( $_REQUEST as $k => $v ) {

					if ( in_array( $k, $ignored ) ) {
						
						continue;
					}

					echo '<input type="hidden" name="'. esc_attr( $k ) .'" value="'. esc_attr( $v ) .'">';
				}

			    $this->search_box( 'Search', 'keyword');

			echo '</form>';
		}


		echo '<form method="POST">';

			wp_nonce_field( 'tam_nonce', 'tam_nonce' );

			echo '<input type="hidden" value="'. ( remove_query_arg( array( 'paged', 's' ) ) ) .'" name="redirect_url">';
			echo '<input type="hidden" value="'. esc_attr( $this->_args['parse_class'] ) .'" name="object_class">';

			do_action( 'tam_parse_list_table/' . $this->_args['screen'] . '/before_table', $this );

			parent::display();

		echo '</form>';
	}


	public function get_bulk_actions()
	{
		return apply_filters( 'tam_parse_list_table/' . $this->_args['screen'] . '/bulk_actions', array(), $this );
	}

	public function extra_tablenav( $which = '' )
	{
		?>
		<div class="alignleft actions bulkactions">
			
			<?php do_action( 'tam_parse_list_table/' . $this->_args['screen'] . '/extra_nav', $this ); ?>
		
		</div>
		<?php
	}


	public function get_views()
	{
		$current = !empty( $_REQUEST['object_status'] ) ? $_REQUEST['object_status'] : 'publish';

		$views = $this->get_statuses();

		if ( !in_array( $current, array_keys( $views ) ) ) {
			
			$k = array_keys( $views );

			if ( isset( $k[0] ) ) {

				$current = $k[0];
			}
		}

		$links = array();
		
		foreach( $views as $k => $v ) {

			$old_link 	= remove_query_arg( array( 'paged', 's' ) );
			$link 		= add_query_arg( array( 'object_status' => $k ), $old_link );

			$links[ $k ] = '<a href="'. esc_url_raw( $link ) .'" class="'. ( $current == $k ? 'current' : '' ) .' tam-status tam-status-'. $k .'">'. $v .'</a>';
		}

		return $links;
	}

	public function ajax_response()
	{
		check_ajax_referer( 'tam_nonce', '_tam_nonce' );
 
	    $this->prepare_items();
	 
	    extract( $this->_args );
	    extract( $this->_pagination_args, EXTR_SKIP );
	 
	    ob_start();
	    if ( ! empty( $_REQUEST['no_placeholder'] ) )
	        $this->display_rows();
	    else
	        $this->display_rows_or_placeholder();
	    $rows = ob_get_clean();
	 
	    ob_start();
	    $this->print_column_headers();
	    $headers = ob_get_clean();
	 
	    ob_start();
	    $this->pagination('top');
	    $pagination_top = ob_get_clean();
	 
	    ob_start();
	    $this->pagination('bottom');
	    $pagination_bottom = ob_get_clean();
	 
	    $response = array( 'rows' => $rows );
	    $response['pagination']['top'] = $pagination_top;
	    $response['pagination']['bottom'] = $pagination_bottom;
	    $response['column_headers'] = $headers;
	 
	    if ( isset( $total_items ) )
	        $response['total_items_i18n'] = sprintf( _n( '1 item', '%s items', $total_items ), number_format_i18n( $total_items ) );
	 
	    if ( isset( $total_pages ) ) {
	        $response['total_pages'] = $total_pages;
	        $response['total_pages_i18n'] = number_format_i18n( $total_pages );
	    }
	 
	    die( wp_json_encode( $response ) );
	}


	public function get_table_classes()
	{
		$classes 	= parent::get_table_classes();

		if ( isset($this->_args['columns']['ordering']) ) {
			
			$classes[] = 'parse-list-table-sortable';
		}

		$classes[] 	= 'parse-list-table';
		$classes[] 	= 'parse-list-table-' . sanitize_html_class( $this->_args['screen'] );
		return $classes;
	}
}

endif; // end class exists check