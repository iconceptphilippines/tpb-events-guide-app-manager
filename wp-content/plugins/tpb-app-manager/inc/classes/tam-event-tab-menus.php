<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Menus' ) ) :

class Tam_Event_Tab_Menus extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'menus', 'Menus' );
		add_action( 'init', array( $this, 'register' ) );
		add_action( 'tam/render_field/parse_object/query', array( $this, 'menu_parse_object' ), 10, 2 );
		add_filter( 'tam/save_menu_field', array( $this, 'editor_mode_modify_saving' ), 10, 3 );
		add_filter( 'tam/menu_type/get_value', array( $this, 'editor_mode_modify_value' ), 10, 3 );
	}

	public function load( $event_id ) 
	{
		parent::load( $event_id );

		add_filter( 'tam_parse_list_table/menus/query', array( $this, 'menu_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/menus/column', array( $this, 'menu_column' ), 10, 2 );
		add_action( 'tam_parse_list_table/menus/row_actions', array( $this, 'menu_row_actions' ), 10, 3 );
	}

	public function menu_query( $query, $table ) 
	{
		$event 		= Parse\ParseObject::create( 'Event', $this->event_id );
		$event->fetch();
		$relation 	= $event->getRelation( 'menus' );
		$query 		= $relation->getQuery();

		$query->ascending( 'order' );

		return $query;
	}

	public function menu_column( $column_name, $menu )
	{
		if ( 'type' == $column_name ) {
			echo tam_get_menu_type_label( $menu->get( 'menuType' ) );
		} else if( 'enabled' == $column_name ) {
			echo '<div class="tam-center"><input type="checkbox" data-id="'. $menu->getObjectId() .'" data-action="enable-menu" '. checked( true, $menu->get( 'enabled' ), false ) .'></div>';
		} else if( 'editor_mode' == $column_name ) {
			echo '<div class="tam-center"><input type="checkbox" data-id="'. $menu->getObjectId() .'" data-action="editor-mode-menu" '. checked( true, $menu->get( 'status' ) == 'editor', false ) .'></div>';
		}
	}

	public function menu_row_actions( $actions, $object, $table )
	{
		$stripped_link 		= remove_query_arg( array( 's', 'paged' ) );
		$actions['edit'] 	= '<a href="#" data-action="edit-menu" data-id="'. $object->getObjectId() .'" data-type="'. $object->get('menuType') .'">Edit</a>';
		$actions['delete'] 	= '<a data-action="confirm-delete" href="' . tam_get_object_delete_link( $object->getObjectId(), 'Menu', true, $stripped_link ) . '" class="tam-text-danger">Delete</a>';

		return $actions;
	}

	public function render( $event_id )
	{
		$table = tam_get_parse_table( 'menus' );

		tam_view( 'list-menu', array(
			'table' => $table,
			'types' => tam_get_menu_type_selection()
		));
	}

	public function register()
	{
		tam_register_parse_table( 'menus', 'Menu', array(
			'columns'				=> array(
				'ordering'		=> '',
				'title' 		=> 'Menu',
				'type'			=> 'Type',
				'enabled'		=> 'Enabled',
				'editor_mode'	=> 'Editor Mode',
			),
			'autofill'				=> array(
				'title',
			),
			'default_column' 		=> 'title',
			'enable_search'			=> false,
			'primary_link'			=> false
		));

		tam_register_menu_type( 'info', array(
			'label' 	=> 'General Information',
			'description' => 'Menu that displays the general information about the event.'
		));

		tam_register_menu_type( 'page', array(
			'label' 	=> 'Page',
			'fields'	=> array(
				'page'	=> array (
					'parse' => array(
						'type' 	=> 'pointer',
						'class'	=> 'Page'
					),
					'required'		=> 1,
					'type'			=> 'parse_object',
					'parse_class' 	=> 'Page',
					'label' 	=> 'Page',
					'description' 	=> 'Select a page to display in the menu.',
					'name'			=> 'page',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control',
					),
					'allow_null'	=> true
				)
			)
		));


		tam_register_menu_type( 'list', array(
			'label' 	=> 'List',
			'fields'	=> array(
				'pages'	=> array (
					'parse'				=>array(
						'type' 	=> 'relation',
						'class'	=> 'Page',
						'merge' => false
					),
					'parse_field_type' => 'drop_selection',
					'type'			=> 'parse_object',
					'parse_class' 	=> 'Page',
					'label' 		=> 'Pages',
					'description' 	=> 'Drag the pages you want to display in list on the right.',
					'name'			=> 'pages',
				)
			)
		));

		tam_register_menu_type( 'social_media', array(
			'label' 	=> 'Social Media',
			'fields'	=> array(
				'facebookLabel'	=> array (
					'label' 	=> 'Facebook Label',
					'name'			=> 'facebook_label',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control'
					),
				),
				'facebookLink'	=> array (
					'label' 	=> 'Facebook Page Link',
					'name'			=> 'facebook_page_link',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control'
					),
				),
				'twitterLabel'	=> array (
					'label' 	=> 'Twitter Label',
					'name'			=> 'twitter_label',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control'
					),
				),
				'twitterTweet'	=> array (
					'label' 	=> 'Twitter Tweet',
					'name'			=> 'twitter_tweet',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control'
					),
				)
			)
		));

		tam_register_menu_type( 'files', array(
			'label' 	=> 'Downloadables',
			'fields'	=> array(
				'files'	=> array (
					'parse'				=>array(
						'type' 	=> 'relation',
						'class'	=> 'File',
						'merge' => false
					),
					'parse_field_type' => 'drop_selection',
					'type'			=> 'parse_object',
					'parse_class' 	=> 'File',
					'parse_label_key' => 'fileName',
					'label' 		=> 'Files',
					'description' 	=> 'Select the files available for download.',
					'name'			=> 'files',
				)
			)
		));

		tam_register_menu_type( 'gallery', array(
			'label' 	=> 'Gallery',
			'fields'	=> array(
				'gallery'	=> array (
					'parse' => array(
						'type' 	=> 'pointer',
						'class'	=> 'Gallery'
					),
					'required'		=> 1,
					'type'			=> 'parse_object',
					'parse_class' 	=> 'Gallery',
					'label' 		=> 'Gallery',
					'description' 	=> 'Select a gallery to display.',
					'name'			=> 'gallery',
					'input_attributes' => array(
						'class' 	=> 'tam-form-control',
					),
					'allow_null'	=> true
				)
			)
		));

		tam_register_menu_type( 'announcements', array(
			'label'       => 'Announcements',
			'description' => 'Displays all the announcements of the event.'
		));

		tam_register_menu_type( 'questions', array(
			'label'       => 'Session Questions',
			'description' => 'Displays all the topics where the user can ask their question.'
		));

		tam_register_menu_type( 'attendees', array(
			'label'       => 'Attendees',
			'description' => 'Displays the list attendees in the event.'
		));

		tam_register_menu_type( 'schedule', array(
			'label'       	=> 'Schedule',
			'fields'		=> array(
				'schedules'	=> array(
					'parse'	=> array(
						'type' 	=> 'relation',
						'class'	=> 'Schedule',
						'merge' => false
					),
					'parse_field_type' => 'drop_selection',
					'type'             => 'parse_object',
					'parse_class'      => 'Schedule',
					'parse_label_key'  => 'title',
					'label'            => 'Schedules',
					'description'      => 'Select schedules to be displayed.',
					'name'             => 'schedules'
				)
			)
		));
	}


	public function menu_parse_object( $query, $field )
	{
		$query->ascending('title');
		if ( $field['event_id'] ) {
			
			$query->equalTo( 'event', Parse\ParseObject::create( 'Event', $field['event_id'] ) );
		}

		return $query;
	}

	public function editor_mode_modify_saving( $value, $column, $type )
	{
		if ( $column == 'status' ) {
			
			if ( $value['value'] == true ) {
				
				$value = 'editor';
			} else {
				$value = 'publish';
			}
		}

		return $value;
	}
	
	public function editor_mode_modify_value( $value, $column, $type ) 
	{
		if ( $column != 'status' ) {
			return $value;
		}

		if ( $value == 'editor' ) {
			return true;
		} else {
			return false;
		}
	}
}

new Tam_Event_Tab_Menus();

endif;