<?php

class Tam_Parse_Object extends Parse\ParseObject
{
	public static $parseClassName = null;

	public function __set($key, $value)
    {
        if ($key != 'className'
        ) {
            $this->set($key, $value);
        } else {
            throw new Exception('Protected field could not be set.');
        }
    }


    public function copy()
    {
    	$this->fetch();

    	$clone = clone $this;
    	$clone->fetch();
    	$clone->objectId = null;
    	$clone->set( 'isDuplicate', true );
    	$clone->set( 'duplicatedObject', $this );

    	foreach( $this->serverData as $k => $v ) {

    		$clone->_performOperation($k, new Parse\Internal\SetOperation($v));
    	}

    	return $clone;
    }
}