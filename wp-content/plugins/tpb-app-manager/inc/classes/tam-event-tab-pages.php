<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Pages' ) ) :

class Tam_Event_Tab_Pages extends Tam_Event_Tab
{
	public function __construct()	
	{
		parent::__construct( 'pages', 'Pages' );

		add_action( 'init', array( $this, 'register' ) );
		add_action( 'tam/ajax_upload/name=page_thumbnail', array( $this, 'thumbnail_uploaded' ), 10, 2 );
	}

	public function load( $event_id )
	{
		parent::load( $event_id );

		add_filter( 'tam/get_object_edit_link', 				array( $this, 'page_edit_link' ), 10, 3 );
		add_filter( 'tam_parse_list_table/pages/query', 		array( $this, 'page_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/pages/before_table', 	array( $this, 'before_table_force_delete' ) );
		add_filter( 'tam_parse_list_table/pages/bulk_actions', 	array( $this, 'bulk_actions' ), 10, 2 );
		add_filter( 'tam_parse_list_table/pages/row_actions', 	array( $this, 'page_row_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/pages/column', 		array( $this, 'page_column' ), 10, 2 );

		if ( $id = $this->is_single( 'page_id' ) ) {

			$this->save_page( $id );
		}
	}

	public function render( $event_id )
	{
		if ( $id = $this->is_single( 'page_id' ) ) {
			
			$page = Parse\ParseObject::create( 'Page', $id );
			$page->fetch();

			$coordinates = $page->get( 'coordinates' );
			$image 		= $page->get( 'image' );
			$image_url  = '';
			$longitude  = 0;
			$latitude   = 0;

			if ( $image ) {
				
				$image_url = $image->getURL();
			}

			if ( $coordinates ) {
				
				$latitude  = $coordinates->getLatitude();
				$longitude = $coordinates->getLongitude();
			}

			tam_view( 'single-page', array(
				'title' 		=> $page->get('title'),
				'subtitle' 		=> $page->get('subtitle'),
				'content' 		=> $page->get('content'),
				'latitude' 		=> $latitude,
				'longitude' 	=> $longitude,
				'page_id'		=> $id,
				'thumbnail_url' => $image_url,
				'delete_url'	=> tam_get_object_delete_link( $id, 'Page', true, remove_query_arg( array( 'page_id' ) ) )
			));
		} else {

			$table = tam_get_parse_table( 'pages' );

			if ( !is_wp_error( $table ) ) {
				
				tam_view( 'list-page', array(
					'table' => $table
				));
			} else {

				tam_view( 'alert', array(
					'type' 	=> 'warning',
					'label' => $table->get_error_message()
				));
			}
		}
	}

	public function page_edit_link( $link, $object_id, $class )
	{
		return add_query_arg( array( 
			'page_id' => $object_id 
		));
	}

	public function page_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function before_table_force_delete()
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function bulk_actions( $actions, $table )
	{
		return array(
			'delete' => 'Delete'
		);
	}

	public function page_row_actions( $actions, $object, $table )
	{
		$stripped_link 	= remove_query_arg( array('s', 'paged') );
		$delete_url 	= tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link );

		return array(
			'edit'		=> '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'" target="_new">Edit</a>',
			'delete' 	=> '<a data-action="confirm-delete" href="' . $delete_url . '">Delete</a>'
		);
	}

	public function page_column( $column_name, $page )
	{
		if ( 'thumbnail' === $column_name ) {

			$image = $page->get( 'image' );

			if ( !empty( $image ) ) {
				
				echo '<div class="tam-thumbnail tam-thumbnail-sm" style="background-image:url('. esc_url( $image->getURL() ) .')"></div>';
			} else {

				echo '<div class="tam-thumbnail tam-no-thumbnail tam-thumbnail-sm"></div>';
			}
		}
	}

	public function save_page( $id )
	{
		if ( empty( $_POST ) ) {

			return;
		}

		// Get the submitted data
		$go_to_list = isset( $_POST['save_and_close'] );
		$title 		= $_POST['title'];
		$subtitle 	= filter_input( INPUT_POST , 'subtitle' );
		$content 	= filter_input( INPUT_POST, 'content' );
		$latitude 	= filter_input( INPUT_POST, 'latitude', FILTER_VALIDATE_FLOAT );
		$longitude 	= filter_input( INPUT_POST, 'longitude', FILTER_VALIDATE_FLOAT );
		$address 	= filter_input( INPUT_POST, 'address' );
		$venue_name = filter_input( INPUT_POST, 'venue_name' );


		try {
			
			// Save the page details
			tam_save_object( 'Page', array(
				'title' 		=> $title,
				'subtitle' 		=> $subtitle,
				'content' 		=> $content,
				'coordinates' 	=> array(
					'type' 		=> 'geopoint',
					'latitude' 	=> $latitude,
					'longitude' => $longitude
				),
				'address'		=> $address,
				'venueName'		=> $venue_name
			), $id );

			// This redirects to the list of pages
			if ( $go_to_list ) {
				
				wp_redirect( remove_query_arg( array( 'page_id' ) ) );
				die();
			}

		} catch (Exception $e) {
			
			echo $e->getMessage();
			die();
		}
	}

	public function register()
	{
		tam_register_parse_table( 'pages', 'Page', array(
			'columns'				=> array(
				'cb' 			=> ' ',
				'thumbnail' 	=> ' ',
				'title' 		=> 'Title',
				'createdAt'		=> 'Created',
			),
			'autofill'				=> array(
				'title',
			),
			'default_column' 		=> 'title',
			'sortable_columns' 		=> array(
				'title' 		=> array( 'title', false ),
				'createdAt' 	=> array( 'createdAt', false )
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'title' ),
		) );
	}


	public function thumbnail_uploaded( $file, $data )
	{
		$page = Parse\ParseObject::create( 'Page', $data['page_id'] );
		$path = $file['file'];
		$file = Parse\ParseFile::createFromFile( $path, basename( $path ), $file['type'] );
		$page->set( 'image', $file );

		try {
			
			$page->save();

			$page->fetch();

			$new_image = $page->get( 'image' );
			$image_url = '';

			if ( $new_image ) {
				
				$image_url = $new_image->getURL();
			}

			wp_send_json_success( array(
				'name'	=> $new_image->getName(),
				'url' 	=> $image_url
			));
		} catch (Exception $e) {
			
			wp_send_json_error( array(
				'message' => $e->getMessage()
			) );
		}
	}
}

new Tam_Event_Tab_Pages();

endif; // end class exists check