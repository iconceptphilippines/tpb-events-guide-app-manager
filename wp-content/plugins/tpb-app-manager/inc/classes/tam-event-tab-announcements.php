<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Announcements' ) ) :

class Tam_Event_Tab_Announcements extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'announcement', 'Announcements' );

		add_action( 'init', array( $this, 'register' ) );
	}

	public function load( $event_id )
	{
		parent::load( $event_id );

		add_filter( 'tam/get_object_edit_link',							array( $this, 'edit_link' ), 10, 3 );

		add_filter( 'tam_parse_list_table/announcement/query', 			array( $this, 'announcements_query' ), 10, 2 );
		add_filter( 'tam_parse_list_table/announcement/row_actions', 	array( $this, 'announcements_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/announcement/bulk_actions', 	array( $this, 'announcements_bulk_actions' ), 10, 2 );
		add_action( 'tam_parse_list_table/announcement/before_table',	array( $this, 'announcements_before_table' ), 10, 1 );

	}

	public function render( $event_id )
	{

		// Get announcement list table
		$table = tam_get_parse_table( 'announcement' );

		// Check if table has no errors
		if ( !is_wp_error( $table ) ) {

			// Display view for list announcement
			tam_view( 'list-announcement', array(
				'table' => $table
			));
		} else {

			// Alert whats the errors message
			tam_view( 'alert', array(
				'label' => $table->get_error_message(),
				'type' 	=> 'warning'
			));
		}
	}

	public function edit_link( $link, $object_id, $class )
	{
		return add_query_arg( array(
			'announcement_id' => $object_id
		));
	}

	public function register()
	{
		// Register Announcement Table
		tam_register_parse_table( 'announcement', 'Announcement', array(
			'columns'	=> array(
				'cb' 				=> ' ',
				'title' 			=> 'Announcement',
				'content' 			=> 'Message',
				'createdAt'			=> 'Created',
			),
			'autofill'	=> array(
				'title',
				'content'
			),
			'default_column' 	=> 'title',
			'sortable_columns' 	=> array(
				'title' 			=> array( 'title', false ),
				'createdAt' 		=> array( 'createdAt', false )
			),
			'searcheable_columns'	=> array(
				'title'
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'title' ),
			'primary_link'			=> false,
			'per_page'				=> 10
		));

	}

	public function announcements_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function announcements_actions( $actions, $object, $table )
	{
		$actions = array();
		$stripped_link = remove_query_arg( array('s', 'paged') );

		$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete Permanently</a>';

		return $actions;
	}

	public function announcements_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;
	}

	public function announcements_before_table( $table )
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

}

new Tam_Event_Tab_Announcements();

endif;
