<?php

abstract class Tam_Event_Tab
{
	protected $label;
	protected $slug;
	protected $event_id;

	protected function __construct( $slug, $label )
	{
		$this->slug 	= $slug;
		$this->label 	= $label;

		// Hook the load of the tab
		add_action( 'tam/event_'. $this->slug .'_tab_load',     array( $this, 'load' 	) );

		// Add the tab
		add_action( 'tam/event_tabs', 							array( $this, '_tabs' 	) );

		// Hook the content of the tab
		add_action( 'tam/event_'. $this->slug .'_tab_content', 	array( $this, 'render' 	) );
	}

	public function _tabs( $tabs )
	{
		$tabs[ $this->slug ] = $this->label;

		return $tabs;
	}

	protected function is_single( $param )
	{
		$id = filter_input( INPUT_GET, $param );

		if ( is_null( $id ) ) {
			
			return false;
		}

		return $id;
	}

	public function load( $event_id ) 
	{
		$this->event_id = $event_id;
	}
	
	abstract public function render( $event_id );
}