<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Gallery' ) ) :

class Tam_Event_Tab_Gallery extends Tam_Event_Tab 
{
	public function __construct()
	{
		parent::__construct( 'gallery', 'Gallery' );

		// Register
		add_action( 'init', array( $this, 'register' ) );

		// Upload photos
		add_action( 'tam/ajax_upload/name=photo', array( $this,'photo_upload' ), 10, 2 );
	}

	/**
	 * On tab load
	 */
	public function load( $event_id )
	{
		parent::load( $event_id );

		// This changes the edit link of the gallery
		add_filter( 'tam/get_object_edit_link', 			array( $this, 'gallery_edit_link' ), 10, 3 );

		// This customizes the query of the gallery table
		add_filter( 'tam_parse_list_table/gallery/query', 	array( $this, 'gallery_query' ), 10, 2 );
		add_filter( 'tam_parse_list_table/photo/query', 	array( $this, 'photo_query' ), 10, 2 );

		// Bulk actions
		add_filter( 'tam_parse_list_table/gallery/bulk_actions', array( $this, 'bulk_actions' ), 10, 2 );
		add_filter( 'tam_parse_list_table/photo/bulk_actions', array( $this, 'bulk_actions' ), 10, 2 );

		// Add a force delete input hidden field to the delet bulk action will permanently delete 
		// the items
		add_action( 'tam_parse_list_table/gallery/before_table', array( $this, 'before_table_force_delete' ) );
		add_action( 'tam_parse_list_table/photo/before_table', array( $this, 'before_table_force_delete' ) );

		// Customize row actions of gallery
		add_filter( 'tam_parse_list_table/gallery/row_actions', array( $this, 'gallery_row_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/photo/row_actions', array( $this, 'photo_row_actions' ), 10, 3 );

		// Gallery columns content
		add_filter( 'tam_parse_list_table/gallery/column', array( $this, 'gallery_column' ), 10, 2 );
		add_filter( 'tam_parse_list_table/photo/column', array( $this, 'photo_column' ), 10, 2 );		
	}


	/**
	 * The content of the tab
	 */
	public function render( $event_id )
	{
		// Displays the single gallery view
		if ( $id = $this->is_single( 'gallery_id' ) ) {
			
			$gallery = Parse\ParseObject::create( 'Gallery', $id );

			// Get gallery column data
			$gallery->fetch();

			// Photos
			$table = tam_get_parse_table( 'photo' );

			// Create view for single gallery
			tam_view( 'single-gallery', array(
				'object_id' => $gallery->getObjectId(),
				'title'		=> $gallery->get( 'title' ),
				'table' 	=> $table
			));			
		}


		// Displays the gallery list
		else {

			// Display view for list gallery
				// tam_view( 'list-gallery', array(
				// ));
				// return;
			// Get gallery list table
			$table = tam_get_parse_table( 'gallery' );

			// Check if table has no errors
			if ( !is_wp_error( $table ) ) {
				
				// Display view for list gallery
				tam_view( 'list-gallery', array(
					'table' => $table
				));
			} else {

				// Alert whats the errors message
				tam_view( 'alert', array(
					'label' => $table->get_error_message(),
					'type' 	=> 'warning'
				));
			}
		}
	}

	public function gallery_edit_link( $link, $object_id, $class )
	{
		return add_query_arg( array( 
			'gallery_id' => $object_id 
		));
	}

	public function gallery_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function photo_query( $query, $table )
	{
		$object = Parse\ParseObject::create( 'Gallery', $this->is_single( 'gallery_id' ) );

		$query->equalTo( 'gallery', $object );

		return $query;
	}

	public function gallery_row_actions( $actions, $object, $table ) 
	{
		$stripped_link 	= remove_query_arg( array('s', 'paged') );
		$delete_url 	= tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link );

		return array(
			'view' 		=> '<a href="#">View</a>',
			'edit_name' => '<a href="#" data-action="edit-gallery-name-list" data-title="'. $object->get('title') .'" data-object-id="'. $object->getObjectId() .'">Edit Name</a>',
			'delete' 	=> '<a href="' . $delete_url . '">Delete</a>'
		);
	}

	public function photo_row_actions( $actions, $object, $table )
	{
		$stripped_link 	= remove_query_arg( array('s', 'paged') );
		$delete_url 	= tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link );

		$photo_url = '';

		if ( $file = $object->get( 'image' ) ) {
			
			$photo_url = $file->getURL();
		}

		return array(
			'edit_name'	=> '<a href="#" target="_new" data-action="edit-photo-name-list" data-object-id="'. $object->getObjectId() .'" data-name="'. $object->get('name') .'">Edit Name</a>',
			'view'		=> '<a href="'. $photo_url .'" target="_new">View Photo</a>',
			'delete' 	=> '<a href="' . $delete_url . '">Delete</a>'
		);
	}

	public function before_table_force_delete()
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function bulk_actions( $actions, $table )
	{
		return array(
			'delete' => 'Delete'
		);
	}

	public function gallery_column( $column_name, $gallery )
	{
		if ( 'photos_count' === $column_name ) {
			
			echo tam_get_object_pointer_count( $gallery, 'Photo', 'gallery' );
		} else if ( 'thumbnail' === $column_name ) {

			$urls = tam_get_gallery_thumbnail_urls( $gallery->getObjectId() );

			if ( !empty( $urls[0] ) ) {
				
				echo '<div class="tam-thumbnail tam-thumbnail-sm" style="background-image:url('. esc_url( $urls[0] ) .')"></div>';
			} else {

				echo '<div class="tam-thumbnail tam-no-thumbnail tam-thumbnail-sm"></div>';
			}
		}
	}


	public function photo_column( $column_name, $photo )
	{
		$file = $photo->get( 'image' );

		if ( 'thumbnail' === $column_name ) {

			if ( $file ) {
				
				echo '<a href="'. $file->getURL() .'" target="_new"><div class="tam-thumbnail tam-thumbnail-sm" style="background-image:url('. esc_url( $file->getURL() ) .')"></div></a>';
			} else {

				echo '<div class="tam-thumbnail tam-no-thumbnail tam-thumbnail-sm"></div>';
			}
		}
	}


	public function photo_upload( $file, $request )
	{
		$gallery_id = !empty( $request['gallery_id'] ) ? $request['gallery_id'] : '';

		if ( empty( $gallery_id ) ) {
			
			return;
		}

		$path 		= $file['file'];
		$type 		= $file['type'];
		$name 		= basename( $path );
		$gallery 	= Parse\ParseObject::create( 'Gallery', $gallery_id );

		$image = Parse\ParseFile::createFromFile( $path, $name, $type );
		$photo = Parse\ParseObject::create( 'Photo' );

		$photo->set( 'name', $name );
		$photo->set( 'gallery', $gallery );
		$photo->set( 'image', $image );

		$photo->save();

		unlink( $path );
	}

	public function register()
	{
		// gallery table
		tam_register_parse_table( 'gallery', 'Gallery', array(
			'columns'				=> array(
				'cb' 			=> ' ',
				'thumbnail' 	=> ' ',
				'title' 		=> 'Title',
				'photos_count'	=> 'No. of Photos',
				'createdAt'		=> 'Created',
			),
			'autofill'				=> array(
				'title',
			),
			'default_column' 		=> 'title',
			'sortable_columns' 		=> array(
				'title' 		=> array( 'title', false ),
				'createdAt' 	=> array( 'createdAt', false )
			),
			'enable_search'			=> true,
			'searcheable_columns'	=> array( 'title' ),
			'per_page'				=> 10
		));

		// Photo table
		tam_register_parse_table( 'photo', 'Photo', array(
			'columns'	=> array(
				'cb' 		=> ' ',
				'thumbnail' => ' ',
				'name' 		=> 'Name',
				'createdAt'	=> 'Created',
			),
			'autofill'	=> array(
				'name',
			),
			'default_column' => 'name',
			'sortable_columns' => array(
				'name' 		=> array( 'name', false ),
				'createdAt' 	=> array( 'createdAt', false )
			),
			'searcheable_columns'	=> array( 'name' ),
			'enable_search'	=> true,
			'primary_link'	=> false,
			'per_page'		=> 3
		));
	}
}

new Tam_Event_Tab_Gallery();

endif;
