<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_General' ) ) :

class Tam_Event_Tab_General extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'general', 'General' );
	}

	public function load( $event_id )
	{
		if( !empty( $_POST ) ) {

			try {

				tam_save_object( 'Event', array(
					'title' 	=> $_POST['tam_title'],
					'content'	=> $_POST['tam_content'],
					'address'	=> $_POST['event_location'],
					'venueName'	=> filter_input( INPUT_POST, 'tam_venue_name' ),
					'startDate'	=> array(
						'type' => 'date',
						'date'	=> $_POST['tam_start_date']
					),
					'endDate'	=> array(
						'type' => 'date',
						'date'	=> $_POST['tam_end_date']
					),
					'coordinates' => array(
						'type' 		=> 'geopoint',
						'latitude' 	=> $_POST['tam_latitude'],
						'longitude' => $_POST['tam_longitude'],
					),
					'image'		=> array(
						'type' => 'file',
					),
					'city'		=> $_POST['tam_city'],
					'country'	=> $_POST['tam_country'],
					'zipCode'	=> $_POST['tam_zipcode'],
					'isPrivate' => array(
						'type' => 'boolean',
						'value' => isset( $_POST['tam_public'] ) ? false : true
					),
					'status'	=> filter_input( INPUT_POST , 'status'),
				), $event_id);
			} catch (Exception $e) {

				tam_view( 'alert', array(
					'type' 	=> 'warning',
					'label' => $e->getMessage()
				));
			}
		}

	}

	public function render( $event_id )
	{

		// Markup rendering
		?>

		<div id="post-stuff" class="event-post-stuff">

			<div class="tam-single-header"></div>

			<form action="" method="post" enctype="multipart/form-data">

				<div id="post-body" class="event-post-body">

					<div id="post-body-content">

						<div id="single-event">

							<?php

							$event = new Parse\ParseObject('Event', $event_id );

							try {

								$event->fetch();
								$start_date = $event->get('startDate');
								$end_date = $event->get('endDate');

							} catch (Exception $e) {

							}

							$coordinates = $event->get('coordinates');

							?>

								<input type="text" name="tam_title" id="event-title" value="<?php echo $event->get('title'); ?>" placeholder="Title (required)" required>

								<div class="field-group">

									<?php

									$content 				= $event->get('content');
									$editor_id 				='tam_content';
									$desciption_settings 	= array(
										'media_buttons'		=> false,
										'textarea_rows'		=> 6,
									);
									wp_editor( $content, $editor_id, $desciption_settings );

									?>
								</div>

								<div class="field-group">

									<div class="field-col">

										<div class="field-col-2">

											<label>Start Date <i>(required)</i></label>

											<input type="text" name="tam_start_date" id="start-event" value="<?php echo ( $start_date != null ? $start_date->format('m/d/Y') : '' ); ?>" required pattern="(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d" placeholder="mm/dd/yyyy" >

										</div>


										<div class="field-col-2">

											<label>End Date</label>

											<input type="text" name="tam_end_date" id="end-event" value="<?php echo ( $end_date != null ? $end_date->format('m/d/Y') : '' ); ?>" pattern="(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d" placeholder="mm/dd/yyyy">
										</div>

									</div>
								</div>

								<div class="tam-postbox">

									<div class="field-group">

										<h3 class="tam-postbox-title">Venue Name</h3>

										<div class="tam-postbox-content">
											<div class="tam-postbox-content-inner">
												<input type="text" name="tam_venue_name" id="tam-venue-name" value="<?php echo $event->get( 'venueName' ); ?>">
											</div>
										</div>

									</div>

								</div>

								<div class="tam-postbox">

									<div class="field-group">

										<h3 class="tam-postbox-title">Map</h3>

										<div class="tam-postbox-content">

											<div class="tam-postbox-content-inner">

												<input type="text" name="event_location" id="event-location">
												<input type="hidden" name="tam_longitude" id="map-long" value="<?php echo ( $coordinates !=null ? $coordinates->getLongitude() : '' ); ?>">
												<input type="hidden" name="tam_latitude" id="map-lat" value="<?php echo ( $coordinates !=null ? $coordinates->getLatitude() : '' ); ?>">
												<input type="hidden" name="tam_city" id="city">
												<input type="hidden" name="tam_country" id="country">
												<input type="hidden" name="tam_zipcode" id="zipcode">
												<div id="location-map"></div>

											</div>

										</div>

									</div>

								</div>

								<div class="tam-postbox" style="display: none;">

									<h3 class="tam-postbox-title">Social Media</h3>

									<div class="tam-postbox-content">

										<div class="tam-postbox-content-inner">

											<div class="field-group">

												<div class="field-col">

													<div class="field-col-2">

														<label>Facebook Label:</label>
														<input type="text" name="tam_facebook_label" id="facebook-label" value="<?php echo ( $event->get('facebookLabel') != null ? $event->get('facebookLabel') : '' ); ?>">

													</div>

													<div class="field-col-2">

														<label>Facebook Link:</label>
														<input type="text" name="tam_facebook_link" id="facebook-link" value="<?php echo ( $event->get('facebookLink') != null ? $event->get('facebookLink') : '' ); ?>">

													</div>

												</div>

											</div>

											<div class="field-group">

												<div class="field-col">

													<div class="field-col-2">

														<label>Twitter Label:</label>
														<input type="text" name="tam_twitter_label" id="twitter-label" value="<?php echo ( $event->get('twitterLabel') != null ? $event->get('twitterLabel') : '' ); ?>">

													</div>

													<div class="field-col-2">

														<label>Twitter Hashtag:</label>
														<input type="text" name="tam_twitter_hashtag" id="twitter-hashtag" value="<?php echo ( $event->get('twitterHashtag') != null ? $event->get('twitterHashtag') : '' ); ?>">

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

						</div>
						<!-- #single-event -->

					</div>
					<!-- #post-body-content -->



					<div class="tam-aside">

						<div class="tam-postbox">

							<?php $file = $event->get( 'image' ); ?>

							<h3 class="tam-postbox-title">Banner</h3>

							<div class="tam-postbox-content">

								<div class="tam-postbox-content-inner tam-clearfix">
									<?php
									if ( $file ) {
										echo '<img src="'. esc_url( $file->getURL() ) .'" alt="" class="tam-thumbnail tam-thumbnail-rect-sm">';
										//echo '<div class="tam-thumbnail" style="background-image:url('. esc_url( $file->getURL() ) .')"></div>';
									} else {

										echo '<div class="tam-thumbnail tam-no-thumbnail"></div>';
									}
									?>
									<input type="file" name="banner" id="banner">
								</div>

							</div>

						</div>

						<div class="tam-postbox">

							<h3 class="tam-postbox-title">Event Status</h3>

							<div class="tam-postbox-content">

								<div class="tam-postbox-content-inner tam-clearfix">

									<div class="field-group">

										<select name="status" id="status">
											<option <?php echo ( $event->get('status') == 'publish' ? 'selected' : ''); ?> value="publish">Publish</option>
											<option <?php echo ( $event->get('status') == 'editor' ? 'selected' : ''); ?> value="editor">Editor Mode</option>
										</select>

									</div>

									<div class="field-group">

										<label><input type="checkbox" name="tam_public" id="public" value="" <?php echo ( !$event->get('isPrivate') ? 'checked="checked"' : '' ); ?>> Public</label>

									</div>


								</div>

								<div  class="tam-postbox-content-inner tam-postbox-content-gray tam-clearfix">

									<input type="submit" name="save_event" class="button button-primary tam-right tam-button-inline save-event" value="Save Event">

								</div>

							</div>
						</div>

					</div>

				</div>

			</form>

		</div>

		<?php

	}
}

new Tam_Event_Tab_General();

endif;
