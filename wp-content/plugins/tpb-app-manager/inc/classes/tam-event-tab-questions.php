<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Question' ) ) :

class Tam_Event_Tab_Question extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'questions', 'Questions' );

		add_action( 'init', array( $this, 'register' ) );
	}

	public function load( $event_id )
	{
		parent::load( $event_id );

		add_filter( 'tam/get_object_edit_link', 				array( $this, 'edit_link' ), 10, 3 );

		add_filter( 'tam_parse_list_table/topic/query', 		array( $this, 'topics_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/topic/column', 		array( $this, 'topics_column' ), 10, 2 );
		add_filter( 'tam_parse_list_table/topic/row_actions', 	array( $this, 'topics_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/topic/bulk_actions', 	array( $this, 'topics_bulk_actions' ), 10, 2 );
		add_action( 'tam_parse_list_table/topic/before_table',	array( $this, 'topics_before_table' ), 10, 1 );

		add_filter( 'tam_parse_list_table/question/query',			array( $this, 'questions_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/question/column', 		array( $this, 'questions_column' ), 10, 2 );
		add_filter( 'tam_parse_list_table/question/row_actions', 	array( $this, 'questions_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/question/bulk_actions', 	array( $this, 'questions_bulk_actions' ), 10, 2 );
		add_filter( 'tam_parse_list_table/question/primary_column_text', array( $this, 'questions_primary_column_text' ), 10, 2 );
		add_action( 'tam_parse_list_table/question/before_table',	array( $this, 'questions_before_table' ), 10, 1 );
	}

	public function render( $event_id )
	{
		// Single
		if ( $id = $this->is_single( 'topic_id' ) ) {

			$topic = Parse\ParseObject::create( 'Topic', $id );
			$topic->fetch();

			// Get topic list table
			$table = tam_get_parse_table( 'question' );

			// Check if table has no errors
			if ( !is_wp_error( $table ) ) {

				// Display view for list topic
				tam_view( 'list-question', array(
					'title'			=> $topic->get( 'title' ),
					'table' 		=> $table,
					'object_id'		=> $id
				));
			} else {

				// Alert whats the errors message
				tam_view( 'alert', array(
					'label' => $table->get_error_message(),
					'type' 	=> 'warning'
				));
			}
		}
		// List
		else {

			// Get topic list table
			$table = tam_get_parse_table( 'topic' );

			// Check if table has no errors
			if ( !is_wp_error( $table ) ) {

				// Display view for list topic
				tam_view( 'list-topic', array(
					'table' => $table
				));
			} else {

				// Alert whats the errors message
				tam_view( 'alert', array(
					'label' => $table->get_error_message(),
					'type' 	=> 'warning'
				));
			}
		}
	}

	public function edit_link( $link, $object_id, $class )
	{
		return add_query_arg( array(
			'topic_id' => $object_id
		));
	}

	public function register()
	{
		// Register Topic Table
		tam_register_parse_table( 'topic', 'Topic', array(
			'columns'	=> array(
				'cb' 				=> ' ',
				'title' 			=> 'Topic',
				'question_count' 	=> 'Number of Questions',
				'createdAt'			=> 'Created',
			),
			'autofill'	=> array(
				'title',
			),
			'default_column' => 'title',
			'sortable_columns' => array(
				'title' 	=> array( 'title', false ),
				'createdAt' 	=> array( 'createdAt', false )
			),
			'searcheable_columns'	=> array(
				'title'
			),
			'enable_search'	=> true,
			'per_page'		=> 10
		));

		// Register Question Tabe
		tam_register_parse_table( 'question', 'Question', array(
			'columns'	=> array(
				'cb' 			=> ' ',
				'question' 		=> 'Question',
				'q_user'		=> 'User',
				'createdAt'		=> 'Submitted Date',
			),
			'autofill'	=> array(
				'question',
			),
			'default_column' => 'question',
			'sortable_columns' => array(
				'question' 	=> array( 'question', false ),
				'createdAt' 	=> array( 'createdAt', false )
			),
			'enable_search'	=> false,
			'primary_link'	=> false,
			'per_page'		=> 10
		));
	}

	public function topics_query( $query, $table )
	{
		$event = Parse\ParseObject::create( 'Event', $this->event_id );

		$query->equalTo( 'event', $event );

		return $query;
	}

	public function questions_query( $query, $table )
	{

		$topic_id = $_GET['topic_id'];

		$topic = Parse\ParseObject::create( 'Topic', $topic_id );

		$query->equalTo( 'topic', $topic );
		$query->includeKey( 'user' );

		return $query;
	}

	public function topics_column( $column_name, $object )
	{
		if ( 'question_count' == $column_name ) {

			$query = new Parse\ParseQuery( 'Question' );
			$query->equalTo( 'topic', $object );
			$questions = $query->find();

			echo count( $questions );
		}
	}

	public function questions_column( $column_name, $object )
	{
		if ( 'q_user' == $column_name ) {

			$user = $object->get( 'user' );

			if( $user ) {
				$full_name = $user->get('firstName') . ' ' . $user->get('lastName');
				echo '<a href="'. tam_get_user_edit_link( $user->getObjectId() ) .'">' . $user->getUsername() . ' ('. $full_name .')</a>';
			}
		}
	}

	public function topics_actions( $actions, $object, $table )
	{
		$actions = array();
		$stripped_link = remove_query_arg( array('s', 'paged') );

		$actions['edit_name'] = '<a href="#" data-action="edit-topic-name-list" data-title="'. $object->get('title') .'" data-object-id="'. $object->getObjectId() .'">Edit Name</a>';
		$actions['edit'] = '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'">View Questions</a>';
		$actions['export'] = '<a target="_new" href="'. tam_get_topic_export_link( $object->getObjectId() ) .'">Export Questions</a>';
		$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete Permanently</a>';

		return $actions;
	}

	public function questions_actions( $actions, $object, $table )
	{
		$actions = array();
		$stripped_link = remove_query_arg( array('s', 'paged') );

		$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete Permanently</a>';

		return $actions;
	}

	public function topics_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;
	}

	public function questions_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'delete' ] = 'Delete';
		return $actions;
	}

	public function questions_primary_column_text( $text, $object )
	{
		$content = preg_replace('~\s*<br ?/?>\s*~',"<br />",$text);
		return nl2br($content);
	}

	public function topics_before_table( $table )
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function questions_before_table( $table )
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

}

new Tam_Event_Tab_Question();

endif;
