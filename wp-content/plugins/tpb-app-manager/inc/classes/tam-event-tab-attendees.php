<?php

// Prevent direct file access
if( !defined( 'WPINC' ) ) {

	die();
}

if ( !class_exists( 'Tam_Event_Tab_Attendees' ) ) :

class Tam_Event_Tab_Attendees extends Tam_Event_Tab
{
	public function __construct()
	{
		parent::__construct( 'attendees', 'Attendees' );

		// Register
		add_action( 'init', array( $this, 'register' ) );

	}

	/**
	 * On tab load
	 */
	public function load( $event_id )
	{
		parent::load( $event_id );

		add_filter( 'tam_parse_list_table/attendees/query',					array( $this, 'attendees_query' ), 10, 2 );
		add_action( 'tam_parse_list_table/attendees/column',				array( $this, 'attendees_column' ), 10, 2 );
		add_filter( 'tam_parse_list_table/attendees/primary_column_text',	array( $this, 'attendees_primary_column_text' ), 10, 2 );
		add_filter( 'tam_parse_list_table/attendees/row_actions',			array( $this, 'attendees_row_actions' ), 10, 3 );
		add_filter( 'tam_parse_list_table/attendees/bulk_actions', 			array( $this, 'attendees_bulk_actions' ), 10, 2 );
		add_action( 'tam_parse_list_table/attendees/before_table',			array( $this, 'attendees_before_table' ), 10, 1 );
		add_action( 'tam_parse_list_table/attendees/extra_nav',				array( $this, 'attendees_extra_nav' ), 10, 1 );



	}

	/**
	 * The content of the tab
	 */
	public function render( $event_id )
	{
		$table = tam_get_parse_table( 'attendees' );

		// Check if table has no errors
		if ( !is_wp_error( $table ) ) {

			// Display view for list gallery
			tam_view( 'list-attedees', array(
				'table' => $table
			));
		} else {

			// Alert whats the errors message
			tam_view( 'alert', array(
				'label' => $table->get_error_message(),
				'type' 	=> 'warning'
			));
		}
	}

	public function register()
	{
		global $wpdb;

		// attendees table
		tam_register_parse_table( 'attendees', 'EventAttendee', array(
			'columns'				=> array(
				'cb' 			=> ' ',
				'thumbnail'		=> ' ',
				'name'			=> 'Name',
				'company'		=> 'Company',
				'status'		=> 'User Status',
				'checkin'		=> 'Checked In'
			),
			'autofill'				=> array(),
			'default_column' 		=> 'name',
			'sortable_columns' 		=> array(
				'name' 				=> array( 'name', false ),
			),
			'enable_search'			=> false,
			'per_page'				=> 10
		));
	}

	public function attendees_query( $query, $table )
	{
		$event 		= Parse\ParseObject::create( 'Event', $this->event_id );
		$query->equalTo( 'event', $event );
		$innerQuery = $query;

		if ( isset( $_GET['attendee_status'] ) ) {
			if ($_GET['attendee_status'] == 'unapproved') {
				$query->equalTo('isApproved', false);
				$innerQuery->doesNotExist('isApproved');
			} elseif( $_GET['attendee_status'] == 'approved' ) {
				$query->equalTo('isApproved', true);
			}
		}

		$finalQuery = Parse\ParseQuery::orQueries( [$query, $innerQuery] );
		$finalQuery->includeKey('user');
		return $finalQuery;
	}

	public function attendees_column( $column_name, $object )
	{
		$user = $object->get( 'user' );

		if( $user ) {
			if ( 'status' == $column_name ) {

				$user_stat = $object->get( 'isApproved' );
				if( true == $user_stat ) {
					echo 'Approved';
				} else {
					echo 'Unapproved';
				}
			} elseif ( 'thumbnail' == $column_name ) {

				$file = $user->get( 'avatar' );

				if ( $file ) {

					echo '<div class="tam-thumbnail tam-thumbnail-sm" style="background-image:url('. esc_url( $file->getURL() ) .')"></div>';
				} else {

					echo '<div class="tam-thumbnail tam-no-thumbnail tam-thumbnail-sm"></div>';
				}
			} elseif ( 'company' == $column_name ) {

				echo $user->get( 'company' );
				echo "<br/>";
				echo $user->get( 'position' );
			} elseif( 'checkin' == $column_name ) {

				$disabled = !$object->get('checkedIn');
			
				echo '<div class=" tam-checkin-icon '. ($disabled ? 'disabled' : '') .'"><i class="dashicons dashicons-yes"></i></div>';
				if (!$disabled && $checkin = $object->get('checkInTime')) {
					$checkin->setTimeZone(new DateTimeZone('Asia/Manila'));
					echo '<br/><time datetime="'. ($checkin->format('F d, Y h:i:s a')) .'">'.$checkin->format('F d, Y h:i:s a').'</time>';
				}
			}
		}
	}

	public function attendees_primary_column_text( $text, $object )
	{
		$user = $object->get( 'user' );

		if( $user ) {
			$first_name = $user->get( 'firstName' );
			$middle_name = $user->get( 'middleName' );
			$last_name = $user->get( 'lastName' );

			if( $middle_name != '' ) {

				$name = $first_name.' '.$middle_name.' '.$last_name;
			} else {

				$name = $first_name.' '.$last_name;
			}

			return $name;
		}
	}

	public function attendees_row_actions( $actions, $object, $table )
	{
		$user = $object->get( 'user' );
			if( $user  ) {
			$actions = array();
			$stripped_link = remove_query_arg( array('s', 'paged') );
			$user_stat = $object->get( 'isApproved' );

			$actions['edit'] = '<a href="'. tam_get_object_edit_link( $user->getObjectId(), $user->getClassName() ) .'">View</a>';
			$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Remove to Event</a>';

			if( false == $user_stat ) {

				$actions['attendee_status'] = '<a href="'. tam_get_approve_attendee_link( $object->getObjectId(), $stripped_link ) .'">Approve</a>';
			} else {

				$actions['attendee_status'] = '<a href="'. tam_get_unapprove_attendee_link( $object->getObjectId(), $stripped_link )  .'">Unapprove</a>';
			}

			return $actions;
		}
	}

	public function attendees_bulk_actions( $actions, $table )
	{
		$actions  = array();

		$actions[ 'approve_attendee' ] = 'Approve';
		$actions[ 'unapprove_attendee' ] = 'Unapprove';
		$actions[ 'delete' ] = 'Delete';

		return $actions;
	}

	public function attendees_before_table( $table )
	{
		echo '<input type="hidden" value="1" name="force_delete">';
	}

	public function attendees_extra_nav()
	{
		$current_status = isset( $_GET['attendee_status'] ) ? $_GET['attendee_status'] : '';
		$statuses 		= array( 'approved' => 'Approved', 'unapproved' => 'Unapproved' );

		if (isset( $_GET['attendee_status'] )) {
			unset( $_GET['attendee_status'] );
		}
		?>
		<form id="filter-form" method="GET">
			
			<div style="display: none;">
				<?php foreach( (array) $_GET as $k => $v ): ?>
					<input type="hidden" name="<?php echo $k ?>" value="<?php echo $v; ?>">
				<?php endforeach; ?>
			</div>
	
			<select onchange="attendeesFilterHiddenStatusField(this);">
				<option value="">- All Status -</option>
				<?php foreach( $statuses as $k => $v ): ?>
				<option value="<?php echo $k; ?>" <?php selected( $k, $current_status ) ?>><?php echo $v; ?></option>
				<?php endforeach; ?>
			</select>

			<script>
			function attendeesFilterHiddenStatusField(el)
			{
				(function($){
					var form = document.getElementById('filter-form');
					if (el.value.length > 0) {
						window.location = window.location.pathname + '?' + $(form).serialize() + '&attendee_status=' + el.value;
					} else {
						window.location = window.location.pathname + '?' + $(form).serialize();
					}
				})(jQuery);
			}
			</script>
		</form>
		<?php
	}

}
new Tam_Event_Tab_Attendees();

endif;