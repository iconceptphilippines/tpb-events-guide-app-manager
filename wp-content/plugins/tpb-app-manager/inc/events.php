<?php

/**
 * Registers the events table
 */
function tam_events_register_table()
{
	tam_register_parse_table( 'events', 'Event', array(
		'columns'	=> array(
			'cb' 		=> ' ',
			'thumbnail' => ' ',
			'title' 	=> 'Event',
			'quicklink' => 'Go to',
			'createdAt'	=> 'Created',
		),
		'autofill'	=> array(
			'title',
		),
		'default_column' => 'title',
		'sortable_columns' => array(
			'title' 	=> array( 'title', false ),
			'createdAt' 	=> array( 'createdAt', false )
		),
		'searcheable_columns'	=> array(
			'title'
		)
	) );
}

add_action( 'init', 'tam_events_register_table' );



/**
 * Fills the contents events table columns
 * @param  string 		$column_name The name of the column
 * @param  ParseObject 	$object      The object column
 * @return void
 */
function tam_events_columns_content( $column_name, $object )
{
	if ( 'thumbnail' == $column_name ) {

		$file = $object->get( 'image' );

		if ( $file ) {

			echo '<div class="tam-thumbnail tam-thumbnail-sm" style="background-image:url('. esc_url( $file->getURL() ) .')"></div>';
		} else {

			echo '<div class="tam-thumbnail tam-no-thumbnail tam-thumbnail-sm"></div>';
		}
	} else if( 'quicklink' == $column_name ) {
		$tabs 		= tam_get_event_tabs();
		?>
		<div class="tam-dropdown">
			<a data-dropdown href="#" class="page-title-action tam-page-title-action tam-dropdown-button" data-action="new-menu">
				<span>Go to</span>
			</a>
			<ul class="tam-dropdown-items">
				<?php foreach( $tabs as $k => $v ): ?>
					<li><a href="<?php echo add_query_arg( array( 'object_id' => $object->getObjectId(), 'object_class' => 'Event', 'event_tab' => $k ) ); ?>"><?php echo $v; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php
	}
}

add_action( 'tam_parse_list_table/events/column', 'tam_events_columns_content', 10, 2 );





function tam_events_table_query( $query, $table )
{
	$query->equalTo( 'status', $table->get_current_status() );

	return $query;
}

add_filter( 'tam_parse_list_table/events/query', 'tam_events_table_query', 10, 2 );




function tam_events_table_views( $views, $table )
{
	return array(
		'publish' => 'Publish',
		'editor'	  => 'Editor Mode',
		'trash'	  => 'Trash'
	);
}

add_filter( 'tam_parse_list_table/events/views', 'tam_events_table_views', 10, 2 );




function tam_events_table_row_actions( $actions, $object, $table )
{
	$actions = array();
	$stripped_link = remove_query_arg( array('s', 'paged') );

	if ( !$table->is_status( 'trash' ) ) {

		// Edit link
		$actions['edit'] = '<a href="'. tam_get_object_edit_link( $object->getObjectId(), $object->getClassName() ) .'">Edit</a>';

		// Duplicate link
		// $actions['copy'] = '<a href="'. tam_get_duplicate_link( $object->getObjectId(), $object->getClassName(), $stripped_link ) .'">Duplicate</a>';

		if ( $table->is_status( 'editor' ) ) {
			
			$actions['toggle_editor'] = '<a href="'. tam_get_change_status_link( $object->getObjectId(), $object->getClassName(), 'publish', $stripped_link ) .'">Set to Publish</a>';
		} else {
			
			// Editor Mode
			$actions['toggle_editor'] = '<a href="'.  tam_get_change_status_link( $object->getObjectId(), $object->getClassName(), 'editor', $stripped_link )  .'">Set to Editor Mode</a>';
		}

		// Trash link
		$actions['trash'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), false, $stripped_link ) .'">Trash</a>';
	} else {

		// Restore link
		$actions['restore'] = '<a href="'. tam_get_object_restore_link( $object->getObjectId(), $object->getClassName(), $stripped_link ) .'">Restore</a>';	
		// Delete link
		$actions['delete'] = '<a href="'. tam_get_object_delete_link( $object->getObjectId(), $object->getClassName(), true, $stripped_link ) .'">Delete Permanently</a>';	
	}

	return $actions;
}

add_filter( 'tam_parse_list_table/events/row_actions', 'tam_events_table_row_actions', 10, 3 );





function tam_events_table_bulk_actions( $actions, $table )
{
	$actions  = array();

	if ( $table->is_status( 'trash' ) ) {
		
		$actions[ 'restore' ] = 'Restore';
		$actions[ 'delete' ] = 'Delete';
	} else {
		
		if ( $table->is_status( 'editor' ) ) {
						
			$actions[ 'change_status' ] = 'Set to Publish';	
		} else {

			$actions[ 'change_status' ] = 'Set to Editor Mode';	
		}

		$actions[ 'delete' ] = 'Trash';
	}

	return $actions;
}

add_filter( 'tam_parse_list_table/events/bulk_actions', 'tam_events_table_bulk_actions', 10, 2 );




function tam_events_table_before_table( $table )
{
	if ( $table->is_status( 'editor' ) ) {

		echo '<input type="hidden" value="publish" name="status">';
	} else if( $table->is_status( 'publish' ) ) {

		echo '<input type="hidden" value="editor" name="status">';
	} else if( $table->is_status( 'trash' ) ) {

		echo '<input type="hidden" value="1" name="force_delete">';
	}
}

add_action( 'tam_parse_list_table/events/before_table', 'tam_events_table_before_table', 10, 1 );