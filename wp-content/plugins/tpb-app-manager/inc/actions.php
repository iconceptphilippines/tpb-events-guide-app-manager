<?php

/**
 * Executes when delete object link is clicked
 * 
 * @param  data   $data The array of request
 * @return void
 */
function tam_listen_delete_object( $data )
{
	$default = array(
		'object_id' 	=> '',
		'object_ids' 	=> '',
		'object_class' 	=> '',
		'force_delete' 	=> '',
		'redirect_url'  => '',
		'status' 	 	=> '',
	);

	// Escape data keys
	$data = wp_parse_args( $data, $default );

	// Get nonce 
	$nonce = ! empty( $data['tam_nonce'] ) ? $data['tam_nonce'] : '';

	$is_single = empty( $data['object_ids'] );

	// Single execution
	if ( $is_single ) {
		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_delete_' . $data['object_id'] . $data['status'] ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] ) ) {
			
			wp_die( 'Restricted' );
		}
	} 
 	// Multiple 
	else {

		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_nonce' ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] ) ) {
			
			wp_die( 'Restricted' );
		}
	}


	$objects = array();

	if ( $is_single ) {

		$object = new Parse\ParseObject( $data['object_class'], $data['object_id'] );
		$objects[] = $object;
	} else {

		foreach( (array) $data['object_ids'] as $v ) {

			$object = new Parse\ParseObject( $data['object_class'], (string) $v );
			$objects[] = $object;
		}
	}

	try {
		

		foreach ( $objects as $o ) {

			// Delete permanently
			if ( $data['force_delete'] ) {

				$o->destroy( TAM_PARSE_MASTER_KEY );
			} 
			// Trash only dont permanently delete
			else {

				$o->fetch();

				// Remember recent status
				$o->set( 'oldStatus', $o->get( 'status' ) );

				// set status to trash
				$o->set( 'status', 'trash' );

				// Save object to parse
				$o->save();
			}	
		}
		
	} catch (Exception $e) {

		wp_die( $e->getMessage() );
	}

	// Redirect after delete
	wp_redirect( $data['redirect_url'] );
	exit;
}

add_action( 'tam/admin_page/action/delete', 'tam_listen_delete_object', 10, 1 );



/**
 * Executes when restore object link is clicked
 * 
 * @param  data   $data The array of request
 * @return void
 */
function tam_listen_restore_object( $data )
{
	$default = array(
		'object_id' 	=> '',
		'object_ids' 	=> '',
		'object_class' 	=> '',
		'redirect_url'  => ''
	);

	// Escape data keys
	$data = wp_parse_args( $data, $default );

	// Get nonce 
	$nonce = ! empty( $data['tam_nonce'] ) ? $data['tam_nonce'] : '';

	$is_single = empty( $data['object_ids'] );

	// Single execution
	if ( $is_single ) {
		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_restore_' . $data['object_id'] ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] ) ) {
			
			wp_die( 'Restricted' );
		}
	} 
 	// Multiple 
	else {

		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_nonce' ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] ) ) {
			
			wp_die( 'Restricted' );
		}
	}


	$objects = array();

	if ( $is_single ) {

		$object = new Parse\ParseObject( $data['object_class'], $data['object_id'] );
		$objects[] = $object;
	} else {

		foreach( (array) $data['object_ids'] as $v ) {

			$object = new Parse\ParseObject( $data['object_class'], (string) $v );
			$objects[] = $object;
		}
	}

	try {


		foreach( $objects as $o ) {

			$o->fetch();

			// GEt old status  of the object
			$oldStatus = $o->get( 'oldStatus' );

			// Set publish as default odls tatus
			if ( empty( $oldStatus ) ) {
				
				$oldStatus = 'publish';
			}

			$o->set( 'status', $oldStatus );

			$o->save();

		}

	} catch (Exception $e) {
		
		wp_die( $e->getMessage() );
	}


	// Redirect
	wp_redirect( $data['redirect_url'] );
	exit;
}

add_action( 'tam/admin_page/action/restore', 'tam_listen_restore_object', 10, 1 );



/**
 * Executes when change status object link is clicked
 * 
 * @param  data   $data The array of request
 * @return void
 */
function tam_listen_change_status_object( $data )
{
	$default = array(
		'object_id' 	=> '',
		'object_class' 	=> '',
		'redirect_url'  => '',
		'status'		=> '',
		'object_ids'	=> array()
	);

	// Escape data keys
	$data = wp_parse_args( $data, $default );

	// Get nonce 
	$nonce = ! empty( $data['tam_nonce'] ) ? $data['tam_nonce'] : '';

	$is_single = empty( $data['object_ids'] );

	// Single execution
	if ( $is_single ) {
		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_status_' . $data['object_id'] . $data['status'] ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] )
			|| empty( $data['status'] ) ) {
			
			wp_die( 'Restricted' );
		}
	} 
 	// Multiple 
	else {

		// Don't execute if nonce is not valid
		// or there is no object class
		if ( !wp_verify_nonce( $nonce, 'tam_nonce' ) 
			|| empty( $data['object_class'] )
			|| empty( $data['redirect_url'] )
			|| empty( $data['status'] ) ) {
			
			wp_die( 'Restricted' );
		}
	}

	$objects = array();

	if ( $is_single ) {

		$object = new Parse\ParseObject( $data['object_class'], $data['object_id'] );
		$object->set( 'status', $data['status'] );
		$objects[] = $object;
	} else {

		foreach( (array) $data['object_ids'] as $v ) {

			$object = new Parse\ParseObject( $data['object_class'], (string) $v );
			$object->set( 'status', $data['status'] );
			$objects[] = $object;
		}
	}


	// Save objects
	try {

		foreach( $objects as $o )
			$o->save();

	} catch (Exception $e) {
		
		wp_die( $e->getMessage() );
	}

	// Redirect
	wp_redirect( $data['redirect_url'] );
	exit;
}

add_action( 'tam/admin_page/action/change_status', 'tam_listen_change_status_object', 10, 1 );



/**
 * Executes when change status object link is clicked
 * 
 * @param  data   $data The array of request
 * @return void
 */
function tam_listen_duplicate_object( $data )
{
	$default = array(
		'object_id' 	=> '',
		'object_class' 	=> '',
		'redirect_url'  => '',
	);

	// Escape data keys
	$data = wp_parse_args( $data, $default );

	// Get nonce 
	$nonce = ! empty( $data['tam_nonce'] ) ? $data['tam_nonce'] : '';

	// Don't execute if nonce is not valid
	// or there is no object class
	if ( !wp_verify_nonce( $nonce, 'tam_duplicate_' . $data['object_id'] ) 
		|| empty( $data['object_class'] )
		|| empty( $data['redirect_url'] ) ) {
		
		wp_die( 'Restricted' );
	}

	Tam_Parse_Object::$parseClassName = $data['object_class'];
	Tam_Parse_Object::registerSubclass();

	$object = new Tam_Parse_Object( $data['object_class'], $data['object_id'] );
	$object->fetch();

	$copy = $object->copy();
	$copy->set( 'title', 'Copy of ' . $copy->get( 'title' ) );
	
	try {
		
		$copy->save();
	} catch (Exception $e) {
		
		wp_die( $e->getMessage() );
	}

	// Redirect
	wp_redirect( $data['redirect_url'] );
	exit;
}

add_action( 'tam/admin_page/action/duplicate', 'tam_listen_duplicate_object', 10, 1 );



function tam_listen_update_attendee_status( $data ) 
{
	$default = array(
		'object_id' 	=> '',
		'object_ids' 	=> array(),
		'approved'		=> null,
		'redirect_url'	=> '',
	);

	// Escape data keys
	$data = wp_parse_args( $data, $default );

	// Get nonce 
	$nonce = ! empty( $data['tam_nonce'] ) ? $data['tam_nonce'] : '';

	$is_single = empty( $data['object_ids'] );

	// Don't execute if nonce is not valid
	// or there is no object class
	if ( !wp_verify_nonce( $nonce, 'tam_nonce' )
		|| empty( $data['redirect_url'] ) ) {
		
		wp_die( 'Restricted' );
	}

	if ( $data['action'] == 'unapprove_attendee' ) {
		$data['approved'] = 0;
	} else {
		$data['approved'] = 1;
	}

	$objects = array();

	if ( $is_single ) {

		$object = new Parse\ParseObject( 'EventAttendee', $data['object_id'] );
		$object->set( 'isApproved', (bool)$data['approved'] );
		$objects[] = $object;
	} else {

		foreach( (array) $data['object_ids'] as $v ) {

			$object = new Parse\ParseObject( 'EventAttendee', (string) $v );
			$object->set( 'isApproved', (bool)$data['approved'] );
			$objects[] = $object;
		}
	}

	// Save objects
	try {

		foreach( $objects as $o )
			$o->save();

	} catch (Exception $e) {
		
		wp_die( $e->getMessage() );
	}

	// Redirect
	wp_redirect( $data['redirect_url'] );
	exit;
}

add_action( 'tam/admin_page/action/unapprove_attendee', 'tam_listen_update_attendee_status', 10, 1 );
add_action( 'tam/admin_page/action/approve_attendee', 'tam_listen_update_attendee_status', 10, 1 );

