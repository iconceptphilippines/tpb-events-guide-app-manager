<?php

function tam_parse_list_table_ajax() {
 
 	$screen = ! empty( $_REQUEST['screen'] ) ? $_REQUEST['screen'] : '';

 	if ( empty( $screen ) ) {
 		
 		die();
 	}

 	$table = tam_get_parse_table( $screen );

 	if ( !is_wp_error( $table ) ) {

	    $table->ajax_response();
 	}

 	die();
}

add_action('wp_ajax_tam/parse_list_table', 'tam_parse_list_table_ajax');



function tam_ajax_get_modal()
{
	check_ajax_referer( 'tam_nonce', 'nonce' );

	$modal_name 	= !empty( $_REQUEST['modal_name'] ) ? $_REQUEST['modal_name'] : '';
	$modal_data     = apply_filters( 'tam/modal_data/name=' . $modal_name, $_REQUEST );

	ob_start();
	tam_view( 'modal-' . $modal_name, $modal_data );
	$modal_content = ob_get_clean();

	$data = array(
		'html' 	=> $modal_content,
		'name'	=> $modal_name
	);

	wp_send_json_success( $data );
}

add_action( 'wp_ajax_tam/get_modal', 'tam_ajax_get_modal' );


function tam_ajax_upload()
{
	check_ajax_referer( 'tam_nonce', 'nonce' );

	$name = !empty( $_REQUEST['tam_upload_name'] ) ? $_REQUEST['tam_upload_name'] : '';

	$uploaded_file = wp_handle_upload( $_FILES['file'], array( 'test_form' => false ) );

	do_action( 'tam/ajax_upload', 				$uploaded_file, $_REQUEST );
	do_action( 'tam/ajax_upload/name=' . $name, $uploaded_file, $_REQUEST );

	die();
}

add_action( 'wp_ajax_tam/upload', 'tam_ajax_upload' );



function tam_ajax_save_object()
{
	check_ajax_referer( 'tam_nonce', 'nonce' );

	$object_id 		= !empty( $_REQUEST['object_id'] ) ? $_REQUEST['object_id'] : null;
	$object_class 	= !empty( $_REQUEST['object_class'] ) ? $_REQUEST['object_class'] : null;
	$object 		= !empty( $_REQUEST['object'] ) ? $_REQUEST['object'] : null;
	$referrer 		= !empty( $_REQUEST['tam_referrer'] ) ? urldecode($_REQUEST['tam_referrer']) : null;

	// Don't continue if there is no object and class name 
	// passed
	if ( is_null( $object_class ) || is_null( $object ) ) {
		
		wp_send_json_error( array(
			'message' => 'Invalid fields'
		));
	}

	$_object_id = 0;

	try {

		// Save the object
		$_object_id = tam_save_object( $object_class, $object, $object_id );
	} catch (Exception $e) {
			
		// Return the error
		wp_send_json_error( array(
 			'messsage' => $e->getMessage()
		));
	}

	// Send success notification
	wp_send_json_success( array(
		'object_id' 	=> $_object_id,
		'object_url' 	=> tam_get_event_object_link( $object_class, $_object_id, $referrer )
	));
}

add_action( 'wp_ajax_tam/save_object', 'tam_ajax_save_object' );



function tam_ajax_bulk_save_object()
{
	check_ajax_referer( 'tam_nonce', 'nonce' );

	$objects = !empty( $_REQUEST['objects'] ) ? $_REQUEST['objects'] : null;

	if ( is_null( $objects ) ) {
		
		wp_send_json_error( array(
			'message' => 'Objects must not be empty.'
		));
	}

	$defaults = array(
		'class' => '',
		'id' 	=> '',
		'data'	=> array()
	);

	$errors  	= array();
	$ids 		= array();
	
	foreach( (array) $objects as $object ) {

		$object = wp_parse_args( $object, $defaults );

		if ( empty( $object['data'] ) ) {
			continue;
		}

		if ( empty( $object['class'] ) ) {
			$errors[] = 'Please define class name';
		}

		$object_id 			= !empty( $object['id'] ) ? $object['id'] : null;
		$saved_object_id 	= 0;
		try {
			
			$saved_object_id = tam_save_object( $object['class'], $object['data'], $object_id );
		} catch (Exception $e) {
			
			$errors[] = $e->getMessage();
		}

		$ids[] = $saved_object_id;
	}

	if ( !empty( $errors ) ) {
		
		wp_send_json_error( array(
			'messages' => $errors
		) );
	} else {

		wp_send_json_success( array(
			'object_ids' => $ids
		) );
	}
}

add_action( 'wp_ajax_tam/bulk_save_objects', 'tam_ajax_bulk_save_object' );

	

function tam_ajax_save_menu()
{
	check_ajax_referer( 'tam_nonce', 'nonce' );

	$defaults = array(
		'type' 		=> '',
		'menu_id' 	=> null,
	);

	$args    = wp_parse_args( $_POST, $defaults );
	$menu_id = !empty( $args['menu_id'] ) ? $args['menu_id'] : null;

	// Check if menu type exists
	if ( empty( $args['type'] ) ) {
		
		wp_send_json_error( array(
			'message' => 'Invalid menu type.',
		));
	}

	// Check if event id is passed
	if ( empty( $args['menu'] ) ) {
		
		wp_send_json_error( array(
			'message' => 'Invalid parameters.'
		));
	}

	$menu 		= $args['menu'];
	$menu_type 	= tam_get_menu_type( $args['type'] );
	$save_data  = array(
		'menuType' => $args['type']
	);


	foreach( (array) $menu_type['fields'] as $parse_column => $field ) {

		$name 	= $field['name'];
		$value  = isset( $menu[ $name ] ) ? $menu[ $name ] : null;
		$parse  = array();
		
		if ( !isset( $field['parse'] ) ) {
			
			$save_data[ $parse_column ] = $value;
		} else {

			$parse 		= $field['parse'];
			$parse_type = $parse['type'];

			if ( $parse_type == 'boolean' ) {
				
				if ( $value == 'on' ) {
					
					$parse['value'] = true;
				} else {
					$parse['value'] = false;
				}
			} else if( $parse_type == 'pointer' ) {

				$parse['object_id'] = $value;
			} else if( $parse_type == 'relation' ) {

				$parse['objects'] = $value;
			} else if( $parse_type == 'date' ) {

				$parse['date'] = $value;
			} else if ( $parse_type == 'number' ) {
				$parse['value'] = $value;
			}

			$save_data[ $parse_column ] = $parse;
		}

		$save_data[ $parse_column ] = apply_filters( 'tam/save_menu_field', $save_data[ $parse_column ], $parse_column, $menu_type );
	}
		
	try {
		// Save menu
		$id = tam_save_object( 'Menu', $save_data, $menu_id );
		
		// Return the id of the menu
		wp_send_json_success( array(
			'menu_id' => $id
		));
	} catch (Exception $e) {
		// Return error
		wp_send_json_error( array(
			'message' => $e->getMessage()
		));
	}
}

add_action( 'wp_ajax_tam/save_menu', 'tam_ajax_save_menu' );



function tam_ajax_user_as_editor()
{
	check_ajax_referer('tam_nonce', 'nonce');

	$is_editor = isset( $_POST['is_editor'] ) ? $_POST['is_editor'] : null;

	if (is_null( $is_editor ) || !isset( $_POST['user_id'] )) {
		wp_send_json_error();
	}

	try {
		
		if ($is_editor == "true") {
			tam_update_user_role( $_POST['user_id'], 'Editor' );
		} else {
			tam_remove_user_role( $_POST['user_id'], 'Editor' );
		}	

		wp_send_json_success();
	} catch (Exception $e) {
		wp_send_json_error(array(
			'message' => $e->getMessage()
		));
	}
}

add_action( 'wp_ajax_tam/user_as_editor', 'tam_ajax_user_as_editor' );





