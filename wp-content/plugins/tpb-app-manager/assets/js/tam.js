var TpbAppManager;

(function($) {

	'use strict';

	$.fn.serializeObject = function(options) {

	    options = jQuery.extend({}, options);

	    var self = this,
	        json = {},
	        push_counters = {},
	        patterns = {
	            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
	            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
	            "push":     /^$/,
	            "fixed":    /^\d+$/,
	            "named":    /^[a-zA-Z0-9_]+$/
	        };


	    this.build = function(base, key, value){
	        base[key] = value;
	        return base;
	    };

	    this.push_counter = function(key){
	        if(push_counters[key] === undefined){
	            push_counters[key] = 0;
	        }
	        return push_counters[key]++;
	    };

	    jQuery.each(jQuery(this).serializeArray(), function(){

	        // skip invalid keys
	        if(!patterns.validate.test(this.name)){
	            return;
	        }

	        var k,
	            keys = this.name.match(patterns.key),
	            merge = this.value,
	            reverse_key = this.name;

	        while((k = keys.pop()) !== undefined){

	            // adjust reverse_key
	            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

	            // push
	            if(k.match(patterns.push)){
	                merge = self.build([], self.push_counter(reverse_key), merge);
	            }

	            // fixed
	            else if(k.match(patterns.fixed)){
	                merge = self.build([], k, merge);
	            }

	            // named
	            else if(k.match(patterns.named)){
	                merge = self.build({}, k, merge);
	            }
	        }

	        json = jQuery.extend(true, json, merge);
	    });


	    return json;
	}

	TpbAppManager = {

		_request: function( action, data, callback )
		{
			var _data 		= {};
			_data.action 	= action;
			_data.nonce     = _TAM.o.nonce;
			_data.tam_referrer = encodeURIComponent(window.location.href);
			data 			= $.extend(true, _data, data);
			
			$.ajax({
				url 	: _TAM.o.ajaxUrl,
				type 	: 'POST',
				dataType: 'JSON',
				data 	: data
			})
			.done(function( response ) {
				callback(response);
			})
			.fail(function( response ) {
				callback(response);
			});
		},

		getQueryVar: function ( field, url ) 
		{
		    var href = url ? url : window.location.href;
		    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
		    var string = reg.exec(href);
		    return string ? string[1] : null;
		},

		saveObject: function( objectClass, objectData, objectId, callback )
		{
			this._request( 'tam/save_object', {
				object_id 	 : objectId,
				object_class : objectClass,
				object 		 : objectData
			}, callback);
		},

		showModal: function(modalName, data, callback, title, size) 
		{
			var $dialog = $('<div>', {id: 'dialog'});

			data 			= !data ? {} : data;
			data.modal_name = modalName;

			$('body').append($dialog);

			this._request( 
				// Action
				'tam/get_modal', 

				// data
				data, 

				// Callback
				function( response ) 
				{
					if ( response.success ) {

						// Set modal properties
						$dialog.html( response.data.html );

						var dialog, height = 300, width = 350;

						size = !size ? {} : size;

						if ( size.height )
							height = size.height;

						if ( size.width )
							width = size.width;

						// Set modal params
						var dialogParams = {
							autoOpen	: false,
							height 		: height,
							width 		: width,
							modal 		: true,
							buttons 	: {},
							dialogClass : 'tam-dialog',
							open 		: function(event, ui) {

								TpbAppManager.doAction( 'tam/dialog/open/name=' + modalName, dialog, $dialog );
							},
							close 		: function(event, ui) {

								dialog.dialog( 'destroy' ).remove();
								TpbAppManager.doAction( 'tam/dialog/close/name=' + modalName, dialog, $dialog );
							}
						};

						// Initialize dialogs
						dialog = $dialog.dialog(dialogParams);
						dialog.dialog( 'option', 'title', title );

						// Open dialog
						dialog.dialog('open');

						callback( dialog );
					}
				}
			);
		},

		createNewGalleryModal: function()
		{	
			var _instance = this;
			_instance.showModal( 'new-gallery', {}, function( dialog ) {
				dialog.dialog( 'option', 'buttons', {
					'Create new gallery' : function(e, i) {

						var $form = $(dialog).find('form');
						
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						// Saving object api
						_instance.saveObject(

							// Class name
							'Gallery',

							// Object data
							{
								title: $form.find('[name="title"]').val(),
								event: {
									type 	 : 'pointer',
									class 	 : 'Event',
									object_id: _instance.getQueryVar('object_id')
								}
							},

							// Object id
							null,

							// Callback
							function( response )
							{
								if ( response.success ) {

									$btnText.text('Redirecting...');

									window.location = response.data.object_url;
								} else {

									// Reset form
									$form.find('input').val('');

									// Enable button
									$btn.attr('disabled', false);
									$btn.prop('disabled', false);
									$btnText.text(tempText);

									alert(response.data.message);
								}
							}
						);
					}
				});
				},
				'New Gallery',
				{
					height: 180
				}				
			);
		},

		showUploader: function( options, callback )
		{
			options = !options ? {} : options;

			var defaults = {
				title : 'Uploader',
				modal : 'upload',
				name  : 'global',
				data  : {},
				fileTypes : 'image/*,application/pdf,.psd',
				maxFiles : null
			};

			options = $.extend(true, defaults, options);
	
			options.data.action 			= 'tam/upload';
			options.data.nonce 				= _TAM.o.nonce;	
			options.data.tam_upload_name 	= options.name;

			var url = _TAM.o.ajaxUrl + '?';
			var params = [];

			$.each(options.data, function(index, val) {
				params.push(index + '=' + val);
			});

			url += params.join('&');

			this.showModal( options.modal, {}, function( dialog ) {

				// Initialize dropzone
				var dropzone = new Dropzone(dialog.find('div#tam-uploader')[0], { 
					url 				: url,
					autoProcessQueue  	: false,
					parallelUploads 	: 100,
					acceptedFiles 		: options.fileTypes,
					maxFiles 			: options.maxFiles
				});

				// Upload button
				dialog.dialog( 'option', 'buttons', {

					'Upload' : function(e)
					{
						dropzone.processQueue();

						// Make button uploading
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Uploading...');
					}
				});

				// Complete callback
				dropzone.on( 'success', function( file, response ) {

					callback( dropzone, dialog, file, response );
				});

				// Destroy drop zone
				dialog.on( 'dialogclose', function() {
					
					dropzone.destroy();
				});

			}, options.title, {
				height: 500,
				width : 600
			});
		},

		editObjectTextColumn: function( modalTitle, oldValue, objectId, objectClass, column, fieldLabel, callback )
		{
			var _instance = this;
			this.showModal( 
				
				// Modal to load
				'edit-object-column', 
				
				// Data of the modal
				{
					old_value 		: oldValue,
					modal_title 	: modalTitle,
					object_column 	: column,
					field_label 	: fieldLabel
				},

				// callback
				function( dialog ) 
				{
					// Get the dialog form
					var $form = dialog.find('form');

					dialog.dialog('option', 'title', modalTitle);

					// Add save buttobn
					dialog.dialog( 'option', 'buttons', {
						'Save' : function(e) {

							var newValue = $form.find('[name="' + column + '"]').val();
							
							// Don't do anything when 
							// nothing change just close the modal
							if ( newValue == oldValue ) {

								dialog.dialog('close');
								callback( oldValue );
							}

							// Make button saving
							var $btnText 	= $(e.target),
								$btn 		= $btnText.parent(),
								tempText 	= $btnText.text();

							// Disable button
							$btn.attr('disabled', true);
							$btn.prop('disabled', true);
							$btnText.text('Saving...');

							var objectData = {};

							objectData[ column ] = newValue;

							// Saving object api
							_instance.saveObject(

								// Class name
								objectClass,

								// Object data
								objectData,

								// Object id
								objectId,

								// Callback
								function( response )
								{
									if ( response.success ) {

										dialog.dialog('close');
										callback( newValue );
									} else {

										// Enable button
										$btn.attr('disabled', false);
										$btn.prop('disabled', false);
										$btnText.text(tempText);

										alert(response.data.message);
										callback( oldValue );
									}
								}
							);
						}
					});
				},

				modalTitle,
				{
					height: 180
				}
			);
		},

		bulkSaveObjects: function(objects, callback)
		{
			this._request( 'tam/bulk_save_objects', {
				objects: objects
			}, callback);
		},

		googleMapPicker: function( options )
		{
			var defaults = {
				elements : {
					map 		: null,
					location 	: null,
					latitude 	: null,
					longitude 	: null
				},
				radius 	 : 0,
				zoom 	 : 14,
				enableAutocomplete: true,
			};

			var defaultLong = $('#default-long').val(),
				defaultLat 	= $('#default-lat').val();

			if( defaultLat === undefined ) {
				defaultLat = 14.5995124;
			}

			if( defaultLong === undefined ) {
				defaultLong = 120.9842195;
			}

			$parent.find('#location-map').locationpicker({
				location 	: {
					latitude : defaultLat,
					longitude: defaultLong
				},
				zoom 		: 14,
				radius 		: 0,
				inputBinding: {
					latitudeInput 		: $parent.find('#map-lat'),
			        longitudeInput 		: $parent.find('#map-long'),
			        locationNameInput 	: $parent.find('#event-location'),
			    },
			    enableAutocomplete: true,
				onchanged: function(currentLocation, radius, isMarkerDropped) {

					var mapContext = $(this).locationpicker('map');
					mapContext.map.setZoom(14);
				}
			});
		},

		sortables: function()
		{
			// Initialize sortable
			var $dropdown = $( "[data-drop-selection-panel]" );

			$dropdown.sortable({
		    	connectWith: "[data-drop-selection-panel]",
		    	receive: function(event, ui) {
		    		var sortables = [ui.sender, ui.sender.siblings()];

		    		for (var i = sortables.length - 1; i >= 0; i--) {
						var item     = sortables[i];
						var disabled = item.hasClass('selection');

		    			item.find(':input').attr('disabled', disabled);
		      			item.find(':input').prop('disabled', disabled);
		    		};
		      	}
		    }).disableSelection();
		},

		saveMenu: function( args, callback )
		{
			this._request( 'tam/save_menu', args, callback );
		}
	};

	// Add hooks
	TpbAppManager = $.extend(true, wp.hooks, TpbAppManager);

})(jQuery);





