(function($) {

	'use strict';

	var api = TpbAppManager;

	var Admin = {

		init: function()
		{
			this._registerEventListeners();
			this._sortableTable();
		},

		_editGalleryNameInList: function(e)
		{
			e.preventDefault();

			var $tr 		= $(this).parentsUntil('tr').parent(),
				$title 		= $tr.find('td.column-title a strong'),
				$btn 		= $(this),
				title 		= $btn.data('title'),
				objectId 	= $btn.data('object-id');

			api.editObjectTextColumn( 'Edit Gallery Name',
				title,
				objectId,
				'Gallery',
				'title',
				'Title',
				function( newValue ) {

					$title.text( newValue );
					$btn.data('title', newValue);
					$btn.prop('data-title', newValue);
					$btn.attr('data-title', newValue);
				}
			);
		},

		_editGalleryNameInSingle: function(e)
		{
			e.preventDefault();

			var $name 		= $('#gallery-name'),
				$btn 		= $(this),
				title 		= $btn.data('title'),
				objectId 	= $btn.data('object-id');

			api.editObjectTextColumn( 'Edit Gallery Name',
				title,
				objectId,
				'Gallery',
				'title',
				'Title',
				function( newValue ) {

					$name.text( newValue );
					$btn.data('title', newValue);
					$btn.prop('data-title', newValue);
					$btn.attr('data-title', newValue);
				}
			);
		},

		_editPhotoNameInList: function(e)
		{
			e.preventDefault();

			var $tr 		= $(this).parentsUntil('tr').parent(),
				$title 		= $tr.find('td.column-name strong'),
				$btn 		= $(this),
				name 		= $btn.data('name'),
				objectId 	= $btn.data('object-id');

			api.editObjectTextColumn( 'Edit Photo Name',
				name,
				objectId,
				'Photo',
				'name',
				'Name',
				function( newValue ) {

					$title.text( newValue );
					$btn.data('name', newValue);
					$btn.prop('data-name', newValue);
					$btn.attr('data-name', newValue);
				}
			);
		},

		_createNewGallery: function(e)
		{
			e.preventDefault();

			api.createNewGalleryModal();
		},

		_uploadGalleryPhotos: function(e)
		{
			e.preventDefault();

			api.showUploader({
				name: 'photo',
				data: {
					gallery_id: api.getQueryVar('gallery_id')
				},
				fileTypes: 'image/*'
			}, function() {
				window.location = window.location;
			});
		},

		_newEventModal: function(e)
		{
			api.showModal( 'new-event', {}, function() {}, 'Create Event', { height: 500, width: 600 } );
		},

		_newTopicModal: function(e)
		{
			api.showModal( 'new-topic', {}, function( dialog ) {

				var $form = dialog.find('form');

				dialog.dialog( 'option', 'buttons', {
					'Save Topic' : function(e) {

						// Make button uploading
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						api.saveObject(

							// Class name
							'Topic',

							// Object data
							{
								title: $form.find('#tam-title').val(),
								event: {
									type 	 : 'pointer',
									class 	 : 'Event',
									object_id: api.getQueryVar('object_id')
								}
							},

							// Object id
							null,

							// Callback
							function( response )
							{
								if ( response.success ) {

									$btnText.text('Redirecting...');

									window.location = response.data.object_url;
								} else {

									// Reset form
									$form.find('input').val('');

									// Enable button
									$btn.attr('disabled', false);
									$btn.prop('disabled', false);
									$btnText.text(tempText);

									alert(response.data.message);
								}
							}

						);
					}
				});
			}, 'Create Topic', { height: 180 } );
		},

		_editTopicName: function(e)
		{
			e.preventDefault();

			var $name 		= $('#topic-name'),
				$btn 		= $(this),
				title 		= $btn.data('title'),
				objectId 	= $btn.data('object-id');

			api.editObjectTextColumn( 'Edit Topic Name',
				title,
				objectId,
				'Topic',
				'title',
				'Title',
				function( newValue ) {

					$name.text( newValue );
					$btn.data('title', newValue);
					$btn.prop('data-title', newValue);
					$btn.attr('data-title', newValue);
				}
			);
		},

		_editTopicNameInList: function(e)
		{
			e.preventDefault();

			var $name 		= $(this).parentsUntil('td').parent().find('a strong'),
				$btn 		= $(this),
				title 		= $btn.data('title'),
				objectId 	= $btn.data('object-id');

			api.editObjectTextColumn( 'Edit Topic Name',
				title,
				objectId,
				'Topic',
				'title',
				'Title',
				function( newValue ) {

					$name.text( newValue );
					$btn.data('title', newValue);
					$btn.prop('data-title', newValue);
					$btn.attr('data-title', newValue);
				}
			);
		},


		_newPage: function(e)
		{
			api.showModal( 'new-page', {}, function(dialog) {

				dialog.dialog( 'option', 'buttons', {
					'Save Page' : function(e) {

						var $form 	 = dialog.find('form'),
							$btnText = $(e.target),
							$btn	 = $btnText.parent(),
							oldText  = $btnText.text(),
							title 	 = $form.find('[data-field="title"]').val(),
							subtitle = $form.find('[data-field="subtitle"]').val(),
							content  = $form.find("#tam-content").val();//tinyMCE.activeEditor.getContent();
							
						// Change button to saving
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						api.saveObject( 'Page', {
							title  	 : title,
							subtitle : subtitle,
							content  : content,
							event 	 : {
								type  	  : 'pointer',
								object_id : api.getQueryVar( 'object_id' ),
								class     : 'Event'
							}
						}, null, function( response ) {

							if ( response.success ) {

								$btnText.text('Redirecting...');

								window.location = response.data.object_url;
							} else {

								// Enable button
								$btn.attr('disabled', false);
								$btn.prop('disabled', false);
								$btnText.text(oldText);

								alert(response.data.message);
							}
						});
					}
				});

			}, 'New Page', { height: 500, width: 600 } );
		},


		_pageThumbnailPicker: function(e)
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			api.showUploader({
				name: 'page_thumbnail',
				data: {
					page_id: api.getQueryVar('page_id')
				},
				fileTypes: 'image/*',
				maxFiles: 1
			}, function( dropzone, dialog, file, response ) {

				if ( response.success ) {
					console.log( response );
					$image.css( 'background-image', 'url(' + response.data.url + ')' );
					$image.attr( 'title', response.data.name );
					$image.prop( 'title', response.data.name );
					$parent.addClass('tam-has');
				} else {

					alert( response.data.message );
				}

				dropzone.disable();
				dialog.dialog('close');
			});
		},

		_pageRemoveThumbnail: function( e )
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			// Ask user first for confirmation
			if ( !confirm( 'Are you sure you wan\'t to remove the thumbnail?' ) )
				return;

			api.saveObject( 'Page', {
				image: {
					'delete' : 1
				}
			}, api.getQueryVar( 'page_id' ), function( response ) {

				if ( response.success ) {

					$image.css( 'background-image', '' );
					$parent.removeClass('tam-has');
				} else {

					alert( response.data.message );
				}
			});
		},

		_scheduleThumbnailPicker: function(e)
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			api.showUploader({
				name: 'schedule_thumbnail',
				data: {
					schedule_slot_id: api.getQueryVar('schedule_slot_id')
				},
				fileTypes: 'image/*',
				maxFiles: 1
			}, function( dropzone, dialog, file, response ) {

				if ( response.success ) {
					console.log( response );
					$image.css( 'background-image', 'url(' + response.data.url + ')' );
					$image.attr( 'title', response.data.name );
					$image.prop( 'title', response.data.name );
					$parent.addClass('tam-has');
				} else {

					alert( response.data.message );
				}

				dropzone.disable();
				dialog.dialog('close');
			});
		},

		_scheduleRemoveThumbnail: function( e )
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			// Ask user first for confirmation
			if ( !confirm( 'Are you sure you wan\'t to remove the thumbnail?' ) )
				return;

			api.saveObject( 'ScheduleSlot', {
				image: {
					'delete' : 1
				}
			}, api.getQueryVar( 'schedule_slot_id' ), function( response ) {

				if ( response.success ) {

					$image.css( 'background-image', '' );
					$parent.removeClass('tam-has');
				} else {

					alert( response.data.message );
				}
			});
		},

		_avatarPicker: function(e)
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			api.showUploader({
				name: 'avatar_thumbnail',
				data: {
					object_id: api.getQueryVar('object_id')
				},
				fileTypes: 'image/*',
				maxFiles: 1
			}, function( dropzone, dialog, file, response ) {

				if ( response.success ) {
					console.log( response );
					$image.css( 'background-image', 'url(' + response.data.url + ')' );
					$image.attr( 'title', response.data.name );
					$image.prop( 'title', response.data.name );
					$parent.addClass('tam-has');
				} else {

					alert( response.data.message );
				}

				dropzone.disable();
				dialog.dialog('close');
			});
		},

		_RemoveAvatar: function( e )
		{
			e.preventDefault();

			var $link 	= $(this),
				$parent = $link.parent(),
				$image  = $parent.find('[data-field="image"]');

			// Ask user first for confirmation
			if ( !confirm( 'Are you sure you wan\'t to remove the avatar?' ) )
				return;

			api.saveObject( '_User', {
				avatar: {
					'delete' : 1
				}
			}, api.getQueryVar( 'object_id' ), function( response ) {
				console.log( response );
				if ( response.success ) {

					$image.css( 'background-image', '' );
					$parent.removeClass('tam-has');
				} else {

					// alert( response.data.message );
				}
			});
		},

		_confirmDelete: function(e)
		{
			if ( !confirm( 'Are you sure you wan\'t to delete this?' ) ) {

				e.preventDefault();
			}
		},

		_newAnnouncementModal: function(e)
		{
			api.showModal( 'new-announcement', {}, function( dialog ) {

				var $form = dialog.find('form');

				dialog.dialog( 'option', 'buttons', {
					'Save Announcement' : function(e) {

						// Make button uploading
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// // Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						api.saveObject(

							// Class name
							'Announcement',

							// Object data
							{
								title: $form.find('#tam-title').val(),
								content: $form.find('#tam-message').val(),
								event: {
									type 	 : 'pointer',
									class 	 : 'Event',
									object_id: api.getQueryVar('object_id')
								}
							},

							// Object id
							null,

							// Callback
							function( response )
							{
								console.log(response);
								if ( response.success ) {

									$btnText.text('Redirecting...');

									window.location = response.data.object_url;
								} else {

									// Reset form
									$form.find('input').val('');

									// Enable button
									$btn.attr('disabled', false);
									$btn.prop('disabled', false);
									$btnText.text(tempText);

									alert(response.data.message);
								}
							}

						);
					}
				});
			}, 'Create Announcement', { height: 450 } );
		},

		_sortableTable: function() 
		{
			var $sortableTable 	= $('.parse-list-table-sortable tbody');
			var orderChanged 	= false;

			$sortableTable.sortable({
		    	placeholder : 'ui-state-highlight',
		    	handle 		: '.tam-hamburger',
		    	start: function(event, ui) {
			        ui.item.data('start_pos', ui.item.index());
			    },
		    	helper      : function(e, tr) {
					var $originals 	= tr.children();
					var $helper 	= tr.clone();

					$helper.children().each(function(index) {
						// Set helper cell sizes to match the original sizes
						$(this).width($originals.eq(index).width());
					});

					return $helper;
				}
		    });

			// Disable selection
			$sortableTable.disableSelection();

		    // Triggers when the sorting has stopped
		    $sortableTable.on( 'sortstop', function(e, ui) {
		    	var start_pos = ui.item.data('start_pos');

		    	orderChanged = (start_pos != ui.item.index());

		    	var $table 	= $(e.target),
			    	$rows 	= $table.find('>tr .tam-hamburger'),
			    	objects = [];

			    $.each($rows, function(index, el) {

			    	// Add object to be saved
			    	objects[index] = {
			    		class 	: el.dataset.parseClass,
			    		id 		: el.dataset.id,
			    		data 	: {
			    			order: {
			    				type 	: 'number',
			    				value 	: index
			    			}
			    		}
			    	};
			    });


			    if (orderChanged) {
			    	// Don't allow drag when it is doing the saving
				    $sortableTable.sortable( 'disable' );

				    // Save the objects
				    api.bulkSaveObjects(objects, function(response) {

				    	// Enable the sorting again after save
				    	$sortableTable.sortable( 'enable' );
				    });
			    }
		    });
		},

		_enableMenu: function(e)
		{
			if ( 'checkbox' != this.type ) {
				return;
			}

			var enabled = this.checked,
				id 		= this.dataset.id;

			api.saveObject( 'Menu', {
				enabled: {
					type: 'boolean',
					value: enabled
				}
			}, id, function( response ) {

				console.log( response );
			});
		},

		_editorModeMenu: function(e)
		{
			if ( 'checkbox' != this.type ) {
				return;
			}

			var enabled = this.checked,
				id 		= this.dataset.id,
				status;

			if ( enabled ) {
				status = 'editor';
			} else {
				status = 'publish';
			}

			api.saveObject( 'Menu', {
				status: status
			}, id, function( response ) {

				console.log( response );
			});
		},

		_newFileByUrlModal: function(e)
		{
			api.showModal( 'new-file', {}, function( dialog ) {

				var $btnText,
					$form = dialog.find('form');

				$('#tam-form-new-file').on( 'submit', function(e){

					e.preventDefault();

					var $btn 		= $btnText.parent(),
						tempText 	= $btnText.text();

					// // Disable button
					$btn.attr('disabled', true);
					$btn.prop('disabled', true);
					$btnText.text('Saving...');

					api.saveObject(

						// Class name
						'File',

						// Object data
						{
							fileName: $form.find('#tam-file-name').val(),
							fileSize:  {
								type 	 : 'number',
								value 	 : parseInt($form.find('#tam-file-size').val(), 10)
							},
							fileType: $form.find('#tam-file-type').val(),
							fileURL: $form.find('#tam-file-url').val(),
							fileSource: 'url',
							event: {
								type 	 : 'pointer',
								class 	 : 'Event',
								object_id: api.getQueryVar('object_id')
							}
						},

						// Object id
						null,

						// Callback
						function( response )
						{
							if ( response.success ) {

								$btnText.text('Redirecting...');

								window.location = response.data.object_url;
							} else {

								// Reset form
								$form.find('input').val('');

								// Enable button
								$btn.attr('disabled', false);
								$btn.prop('disabled', false);
								$btnText.text(tempText);

								alert(response.data.message);
							}
						}

					);
				});

				dialog.dialog( 'option', 'buttons', {
					'Save File' : function(e) {

						$btnText = $(e.target);
						$form.find( 'input[type="submit"]' ).trigger('click');
					}
				});
			}, 'Upload File by URL', { height: 400 } );
		},


		_uploadFiles: function()
		{
			api.showUploader({
				name: 'file',
				data: {
					event_id: api.getQueryVar('object_id')
				}
			}, function(response) {
				window.location = window.location;
			});
		},

		_dropdown: function(e)
		{
			e.preventDefault();

			var $dropdown = $(this).parent();
			$dropdown.toggleClass('tam-dropdown-open');
		},

		_newMenu: function(e)
		{
			e.preventDefault();
			var eventId = api.getQueryVar( 'object_id' ),
				type 	= this.dataset.type;

			api.showModal( 'menu-form', { type: type, event_id: eventId }, function(dialog) {

				api.sortables();

				var $form = dialog.find( 'form' ),
					$btn = null;

				$form.submit(function(e) {
					e.preventDefault();

					var $btnText 	= $btn.parent(),
						tempText 	= $btn.text();

					// // Disable button
					$btnText.attr('disabled', true);
					$btnText.prop('disabled', true);
					$btn.text('Saving...');

					// Save menu
					api.saveMenu( {
						menu_id  : null,
						menu 	 : $form.serializeObject(),
						type 	 : type
					}, function(response) {

						if ( response.success ) {

							// Add menu to event
							api.saveObject( 'Event', {
								menus: {
									type: 'relation',
									class: 'Menu',
									objects: [response.data.menu_id]
								}
							}, eventId, function(saveEventResponse) {

								if ( response.success ) {

									$btn.text('Redirecting...');

									window.location = window.location;
								} else {

									// Enable button
									$btnText.attr('disabled', false);
									$btnText.prop('disabled', false);
									$btn.text(tempText);

									alert(response.data.message);
								}
							});
						} else {
							alert(response.data.message);
						}
					});
				});

				dialog.dialog( 'option', 'buttons', {
					'Save' : function(e) {
						$btn = $(e.target);
						$form.find('input[type="submit"]').trigger('click');
					}
				});
			}, 'Create Menu', {height: 500, width: 600});
		},

		_editMenu: function(e)
		{
			e.preventDefault();

			var menuId 	= this.dataset.id,
				type 	= this.dataset.type;

			api.showModal( 'menu-form', {
				menu_id: menuId,
				event_id: api.getQueryVar( 'object_id' )
			}, function(dialog) {

				api.sortables();

				var $form = dialog.find( 'form' ),
					$btn = null;

				$form.submit(function(e) {
					e.preventDefault();

					var $btnText 	= $btn.parent(),
						tempText 	= $btn.text();

					// // Disable button
					$btnText.attr('disabled', true);
					$btnText.prop('disabled', true);
					$btn.text('Saving...');

					// Save menu
					api.saveMenu( {
						menu_id  : menuId,
						menu 	 : $form.serializeObject(),
						type 	 : type
					}, function(response) {

						// Enable button
						$btnText.attr('disabled', false);
						$btnText.prop('disabled', false);
						$btn.text(tempText);

						if ( response.success ) {
							// window.location = window.location;
						} else {

							alert(response.data.message);
						}
					});
				});

				dialog.dialog( 'option', 'buttons', {
					'Save' : function(e) {
						$btn = $(e.target);
						$form.find('input[type="submit"]').trigger('click');
					}
				});
			}, 'Edit Menu', {height: 500, width: 600});
		},

		_newScheduleModal: function(e)
		{
			api.showModal( 'new-schedule', {}, function( dialog ) {

				var $form = dialog.find('form');
				date_picker( '#tam-schedule-start-date', '#tam-schedule-end-date', $form, false );

				dialog.dialog( 'option', 'buttons', {
					'Save Schedule' : function(e) {

						// Make button uploading
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// // Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						api.saveObject(

							// Class name
							'Schedule',

							// Object data
							{
								title: $form.find('#tam-title').val(),
								description: $form.find('#tam-schedule-description').val(),
								startDate: {
									type 	 : 'date',
									date 	 : $form.find('#tam-schedule-start-date').val()
								},
								endDate: {
									type 	 : 'date',
									date 	 : $form.find('#tam-schedule-end-date').val()
								},
								event: {
									type 	 : 'pointer',
									class 	 : 'Event',
									object_id: api.getQueryVar('object_id')
								}
							},

							// Object id
							null,

							// Callback
							function( response )
							{
								console.log(response);
								if ( response.success ) {

									$btnText.text('Redirecting...');

									window.location = response.data.object_url;
								} else {

									// Reset form
									$form.find('input').val('');

									// Enable button
									$btn.attr('disabled', false);
									$btn.prop('disabled', false);
									$btnText.text(tempText);

									alert(response.data.message);
								}
							}

						);
					}
				});
			}, 'Create Schedule', { width: 500, height: 600 } );
		},

		_newScheduleSlotModal: function(e)
		{
			api.showModal( 'new-schedule-slot', {}, function( dialog ) {

				var $form = dialog.find('form');

				dialog.dialog( 'option', 'buttons', {
					'Save Schedule Slot' : function(e) {

						// Make button uploading
						var $btnText 	= $(e.target),
							$btn 		= $btnText.parent(),
							tempText 	= $btnText.text();

						// // Disable button
						$btn.attr('disabled', true);
						$btn.prop('disabled', true);
						$btnText.text('Saving...');

						api.saveObject(

							// Class name
							'ScheduleSlot',

							// Object data
							{
								title: $form.find('#tam-title').val(),
								startDate: {
									type 	 : 'date',
									date 	 : $form.find('#tam-schedSlot-start-slot').val()
								},
								endDate: {
									type 	 : 'date',
									date 	 : $form.find('#tam-schedSlot-end-slot').val()
								},
								event: {
									type 	 : 'pointer',
									class 	 : 'Event',
									object_id: api.getQueryVar('object_id')
								}
							},

							// Object id
							null,

							// Callback
							function( response )
							{
								console.log(response);
								if ( response.success ) {

									$btnText.text('Redirecting...');

									window.location = response.data.object_url;
								} else {

									// Reset form
									$form.find('input').val('');

									// Enable button
									$btn.attr('disabled', false);
									$btn.prop('disabled', false);
									$btnText.text(tempText);

									alert(response.data.message);
								}
							}

						);
					}
				});
			}, 'Create Schedule Slot', { width: 500, height: 300 } );
		},

		_assignUserAsEditor: function(e) 
		{
			api._request( 'tam/user_as_editor', {
				is_editor : this.checked,
				user_id : this.dataset.userid
			}, function(res) {
				console.log(res);
			} );
		},

		_registerEventListeners: function()
		{
			$(document).on( 'click', '[data-action="edit-gallery-name-list"]', this._editGalleryNameInList );
			$(document).on( 'click', '[data-action="edit-gallery-name-single"]', this._editGalleryNameInSingle );
			$(document).on( 'click', '[data-action="edit-photo-name-list"]', this._editPhotoNameInList );
			$(document).on( 'click', '[data-action="create-new-gallery"]', this._createNewGallery );
			$(document).on( 'click', '[data-action="upload-photos"]', this._uploadGalleryPhotos );
			$(document).on( 'click', '[data-action="new-event"]', this._newEventModal );
			$(document).on( 'click', '[data-action="new-topic"]', this._newTopicModal );
			$(document).on( 'click', '[data-action="edit-topic-name"]', this._editTopicName );
			$(document).on( 'click', '[data-action="edit-topic-name-list"]', this._editTopicNameInList );
			$(document).on( 'click', '[data-action="create-new-page"]', this._newPage );
			$(document).on( 'click', '[data-action="page-thumbnail-picker"]', this._pageThumbnailPicker );
			$(document).on( 'click', '[data-action="page-thumbnail-remove"]', this._pageRemoveThumbnail );
			$(document).on( 'click', '[data-action="schedule-thumbnail-picker"]', this._scheduleThumbnailPicker );
			$(document).on( 'click', '[data-action="schedule-thumbnail-remove"]', this._scheduleRemoveThumbnail );
			$(document).on( 'click', '[data-action="avatar-picker"]', this._avatarPicker );
			$(document).on( 'click', '[data-action="avatar-remove"]', this._RemoveAvatar );
			$(document).on( 'click', '[data-action="confirm-delete"]', this._confirmDelete );
			$(document).on( 'click', '[data-action="new-announcement"]', this._newAnnouncementModal );
			$(document).on( 'click', '[data-action="upload-by-url"]', this._newFileByUrlModal );
			$(document).on( 'change', '[data-action="enable-menu"]', this._enableMenu );
			$(document).on( 'change', '[data-action="editor-mode-menu"]', this._editorModeMenu );
			$(document).on( 'click', '[data-action="upload-files"]', this._uploadFiles );
			$(document).on( 'click', '[data-dropdown]', this._dropdown );
			$(document).on( 'click', '[data-action="create-menu"]', this._newMenu );
			$(document).on( 'click', '[data-action="edit-menu"]', this._editMenu );
			$(document).on( 'click', '[data-action="new-schedule"]', this._newScheduleModal );
			$(document).on( 'click', '[data-action="new-schedule-slot"]', this._newScheduleSlotModal );
			$(document).on( 'change', '[data-action="user-as-editor"]', this._assignUserAsEditor );
		}
	}

	Admin.init();

	/**
	 * Location Picker Function
	 */
	function location_picker( $parent )
	{
		var defaultLong = parseFloat( $parent.find('#map-long').val() ),
			defaultLat 	= parseFloat( $parent.find('#map-lat').val() );

		if( isNaN( defaultLat ) ) {
			defaultLat = 14.5995124;
		}

		if( isNaN( defaultLong ) ) {
			defaultLong = 120.9842195;
		}

		$parent.find('#location-map').locationpicker({
			location 	: {
				latitude : defaultLat,
				longitude: defaultLong
			},
			zoom 		: 14,
			radius 		: 0,
			inputBinding: {
				latitudeInput 		: $parent.find('#map-lat'),
		        longitudeInput 		: $parent.find('#map-long'),
		        locationNameInput 	: $parent.find('#event-location'),
		    },
		    enableAutocomplete: true,
			onchanged: function(currentLocation, radius, isMarkerDropped) {

				var mapContext = $(this).locationpicker('map');
				mapContext.map.setZoom(14);

				$parent.find('#city').val(mapContext.location.addressComponents.city);
				$parent.find('#country').val(mapContext.location.addressComponents.country);
				$parent.find('#zipcode').val(mapContext.location.addressComponents.postalCode);
			}
		});
	}

	/**
	 * Date Picker Function
	 */
	function date_picker( $start, $end, $parent, isSingle )
	{
		if( isSingle === true ) {

			$parent.find($start).datepicker();
		} else {

			var default_end = $parent.find($end).val();
			var default_start = $parent.find($start).val();
			if( default_end === undefined ) {
				default_end = '';
			}
			if( default_start === undefined ) {
				default_start = '';
			}

			$parent.find($start).datepicker({
				maxDate: default_end,
				onClose: function( selectedDate ) {
					$parent.find($end).datepicker( "option", "minDate", selectedDate );
				}
			});

			$parent.find($end).datepicker({
				minDate: default_start,
				onClose: function( selectedDate ) {
					$parent.find($start).datepicker( "option", "maxDate", selectedDate );
				}
			});
		}
	}

	date_picker( '#start-event', '#end-event', $(document), false );
	location_picker( $(document) );


	/**
	 * Runs when new event modal is displayed
	 */
	function action_open_new_event_dialog( dialog, $el )
	{
		// Setup datepickers
		date_picker( '#tam-event-start-date', '#tam-event-end-date', $el, false );

		// Setup google map
		location_picker($el);

		// Add Dialog buttons
		dialog.dialog( 'option', 'buttons', {
			'Create Event' : function() {
				$el.find('#tam-new-event-form').submit();
			}
		});
	}

	TpbAppManager.addAction( 'tam/dialog/open/name=new-event', action_open_new_event_dialog );

})(jQuery, TpbAppManager);