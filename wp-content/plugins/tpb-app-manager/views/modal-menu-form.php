<form method="POST" id="tam-dialog-new-menu">
		
	<div style="display: none;">
		<input type="submit">

		<?php if ( !empty( $menu_id ) ): ?>
			<input type="hidden" value="<?php echo $menu_id ?>" name="menu[id]">
		<?php endif; ?>

	</div>

	<?php do_action( 'tam/menu_type/fields', (!empty($menu_id) ? $menu_id : null), (!empty($type) ? $type : null), (!empty($event_id) ? $event_id : null) ); ?>

</form>