<form class="tam-dialog-form">

	<fieldset>
		<label for="title">Announcement</label>
		<input type="text" value="" required id="tam-title" name="title" class="tam-dialog-form-field" placeholder="">
	</fieldset>
	
	<div class="field-group">
		<label>Message</label>
		<textarea name="message" id="tam-message" cols="30" rows="10"></textarea>
	</div>

</form>