<form class="tam-dialog-form">
	
	<fieldset>
		<label for="title"><?php echo $field_label; ?></label>
		<input type="text" value="<?php echo !empty( $old_value ) ? $old_value : ''; ?>" required id="tam-name" name="<?php echo esc_attr( $object_column ); ?>" class="tam-dialog-form-field" placeholder="">
	</fieldset>

</form>