<?php  
/**
 * Vars
 */
$tabs 		= tam_get_event_tabs();
$active_tab = tam_get_event_active_tab();
?>


<h2>
	<?php echo $object->get('title'); ?>
	<a href="<?php echo tam_admin_page_url(); ?>" class="page-title-action">&laquo; Back to Events</a>
</h2>


<h2 class="nav-tab-wrapper tam-nav-tab-wrapper">

	<?php foreach( $tabs as $k => $v ): $is_active_tab = ( $active_tab == $k ); ?>

		<a href="<?php echo tam_get_event_tab_link( $k ); ?>" class="nav-tab <?php echo $is_active_tab ? 'nav-tab-active' : ''; ?>"><?php echo $v; ?></a>

	<?php endforeach; ?>

</h2>


<?php do_action( 'tam/event_'. $active_tab .'_tab_content', $object_id ); ?>


