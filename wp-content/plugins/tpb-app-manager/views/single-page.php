<div class="tam-single-header tam-clearfix">
	<h3 class="tam-single-header-title">
		<span id="page-name"><?php echo $title; ?></span>
	</h3>
	<a href="<?php echo remove_query_arg( array( 'page_id', 's', 'paged' ) ); ?>" class="page-title-action tam-page-title-action">Back to Pages</a>
</div>


<form action="" method="POST">
	
	<div style="display: none;">

		<input type="hidden" name="object_id" value="<?php echo esc_attr( $page_id ); ?>">

		<?php wp_nonce_field( 'tam_nonce', 'tam_nonce' ); ?>

	</div>

	<div class="tam-two-col">
		
		<div class="tam-content">
			
			<div class="field-group">
				
				<label for="">Title</label>
				<input type="text" name="title" value="<?php echo $title; ?>" class="tam-form-control" placeholder="Enter title here...">

			</div>

			<div class="field-group">
				
				<label for="">Subtitle</label>
				<input type="text" name="subtitle" value="<?php echo $subtitle; ?>" class="tam-form-control" placeholder="Enter subtitle here...">

			</div>

			<div class="field-group">
				
				<label for="">Content</label>
				<?php

				$editor_id 	='tam-event-content';

				$desciption_settings = array(
					'media_buttons'		=> false,
					'default_editor'	=> 'tinymce',
					'textarea_rows'		=> 6,
					'textarea_name'		=> 'content'
				);
				wp_editor( $content, $editor_id, $desciption_settings );

				?>

			</div>

			<div class="tam-postbox">
				<h3 class="tam-postbox-title">Map</h3>
				<div class="tam-postbox-content">
					
					<div class="tam-postbox-content-inner">
						<div class="field-group">
							<label>Type location: </label>
							<input type="text" required name="address" class="tam-form-control" id="event-location">
						</div>
						<div id="location-map"></div>
						<input type="hidden" name="longitude" id="map-long">
						<input type="hidden" name="latitude" id="map-lat">
					</div>

				</div>
			</div>

		</div>

		<div class="tam-aside">
			
			<div class="tam-postbox">
				<h3 class="tam-postbox-title">Actions</h3>
				<div class="tam-postbox-content">
					
					<div class="tam-postbox-content-inner tam-postbox-content-gray tam-clearfix">
						<a data-action="confirm-delete" href="<?php echo $delete_url; ?>" class="tam-text-danger tam-link-button tam-left">Delete</a>
						<input name="save" type="submit" value="Save" class="button button-primary tam-right tam-button-inline">
						<input name="save_and_close" title="Save and go back to the list of pages" type="submit" value="Save &amp; Close" class="button tam-button-inline tam-right">
					</div>

				</div>
			</div>

			<div class="tam-postbox">
				<h3 class="tam-postbox-title">Media</h3>
				<div class="tam-postbox-content">
					
					<div class="tam-postbox-content-inner tam-clearfix">
						<div class="tam-has-parent <?php echo !empty( $thumbnail_url ) ? 'tam-has' : ''; ?>">
							<div data-field="image" class="tam-thumbnail tam-postbox-thumbnail tam-thumbnail-rect-sm" style="background-image: url(<?php echo $thumbnail_url; ?>);"></div>
							<a href="#" class="tam-thumbnail-picker" data-action="page-thumbnail-picker">Set thumbnail photo</a>
							<a href="#" class="tam-thumbnail-remove tam-text-danger" data-action="page-thumbnail-remove">Remove thumbnail</a>
						</div>
					</div>

				</div>
			</div>

		</div>

	</div>

</form>