<form class="tam-dialog-form">
	
	<div class="field-group">
		<label>Title</label>
		<input type="text" required name="title" data-field="title">
		<p class="description">Please enter the title of the page.</p>
	</div>

	<div class="field-group">
		<label>Subtitle</label>
		<input type="text" required name="subtitle" data-field="subtitle">
	</div>

	<div class="field-group">
		<label>Content</label>
		<p class="description">Please enter the contents of the page below.</p>
		<?php
		$content 	= '';
		$editor_id 	= 'tam-content';

		$desciption_settings = array(
			'wpautop'			=> false,
			'media_buttons'		=> false,
			'default_editor'	=> 'tinymce',
			'textarea_rows'		=> 6,
			'textarea_name'		=> 'content'
		);
		wp_editor( $content, $editor_id, $desciption_settings );
		?>

	</div>
		
</form>

<?php

// Load only if template is loaded via ajax
if ( defined( 'DOING_AJAX' ) &&  DOING_AJAX ) {
		
	_WP_Editors::enqueue_scripts();
	print_footer_scripts();
	_WP_Editors::editor_js();
}