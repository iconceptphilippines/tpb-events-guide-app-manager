<form action="" method="post" id="tam-new-event-form" enctype="multipart/form-data">

	<div class="field-group">
		<label>Title</label>
		<input type="text" required name="tam_title">
	</div>

	<div class="field-group">
		<label>Description</label>
		<?php

		$content 	='';
		$editor_id 	='tam-event-content';

		$desciption_settings = array(
			'media_buttons'		=> false,
			'default_editor'	=> 'tinymce',
			'textarea_rows'		=> 6,
			'textarea_name'		=> 'tam_content'
		);
		wp_editor( $content, $editor_id, $desciption_settings );

		?>
	</div>

	<div class="field-group">
		<div class="field-col">

			<div class="field-col-2">
				<label>Start Date</label>
				<input required type="text" name="tam_start_date" id="tam-event-start-date">
			</div>

			<div class="field-col-2">
				<label>End Date</label>
				<input type="text" name="tam_end_date" id="tam-event-end-date">
			</div>

		</div>
	</div>

	<div class="field-group">
		<label>Banner</label>
		<input required type="file" name="image">
	</div>

	<div class="field-group">
		<label>Venue Name</label>
		<input type="text" name="tam_venue_name" id="tam-venue-name">
	</div>

	<div class="field-group">
		<label>Map</label>
		<input type="text" required name="tam_address" id="event-location">
		<div id="location-map"></div>
		<input type="hidden" name="tam_longitude" id="map-long">
		<input type="hidden" name="tam_latitude" id="map-lat">
		<input type="hidden" name="tam_city" id="city">
		<input type="hidden" name="tam_country" id="country">
		<input type="hidden" name="tam_zipcode" id="zipcode">
		<input type="hidden" name="action" value="new_event">
	</div>

	<div class="field-group">

		<select name="status" id="status">
			<option value="publish">Publish</option>
			<option value="editor">Editor Mode</option>
		</select>

	</div>

	<div class="field-group">
		<label><input type="checkbox" name="tam_public" value="1"> Public</label>
	</div>

</form>

<?php

// Load only if template is loaded via ajax
if ( defined( 'DOING_AJAX' ) &&  DOING_AJAX ) {

	_WP_Editors::enqueue_scripts();
	print_footer_scripts();
	_WP_Editors::editor_js();
}