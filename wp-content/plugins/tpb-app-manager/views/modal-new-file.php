<form class="tam-dialog-form" id="tam-form-new-file" method="POST">
	
	<div class="field-group">
		<label>File Name <span class="tam-text-danger">*</span></label>
		<input type="text" id="tam-file-name" name="file_name" required="required">
	</div>

	<div class="field-group">
		<label>File URL <span class="tam-text-danger">*</span></label>
		<input type="text" id="tam-file-url" name="file_url" required="required">
	</div>

	<div class="field-group">
		<label>File Size <span class="tam-text-danger">*</span></label>
		<input type="text" id="tam-file-size" name="file_size" required="required">
	</div>

	<div class="field-group">
		<label>File Type</label>
		<input type="text" id="tam-file-type" name="file_type">
	</div>

	<div style="display: none;">
		<input type="submit">
	</div>

</form>