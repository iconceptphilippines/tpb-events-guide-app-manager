<div class="tam-single-header tam-clearfix">
	<h3 class="tam-single-header-title"><span id="gallery-name"><?php echo $title; ?></span>
	</h3>
	<a href="<?php echo remove_query_arg( array( 'gallery_id', 's', 'paged' ) ); ?>" class="page-title-action tam-page-title-action">Back to Galleries &raquo;</a>
	<a href="" class="page-title-action tam-page-title-action" data-action="upload-photos">Upload Photos</a>
	<a href="#" class="page-title-action tam-page-title-action" data-action="create-new-gallery"><i class="dashicons dashicons-plus"></i> New Gallery</a>
	<a href="#" class="page-title-action tam-page-title-action" title="Edit Gallery Name" data-object-id="<?php echo esc_attr( $object_id ); ?>" data-title="<?php echo $title; ?>" data-action="edit-gallery-name-single"><i class="dashicons dashicons-edit"></i></a>
</div>

<?php

$table->display();