<form class="tam-dialog-form">
	
	<div class="field-group">
		<label>Title</label>
		<input type="text" required name="title" id="tam-title" data-field="title">
		<p class="description">Please enter the title of the schedule.</p>
	</div>


	<div class="field-group">
		<label>Description</label>
		<p class="description">Please enter the description of the schedule below.</p>
		<?php
		$content 	= '';
		$editor_id 	= 'tam-schedule-description';

		$desciption_settings = array(
			'media_buttons'		=> false,
			'default_editor'	=> 'tinymce',
			'textarea_rows'		=> 6,
			'textarea_name'		=> 'description'
		);
		wp_editor( $content, $editor_id, $desciption_settings );
		?>

	</div>
	
	<div class="field-group">
		<div class="field-col">

			<div class="field-col-2">
				<label>Start Date</label>
				<input type="text" name="tam_start_date" id="tam-schedule-start-date">
			</div>

			<div class="field-col-2">
				<label>End Date</label>
				<input type="text" name="tam_start_date" id="tam-schedule-end-date">
			</div>

		</div>
	</div>

</form>

<?php

// Load only if template is loaded via ajax
if ( defined( 'DOING_AJAX' ) &&  DOING_AJAX ) {
		
	_WP_Editors::enqueue_scripts();
	print_footer_scripts();
	_WP_Editors::editor_js();
}