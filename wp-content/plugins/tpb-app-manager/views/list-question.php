<div class="tam-single-header">
	<h3 class="tam-single-header-title"><span id="topic-name"><?php echo $title; ?></span>
	</h3>
	<a href="<?php echo remove_query_arg( array( 'topic_id' ) ); ?>" class="page-title-action tam-page-title-action">Back to Topics &raquo;</a>
	<a href="<?php echo tam_get_topic_export_link($object_id); ?>" target="_new" class="page-title-action tam-page-title-action">Export as PDF</a>
	<a href="#" class="page-title-action tam-page-title-action" title="Edit Topic Name" data-object-id="<?php echo esc_attr( $object_id ); ?>" data-title="<?php echo $title; ?>" data-action="edit-topic-name"><i class="dashicons dashicons-edit"></i></a>
</div>
<?php

$table->display();