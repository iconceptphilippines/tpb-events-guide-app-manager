<div class="tam-single-header">
	<a href="<?php echo remove_query_arg( array( 'schedule_id' ) ); ?>" class="page-title-action tam-page-title-action">Back to Schedules &raquo;</a>
	<a href="#" class="page-title-action tam-page-title-action" data-action="new-schedule-slot"><i class="dashicons dashicons-plus"></i> Add Schedule Slot</a>
</div>
<?php

$table->display();