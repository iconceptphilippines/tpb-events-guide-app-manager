<div class="tam-single-header tam-clearfix">
	<div class="tam-right">
		<div class="tam-dropdown">
			<a data-dropdown href="#" class="page-title-action tam-page-title-action tam-dropdown-button" data-action="new-menu">
				<span>Create Menu</span>
			</a>
			<ul class="tam-dropdown-items">
				<?php foreach( $types as $key => $label ): ?>
				<li><a href="#" data-action="create-menu" data-type="<?php echo esc_attr( $key ) ?>"><?php echo $label; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<?php $table->display(); ?>

