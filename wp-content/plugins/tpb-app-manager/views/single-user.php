<?php
/**
 * Saving
 */

if( isset($_POST['update_profile']) ) {
	$id = $_GET['object_id'];

	// Get the submitted data
	$firstName 	= filter_input( INPUT_POST , 'tam_user_first_name' );
	$lastName 	= filter_input( INPUT_POST , 'tam_user_last_name' );
	$middleName = filter_input( INPUT_POST , 'tam_user_middle_name' );
	$email 		= filter_input( INPUT_POST, 'tam_user_email' );
	$mobile_no 	= filter_input( INPUT_POST, 'tam_user_number' );
	$company 	= filter_input( INPUT_POST, 'tam_user_company' );
	$position 	= filter_input( INPUT_POST, 'tam_user_position' );


	try {

		// Save the page details
		tam_save_object( '_User', array(
			'firstName' 		=> $firstName,
			'middleName' 		=> $middleName,
			'lastName' 			=> $lastName,
			'email' 			=> $email,
			'mobileNo' 			=> $mobile_no,
			'company' 			=> $company,
			'position' 			=> $position,

		), $id );

		wp_redirect(
			add_query_arg( array(
				'page'			=> 'tam-users',
				'object_class' 	=> '_User',
				'object_id' 	=> $id
			), tam_admin_page_url() )
		);

		exit;

	} catch (Exception $e) {

		echo $e->getMessage();
		die();
	}
}

?>


<h2>
	Profile
	<a href="<?php echo esc_url( add_query_arg( array( 'page' => 'tam-users' ), tam_admin_page_url() ) ); ?>" class="page-title-action">&laquo; Back to Users</a>
</h2>

<div id="post-stuff" class="event-post-stuff">

	<form action="" method="post" enctype="multipart/form-data">

		<div id="post-body" class="event-post-body">

			<div id="post-body-content">

				<div class="tam-postbox">
					<h3 class="tam-postbox-title">Information</h3>

					<div class="tam-postbox-content">
						<div class="tam-postbox-content-inner">

							<div class="field-group">
								<label for="">Username</label>
								<input type="text" name="tam_username" id="tam-username" value="<?php echo $username; ?>" readonly="readonly">
							</div>

							<div class="field-group">
								<label for="">First Name</label>
								<input type="text" name="tam_user_first_name" id="tam-user-first-name" value="<?php echo $first_name; ?>" >
							</div>

							<div class="field-group">
								<label for="">Middle Name</label>
								<input type="text" name="tam_user_middle_name" id="tam-user-middle-name" value="<?php echo $middle_name; ?>" >
							</div>

							<div class="field-group">
								<label for="">Last Name</label>
								<input type="text" name="tam_user_last_name" id="tam-user-last-name" value="<?php echo $last_name; ?>" >
							</div>

							<div class="field-group">
								<label for="">Email</label>
								<input type="email" name="tam_user_email" id="tam-user-email" value="<?php echo $email_add; ?>">
							</div>

							<div class="field-group">
								<label for="">Mobile No.</label>
								<input type="text" name="tam_user_number" id="tam-user-number" value="<?php echo $mobile_no; ?>">
							</div>

						</div>
					</div>
				</div>

			</div>

			<div class="tam-aside">

				<div class="tam-postbox">
					<h3 class="tam-postbox-title">Avatar</h3>
					<div class="tam-postbox-content">
						<div class="tam-postbox-content-inner">

							<div class="tam-has-parent <?php echo !empty( $avatar ) ? 'tam-has' : ''; ?>">
								<div data-field="image" class="tam-thumbnail tam-postbox-thumbnail tam-thumbnail-rect-sm tam-avatar" style="background-image: url(<?php echo $avatar; ?>);"></div>
								<a href="#" class="tam-thumbnail-picker" data-action="avatar-picker">Set Avatar</a>
								<a href="#" class="tam-thumbnail-remove tam-text-danger" data-action="avatar-remove">Remove Avatar</a>
							</div>
						</div>
					</div>
				</div>

				<div class="tam-postbox">
					<h3 class="tam-postbox-title">Company Information</h3>
					<div class="tam-postbox-content">
						<div class="tam-postbox-content-inner">

							<div class="field-group">
								<label for="">Company</label>
								<input type="text" name="tam_user_company" id="tam-user-company" value="<?php echo $object->get( 'company' ); ?>" >
							</div>

							<div class="field-group">
								<label for="">Position</label>
								<input type="text" name="tam_user_position" id="tam-user-position" value="<?php echo $object->get( 'position' ); ?>" >
							</div>

						</div>

						<div class="tam-postbox-content-inner tam-postbox-content-gray tam-clearfix">
							<input type="submit" name="update_profile" class="button button-primary tam-right tam-button-inline tam-update-user" value="Update Profile">
						</div>
					</div>
				</div>

			</div>

		</div>

	</form>

</div>