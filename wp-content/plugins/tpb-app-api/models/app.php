<?php
namespace TPBAppApi;
use WP_Query;

class App {

	public static function verified($token) {
		return tpb_app_api_app_is_valid_by_token($token);
	}
}