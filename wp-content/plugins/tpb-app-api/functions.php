<?php

function tpb_app_api_register_endpoint() {
	add_rewrite_tag( '%api%', '([^&]+)' );
    add_rewrite_rule( 'api/([^&]+)/?', 'index.php?api=$matches[1]', 'top' );
	add_rewrite_tag( '%apps%', '([^&]+)' );
    add_rewrite_rule( 'apps(/([^&]+)/?)?', 'index.php?apps=$matches[1]', 'top' );
}

add_action( 'init', 'tpb_app_api_register_endpoint' );



function tpb_app_api_endpoint_listener() {
	global $wp_query;

	$endpoint_string = $wp_query->get('api');

	// Bail early if it wasn't an api call
	if (!$endpoint_string) {
		return;
	}

	$endpoint_params = explode(DIRECTORY_SEPARATOR, $endpoint_string);
	$version         = isset( $endpoint_params[0] ) ? $endpoint_params[0] : 'v1';
	$endpoint        = isset( $endpoint_params[1] ) ? $endpoint_params[1] : '';
	$params          = isset( $endpoint_params[2] ) ? $endpoint_params[2] : '';
	$api_endpoint    = str_replace($version . '/', '', $endpoint_string);
	$path            = TPB_APP_API_DIR . '/api/' . $version . '/' . $endpoint . '.php';
	$path_abstract   = TPB_APP_API_DIR . '/api/' . $version . '/api.php';


	if (file_exists($path_abstract)) {
		include_once $path_abstract;

		if (file_exists($path)) {
			include_once $path;
			$class = 'TPBAppApi\Api\\' . ucwords(strtolower($endpoint));

			$api = new $class($api_endpoint);
			$api->authenticate();
			$api->processApi();
		} else {
			header("Access-Control-Allow-Orgin: *");
	    	header("Access-Control-Allow-Methods: *");
	    	header("Content-Type: application/json");
			echo json_encode(array(
				"message" => "{$endpoint} endpoint not found .",
				"success" => false,
				"status"  => 200,
				"body" 	  => ""
			));
		}
	} else {
		header("Access-Control-Allow-Orgin: *");
    	header("Access-Control-Allow-Methods: *");
    	header("Content-Type: application/json");
		echo json_encode(array(
			"message" => "{$endpoint} endpoint not found.",
			"success" => false,
			"status"  => 200,
			"body" 	  => ""
		));
	}

	die();
} 

add_action( 'template_redirect', 'tpb_app_api_endpoint_listener' );


function tpb_app_api_endpoint_restrict_login() {
	global $wp_query;

	// Bail early if it wasn't an api call
	if (isset($wp_query->query_vars['api']) || isset($wp_query->query_vars['apps'])) {
		return;
	}

	if (!is_user_logged_in()) {
		wp_redirect( wp_login_url( site_url( '/apps' ) ) );
	} else {
		wp_redirect( site_url( '/apps' ) );
	}
	die();
}

// last to execute
add_action( 'template_redirect', 'tpb_app_api_endpoint_restrict_login', 9999999 );


function tpb_app_api_endpoint_apps() {
	global $wp_query;

	$message = '';

	// Bail early if it wasn't an api call
	if (!isset($wp_query->query_vars['apps'])) {
		return;
	}

	if (!is_user_logged_in()) {
		wp_redirect(wp_login_url(site_url('/apps')));
		die();
	}

	// Save app
	if (!empty($_POST)) {
		// verify nonce
		if (wp_verify_nonce( $_POST['tpb_nonce'], 'tpb_nonce' )) {
			// Save app
			$id = wp_insert_post(array(
				'post_type' 	=> 'app',
				'post_title' 	=> $_POST['app_name'],
				'post_status' 	=> 'publish',
				'post_author'	=> get_current_user_id()
			));

			update_post_meta( $id, 'app_enabled', true );
		}
	}

	// Delete app
	if (isset($_GET['delete_id'])) {
		// Verify nonce
		if (wp_verify_nonce( filter_input(INPUT_GET, 'nonce'), 'tpb_nonce' )) {
			wp_delete_post( $_GET['delete_id'], true );
			wp_redirect(site_url('/apps'));
			die();
		}
	}

	$query_apps = tpb_app_api_get_user_apps( get_current_user_id() );

	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Your Apps</title>
		<style>
		.wrapper {
			width: 100%;
			margin: 0 auto;
			max-width: 960px;
		}
		.table {
			width: 100%;
			table-layout: collapse;
			border: 0;
		}
		.table th,
		.table td {
			padding: 10px;
		}
		.table td {
			border-bottom: 1px solid #f9f9f9;
		}
		.table th {
			text-align: left;
			background: #f9f9f9;
		}
		.description {
			color: gray;
			font-family: arial, sans-serif;
			font-size: 12px;
		}
		form {
			margin-bottom: 30px;
		}
		.flash {
			display: block;
			padding: 10px;
			color: #fff;
			border: 1px solid #999;
			margin: 0;
		}
		.flash.success {
			border-color: #187B18;
			background: #48AF48;
			color: #fff;
		}
		.flash.error {
			border-color: red;
		    background: #FD704C;
		    color: #fff;
		}
		</style>
	</head>
	<body>
		<div class="wrapper">
			<h1>Your Apps</h1>
			<p class="description">These are the apps that can be use to access the API of the TPB Events Guide application.</p>
			<p><a href="<?php echo wp_logout_url( wp_login_url(site_url('/apps')) ); ?>">Logout</a></p>
			<form action="" method="POST">
				<?php wp_nonce_field( 'tpb_nonce', 'tpb_nonce' ); ?>
				<fieldset>
					<legend>Add new app</legend>
					<input type="text" size="30" name="app_name" placeholder="Type app name here">
					<input type="submit" value="Add">
				</fieldset>
			</form>
			<table class="table" cellspacing="0">
				<thead>
					<th>Name</th>
					<th>Token</th>
					<th>Actions</th>
				</thead>
				<tbody>
					<?php while( $query_apps->have_posts() ): $query_apps->the_post(); global $post; ?>
					<tr>
						<td><?php the_title(); ?></td>
						<td><?php echo get_post_meta($post->ID, 'app_token', true); ?></td>
						<td><a href="<?php echo add_query_arg(array('delete_id' => $post->ID, 'nonce' => wp_create_nonce( 'tpb_nonce' ))); ?>" style="color: #f30;" onclick="if(!confirm('Are you sure?')) {return false;}">Delete</a></td>
					</tr>
					<?php endwhile; ?>
					<?php if( !$query_apps->have_posts() ): ?>
						<tr>
							<td colspan="3">No apps found.</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</body>
	</html>
	<?php

	die();
}

add_action( 'template_redirect', 'tpb_app_api_endpoint_apps' );



function your_function($user_login, $user) {
	if (in_array('subscriber', $user->roles)) {
		wp_redirect(site_url('/apps'));
		die();
	}
}

add_action('wp_login', 'your_function', 10, 2);


/**
 * Register app post type
 * @return void
 */
function tpb_app_api_data() {
	register_post_type( 'app', array(
		'labels' => array(
			'name'                => __( 'Third Party Apps', 'tpb-app-api' ),
			'singular_name'       => __( 'App', 'tpb-app-api' ),
			'add_new'             => _x( 'New App', 'tpb-app-api', 'tpb-app-api' ),
			'add_new_item'        => __( 'New App', 'tpb-app-api' ),
			'edit_item'           => __( 'Edit App', 'tpb-app-api' ),
			'new_item'            => __( 'New App', 'tpb-app-api' ),
			'view_item'           => __( 'View App', 'tpb-app-api' ),
			'search_items'        => __( 'Search Apps', 'tpb-app-api' ),
			'not_found'           => __( 'No Apps found', 'tpb-app-api' ),
			'not_found_in_trash'  => __( 'No Apps found in Trash', 'tpb-app-api' ),
			'parent_item_colon'   => __( 'Parent App:', 'tpb-app-api' ),
			'menu_name'           => __( 'Third Party Apps', 'tpb-app-api' ),
			'all_items'           => __( 'Apps', 'tpb-app-api' ),
		),
		'public'              => false,
		'show_ui'             => true,
		'show_in_admin_bar'   => false,
		'menu_position'       => 99,
		'menu_icon'			  => 'dashicons-hammer',
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => false,
		'supports'            => array('title')
	));
}

add_action( 'init', 'tpb_app_api_data' );



/**
 * Create new metabox for the app post type
 * @return void
 */
function tpb_app_api_apps_metaboxes() {
	add_meta_box(
		'Apps-metabox',
		__( 'App Details', 'tpb-app-api' ),
		'tpb_app_api_apps_metabox_content',
		array( 'App' ),
		'normal',
		'high'
	);
}

add_action( 'add_meta_boxes', 'tpb_app_api_apps_metaboxes' );


/**
 * Add the metabox content in the app edit page in the admin
 * @return void
 */
function tpb_app_api_apps_metabox_content() {
	global $pagenow, $post;

	$is_new  = "post.php" !== $pagenow;
	$token   = '';
	$enabled = true;

	if (isset($post->ID)) {
		$token   = get_post_meta( $post->ID, 'app_token', true );
		$enabled = get_post_meta( $post->ID, 'app_enabled', true );

		if ($enabled == null) {
			$enabled = true;
		}
	}

	?>
		<?php if( $is_new ): ?>
			<p class="description">
				Please save to generate token.
			</p>
		<?php else: ?>
			<p>
				<label for="">Token</label>
				<input readonly type="text" class="widefat" value="<?php echo esc_attr($token); ?>">
			</p>
		<?php endif; ?>
		<p>
			<label for="">Enabled</label>
			<span style="display: block;">
				<input type="hidden" name="app_enabled" value="0">
				<input type="checkbox" name="app_enabled" value="1" <?php checked( $enabled, true ); ?>>
			</span>
		</p>
	<?php
}


/**
 * Generates a token string
 * @param  integer $app_id 
 * @return void
 */
function tpb_app_api_apps_generate_token($app_id) {
	return md5(bin2hex(random_bytes(20)) . $app_id);
}

/**
 * Generates an app token when the post is saved for the first time
 * @param  integer $app_id The id of the app
 * @param  WP_Post $post   Object of the app
 * @param  bool    $update Whether the app is new or just updated
 * @return void
 */
function tpb_app_api_apps_save_app($app_id, $post, $update) {

	if ( wp_is_post_revision( $app_id ) )
		return;

	if (!$post) {
		return;
	}


	// Only new post
	if (!$update) {
		$token = tpb_app_api_apps_generate_token($app_id);
		update_post_meta( $app_id, 'app_token', $token );
	}	

	// Save enabled
	update_post_meta( $app_id, 'app_enabled', filter_input(INPUT_POST, 'app_enabled') );
}

add_action( 'save_post_app', 'tpb_app_api_apps_save_app', 10, 3 );


function tpb_app_api_get_user_apps( $user_id ) {
	$query = new WP_Query(array(
		'post_type'      => 'app',
		'posts_per_page' => -1,
		'author'         => $user_id
	));

	return $query;
}


function tpb_app_api_app_is_valid_by_token( $app_token ) {
	$query = new WP_Query(array(
		'post_type'      => 'app',
		'posts_per_page' => 1,
		'fields'         => 'ids',
		'meta_query'     => array(
			array(
				'key'     => 'app_token',
				'value'   => $app_token,
				'compare' => '=='
			)
		)
	));

	$valid = false;
	$valid = $query->have_posts();
	$post_id = 0;

	if ($valid) {
		// Get first id
		$post_id = $query->posts[0];
		// Check if the app api is enabled
		$enabled = get_post_meta( $post_id, 'app_enabled', true );
		
		if (!$enabled) {
			throw new Exception("App not enabled.");
		}
	}

	return $valid;
}




function tpb_app_api_admin_menu() {
	add_submenu_page( 'edit.php?post_type=app', 'Documentation', 'Documentation', 'manage_options', 'documentation', '__return_false' );
}

add_action( 'admin_menu', 'tpb_app_api_admin_menu' );


function tpb_app_api_admin_menu_init() {
	global $submenu;

	if (!empty($submenu['edit.php?post_type=app'])) {
		if (!empty($submenu['edit.php?post_type=app'][11])) {
			$submenu['edit.php?post_type=app'][11][2] = TPB_APP_API_URL . 'documentation';
		}
	}
}

add_action( 'admin_init', 'tpb_app_api_admin_menu_init' );




