<?php
/**
 * Plugin Name: TPB App API
 * Author: iConcept Philippines
 * Author URI: https://iconcept.com.ph
 * Description: API plugin for the TPB Events Guide app (LIMITED FUNCTIONS ONLY)
 * Version: 0.0.2
 */

// Bail out if file is accessed directly
if (!defined('WPINC')) {
	die();
}

if (!defined('TPB_APP_API_DIR')) {
	define('TPB_APP_API_DIR', plugin_dir_path( __FILE__ ));
}

if (!defined('TPB_APP_API_URL')) {
	define('TPB_APP_API_URL', plugin_dir_url( __FILE__ ));
}

require_once TPB_APP_API_DIR . '/functions.php';
require_once TPB_APP_API_DIR . '/models/app.php';
