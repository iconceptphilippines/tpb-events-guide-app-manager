<?php

namespace TPBAppApi\Api;
use Exception;

class User extends Api {
	public function user($args) {
		/**
		 * Create new user
		 */
		if ('POST' === $this->method && empty($this->args[0])) {
			$object = new \Parse\ParseObject('_User');
			$params = $this->request;

			if (!empty($this->request['event'])) {
				unset($params['event']);
			}

			foreach($params as $k => $v) {
				$object->set($k, $v);
			}

			try {
				$object->save();

				// Save event
				if (!empty($this->request['event'])) {
					$eventAttendee 	= new \Parse\ParseObject('EventAttendee');
					$event 			= new \Parse\ParseObject('Event', $this->request['event']);
					$eventAttendee->set('event', $event);
					$eventAttendee->set('user', $object);
					$eventAttendee->set('isApproved', false);
					$eventAttendee->save();
				}
				
				$this->_response(array(
					"success" => true,
					"body"    => $object->getObjectId()
				));

			} catch (Exception $e) {
				$this->_response(array(
					"success" => false, 
					"message" => $e->getMessage()
				));
			}
		}
	}
}