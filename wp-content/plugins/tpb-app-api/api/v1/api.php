<?php

namespace TPBAppApi\Api;
use Exception;
use TPBAppApi\App;

abstract class Api {

	/**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
	protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($request) {
    	header("Access-Control-Allow-Orgin: *");
    	header("Access-Control-Allow-Methods: *");
    	header("Content-Type: application/json");

    	$this->args = explode('/', rtrim($request, '/'));
    	$this->endpoint = array_shift($this->args);
    	if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
    		$this->verb = array_shift($this->args);
    	}

    	$this->method = $_SERVER['REQUEST_METHOD'];
    	if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
    		if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
    			$this->method = 'DELETE';
    		} else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
    			$this->method = 'PUT';
    		} else {
    			throw new Exception("Unexpected Header");
    		}
    	}

    	switch($this->method) {
    		case 'DELETE':
    		case 'POST':
    		$this->request = $this->_cleanInputs($_POST);
    		break;
    		case 'GET':
    		$this->request = $this->_cleanInputs($_GET);
    		break;
    		case 'PUT':
    		$this->request = $this->_cleanInputs($_GET);
    		$this->file = file_get_contents("php://input");
    		break;
    		default:
    		$this->_response('Invalid Method', 405);
    		break;
    	}
    }

    public function processAPI() {
    	if (method_exists($this, $this->endpoint)) {
    		try {
    			$this->{$this->endpoint}($this->args);
    		} catch (Exception $e) {
				$this->_response( ["success" => false, "message" => $e->getMessage()] , 405);
    		}
    	} else {
	    	$this->_response(["success" => false, "message" => "No Endpoint: $this->endpoint"], 404);
    	}
    }

    public function authenticate() {
        if (isset($_REQUEST['token'])) {
            try {
                if (!App::verified($_REQUEST['token'])) {
                    $this->_response(array(
                        'success' => false,
                        'message' => 'Invalid token'
                    ));
                    die();
                }
            } catch (Exception $e) {
                $this->_response(array(
                    'success' => false,
                    'message' => $e->getMessage()
                ));
                die();
            }
        } else {
            $this->_response(array(
                'success' => false,
                'message' => "Please provide a 'token' to access the api. Register and create your app here ". site_url('/apps') ." to get a token.",
            ));
            die();
        }
    }

    protected function _response($data, $status = 200) {
    	header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
    	$defaults = array(
			"body"    => "",
			"success" => true,
			"status"  => $status,
			"message" => ""
    	);

    	echo json_encode(wp_parse_args( $data, $defaults ));
    }

    private function _cleanInputs($data) {

        if (isset($data['token'])) {
            unset($data['token']);
        }

    	$clean_input = Array();
    	if (is_array($data)) {
    		foreach ($data as $k => $v) {
    			$clean_input[$k] = $this->_cleanInputs($v);
    		}
    	} else {
    		$clean_input = trim(strip_tags($data));
    	}
    	return $clean_input;
    }

    private function _requestStatus($code) {
    	$status = array(  
    		200 => 'OK',
    		404 => 'Not Found',   
    		405 => 'Method Not Allowed',
    		500 => 'Internal Server Error',
    		); 
    	return ($status[$code])?$status[$code]:$status[500]; 
    }
}

